﻿using AsyncIO;
using NetMQ;
using NetMQ.Sockets;
using UnityEngine;
using HI5;

public class VibrationListener : RunAbleThread
{
    private const string RIGHT_HAND = "RIGHT";
    private const string LEFT_HAND = "LEFT";

    protected override void Run()
    {
        ForceDotNet.Force(); // this line is needed to prevent unity freeze after one use, not sure why yet
        using (PullSocket client = new PullSocket())
        {
            client.Connect("tcp://localhost:7777");
            client.Options.ReceiveHighWatermark = 2;
            HI5_Manager.Connect();

            string message = null;
            bool gotMessage = false;
            while (Running)
            {
                gotMessage = client.TryReceiveFrameString(out message);
                if (gotMessage)
                {


                    Debug.Log("Received " + message);
                    var parts = message.Split(':');

                    int time = int.Parse(parts[1]);

                    if (parts[0].Equals(RIGHT_HAND))
                    {
                        HI5_Manager.EnableRightVibration(time);
                    } else if (parts[0].Equals(LEFT_HAND))
                    {
                        HI5_Manager.EnableLeftVibration(time);
                    }
                }
            }

            HI5_Manager.DisConnect();
    
        }
        NetMQConfig.Cleanup(); // this line is needed to prevent unity freeze after one use, not sure why yet
    }
}
