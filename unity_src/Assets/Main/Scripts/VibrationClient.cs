﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibrationClient : MonoBehaviour
{
    private VibrationListener _vibrationListener;

    private void Start()
    {
        _vibrationListener = new VibrationListener();
        _vibrationListener.Start();
    }

    private void OnDestroy()
    {
        if (_vibrationListener != null )
            _vibrationListener.Stop();
    }
}
