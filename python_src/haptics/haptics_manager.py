import logging
import weakref

import zmq

from models.hand_enum import HandEnum


class _HapticsManager:
    """Gloves wrapper class, communicates with Unity application via sockets to access the Hi5 Gloves SDK"""
    _context = None
    _socket = None

    def __init__(self):
        logging.getLogger(f"{_HapticsManager.__name__}").debug(f"{_HapticsManager.__name__}()")
        self._context = zmq.Context()
        self._socket = self._context.socket(zmq.PUSH)
        self._finalizer = weakref.finalize(self, self._cleanup)
        self._socket.bind("tcp://*:7777")

    @classmethod
    def _cleanup(cls):
        cls._zmq_cleanup(cls)

    def _zmq_cleanup(self):
        logging.getLogger(f"{_HapticsManager.__name__}").debug(
            f"{_HapticsManager.__name__}{self._zmq_cleanup.__name__}()")
        try:
            if self._socket is not None and not self._socket.closed:
                self._socket.close()
            if self._context is not None:
                self._context.destroy()
        except Exception as e:
            logging.getLogger(f"{_HapticsManager.__name__}").warning("Caught zmq socket close error ...")
            logging.getLogger(f"{_HapticsManager.__name__}").warning(e)

    def cleanup(self):
        if self._finalizer.detach():
            self._zmq_cleanup()

    def vibrate(self, hand: HandEnum, milliseconds):
        try:
            msg = f"{hand.name}:{str(int(milliseconds))}"
            self._socket.send(msg.encode('utf-8'), flags=zmq.NOBLOCK)
        except zmq.error.Again as e:
            logging.getLogger(f"{_HapticsManager.__name__}").warning(f"{self.vibrate.__name__}() zmq error caught ...")
            logging.getLogger(f"{_HapticsManager.__name__}").warning(e)

    def vibrate_right(self, milliseconds):
        self.vibrate(HandEnum.RIGHT, milliseconds)

    def vibrate_left(self, milliseconds):
        self.vibrate(HandEnum.LEFT, milliseconds)


haptics_manager_singleton = _HapticsManager()
