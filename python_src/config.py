import logging
import os
from pathlib import Path

import yaml

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
logging.getLogger("svglib.svglib").setLevel(logging.ERROR)

config_path = os.path.join(os.path.dirname(__file__), 'config.yaml')
config = yaml.safe_load(open(config_path))

# configuration files location
# src
# -- camera
# ----- presets
# -- config.py (this file)

# Build data folder structure
# data
# -- output
# ------ errors
# ------ recordings
# ------ evaluations
# ------ calibrations

src_folder_path = os.path.dirname(__file__)

camera_presets_path = src_folder_path + "/camera/presets"

data_folder_path = src_folder_path + "/../data"

output_folder_path = data_folder_path + "/output"
Path(output_folder_path).mkdir(parents=True, exist_ok=True)

errors_folder_path = output_folder_path + "/errors"
Path(errors_folder_path).mkdir(parents=True, exist_ok=True)

recordings_folder_path = output_folder_path + "/recordings"
Path(recordings_folder_path).mkdir(parents=True, exist_ok=True)

evaluations_folder_path = output_folder_path + "/evaluations"
Path(evaluations_folder_path).mkdir(parents=True, exist_ok=True)

calibrations_folder_path = output_folder_path + "/calibrations"
Path(calibrations_folder_path).mkdir(parents=True, exist_ok=True)

scenes_folder_path = output_folder_path + "/scenes"
Path(scenes_folder_path).mkdir(parents=True, exist_ok=True)

experiments_folder_path = output_folder_path + "/experiments"
Path(experiments_folder_path).mkdir(parents=True, exist_ok=True)
