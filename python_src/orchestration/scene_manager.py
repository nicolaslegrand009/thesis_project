import logging

from camera.camera import Camera
from display.projector import Projector
from models.calibration.depth_info import DepthInfo
from models.calibration.workspace import Workspace
from scenarios.scene import Scene


class SceneManager:
    scene: Scene = None

    def __init__(self, scene: Scene):
        logging.getLogger(f"{self.__class__.__name__}").debug(f"{self.__class__.__name__}()")
        self.scene = scene

    def start_loop(self, camera: Camera, projector: Projector, workspace: Workspace, depth_calibration_info: DepthInfo):
        pass
