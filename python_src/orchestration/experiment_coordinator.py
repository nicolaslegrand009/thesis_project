import json
import logging
import random
import time
from collections import deque
from datetime import datetime
from pathlib import Path
from typing import Deque

import cv2

from calibration.projector_camera_calibrator import ProjectorCameraCalibrator
from camera.camera import Camera
from camera.camera_pipeline_api import CameraPipelineAPI
from config import config
from config import experiments_folder_path
from display.display_lib import display_instruction
from display.projector import Projector
from display.second_screen_projector import SecondScreenProjector
from haptics.haptics_manager import haptics_manager_singleton
from models.calibration.depth_info import DepthInfo
from models.calibration.workspace import Workspace
from models.hand_enum import HandEnum
from models.user_group import UserGroup
from orchestration.marker_based_scene_manager import MarkerBasedSceneManager
from orchestration.scene_manager import SceneManager
from scenarios.scene import Scene
from scenarios.scene_obstacle_course import SceneObstacleCourse
from scenarios.scene_simon_says import SceneSimonSays
from scenarios.scene_trace_line import SceneTraceLine
from utils.color_utils import FlatColors


class ExperimentCoordinator:
    # --- CLASS CONSTANTS
    TEST_SCENES = [SceneTraceLine, SceneObstacleCourse, SceneObstacleCourse, SceneSimonSays]
    SCENES = [SceneTraceLine, SceneObstacleCourse, SceneSimonSays]

    TEST_PHASE_REPEAT_EXPERIMENT = config["orchestration"]["experiment_suite"]["TEST_PHASE_REPEAT_EXPERIMENT"]
    MAIN_PHASE_REPEAT_EXPERIMENT = config["orchestration"]["experiment_suite"]["MAIN_PHASE_REPEAT_EXPERIMENT"]

    MSG_START_CALIBRATION = "Press [enter] to start calibration, please keep the workspace clear of any object"
    MSG_CALIBRATION_IN_PROGRESS = "Calibrating ... please keep the workspace clear of any object"

    MSG_TEST_PHASE = "Press [enter] to start test phase, feel free to experiment a bit, this phase doesn't count."
    MSG_MAIN_PHASE = "Press [enter] to start main phase"

    MSG_START_EXPERIMENT = "Press [enter] to start the next experiment"
    MSG_EXPERIMENT_IN_PROGRESS = "Experiment in progress..."

    MSG_TWO_MARKERS = "2 fluo markers are required, one on your index finger and another one on your thumb"
    MSG_ONE_MARKER = "1 fluo marker is required on your index finger only"

    MSG_PUT_GLOVE = "Please put on the haptic glove.\nPress [enter] when you're ready"
    MSG_REMOVE_GLOVE = "Please remove the haptic glove.\nPress [enter] when you're ready"

    MSG_FILL_FORM = "Set of experiments done, please tick the checkbox and fill in the form in the next section of Google Form.\nPress [enter] when you're done filling the form."

    MSG_HAPTIC_CHECK = "Did you feel the glove vibrate?\nPress [y]es or [n]o."

    MSG_EXIT = "Experiments done, thank you! Press [enter] to quit"

    # --- INSTANCE VARIABLES
    launch_time: time.time = None
    hand_mode: HandEnum = None
    candidate_id: str = None
    candidate_group: UserGroup = None
    scene_tag: str = None

    camera: Camera = None
    projector: Projector = None
    workspace: Workspace = None
    depth_info: DepthInfo = None

    test_glove_phase_experiments: Deque[Scene] = None
    glove_phase_experiments: Deque[Scene] = None

    test_barehanded_phase_experiments: Deque[Scene] = None
    barehanded_phase_experiments: Deque[Scene] = None

    test_phases_ordered = None
    main_phases_ordered = None

    initialized_run_order = None
    execute_called_experiment_tags = None
    execution_completed_tags = None

    curr_experiment_folder = None

    def __init__(self, candidate_id: str, candidate_group: str, is_left_hand_mode=False):
        logging.getLogger(ExperimentCoordinator.__name__).debug(f"{ExperimentCoordinator.__name__}()")
        self.candidate_id = str(candidate_id)
        self.candidate_group = UserGroup.A if candidate_group.upper() == "A" else UserGroup.B
        self.hand_mode: HandEnum = HandEnum.LEFT if is_left_hand_mode else HandEnum.RIGHT
        self.launch_time = time.time()

        self.initialized_run_order = []
        self.execute_called_experiment_tags = []
        self.execution_completed_tags = []

        date_string = datetime.fromtimestamp(self.launch_time).strftime(
            '%Y-%m-%d-%Hh-%Mm-%Ss')
        self.curr_experiment_folder = f"{experiments_folder_path}/date-{date_string}_id-{self.candidate_id}_group-{self.candidate_group.name}_hand-{self.hand_mode.name}".lower()

        Path(self.curr_experiment_folder).mkdir(parents=True, exist_ok=False)

        self._initialize_experiments()
        self._dump_init_info()

    def show_instruction(self, msg: str, color=FlatColors.WHITE):
        display_instruction(msg, font_size=2, color=color)

    def show_instruction_and_wait(self, msg: str, color=FlatColors.WHITE):
        self.show_instruction(msg, color=color)
        return cv2.waitKey(0)

    def _starts_with_gloves(self):
        return self.candidate_group == UserGroup.B

    def _perform_haptic_check(self):
        k = None

        while k is None or k != ord("y"):
            haptics_manager_singleton.vibrate(self.hand_mode, 500)
            k = self.show_instruction_and_wait(self.MSG_HAPTIC_CHECK)

    def start(self):
        logging.getLogger(ExperimentCoordinator.__name__).info(f"\t {ExperimentCoordinator.start.__name__}()")
        logging.getLogger(ExperimentCoordinator.__name__).info(f"\t\t candidate_id: {self.candidate_id}")
        logging.getLogger(ExperimentCoordinator.__name__).info(f"\t\t candidate_group: {self.candidate_group.name}")
        logging.getLogger(ExperimentCoordinator.__name__).info(f"\t\t hand_mode: {self.hand_mode.name}")

        try:
            with CameraPipelineAPI() as camera:
                self._execute_calibration(camera)

                if self._starts_with_gloves():
                    self.show_instruction_and_wait(self.MSG_PUT_GLOVE)
                    self._perform_haptic_check()

                # Test Phase [0]
                self.show_instruction_and_wait(self.MSG_TEST_PHASE)
                while len(self.test_phases_ordered[0]) != 0:
                    self.execute_experiment(scene=self.test_phases_ordered[0].pop())

                # Main Phase [0]
                self.show_instruction_and_wait(self.MSG_MAIN_PHASE)
                while len(self.main_phases_ordered[0]) != 0:
                    self.execute_experiment(scene=self.main_phases_ordered[0].pop())

                # Phase switch, put/remove haptics and fill form
                if self._starts_with_gloves():
                    self.show_instruction_and_wait(self.MSG_REMOVE_GLOVE, FlatColors.PETER_RIVER)
                    self.show_instruction_and_wait(self.MSG_FILL_FORM, FlatColors.ORANGE)
                else:
                    self.show_instruction_and_wait(self.MSG_FILL_FORM, FlatColors.PETER_RIVER)
                    self.show_instruction_and_wait(self.MSG_PUT_GLOVE, FlatColors.PETER_RIVER)
                    self._perform_haptic_check()

                # Test Phase [1]
                self.show_instruction_and_wait(self.MSG_TEST_PHASE)
                while len(self.test_phases_ordered[1]) != 0:
                    self.execute_experiment(scene=self.test_phases_ordered[1].pop())

                # Main Phase [1]
                self.show_instruction_and_wait(self.MSG_MAIN_PHASE)
                while len(self.main_phases_ordered[1]) != 0:
                    self.execute_experiment(scene=self.main_phases_ordered[1].pop())

                if not self._starts_with_gloves():
                    self.show_instruction_and_wait(self.MSG_REMOVE_GLOVE, FlatColors.PETER_RIVER)

                self.show_instruction_and_wait(self.MSG_FILL_FORM, FlatColors.PETER_RIVER)

                # Experiment suite over
                self.show_instruction_and_wait(self.MSG_EXIT, FlatColors.GREEN_SEA)
        finally:
            self._dump_run_info()

    def _execute_calibration(self, camera):

        self.show_instruction_and_wait(self.MSG_START_CALIBRATION, FlatColors.ORANGE)

        self.show_instruction(self.MSG_CALIBRATION_IN_PROGRESS, FlatColors.POMEGRANATE)

        self.camera = camera
        self.projector = SecondScreenProjector()
        calibrator = ProjectorCameraCalibrator(camera=camera, projector=self.projector)
        self.workspace, self.depth_info = calibrator.start_calibration()

    def _dump_run_info(self):
        run_info = {"execute_called": self.execute_called_experiment_tags,
                    "execution_completed": self.execution_completed_tags}
        run_info_json_string = json.dumps(run_info)

        json_file_path = f"{self.curr_experiment_folder}/run_info.json"
        with open(json_file_path, "w") as text_file:
            text_file.write(run_info_json_string)

    def _dump_init_info(self):
        init_info = {
            "candidate_id": self.candidate_id,
            "candidate_group": self.candidate_group.name,
            "hand_mode": self.hand_mode.name,
            "launch_time": self.launch_time,
            "test_phase_1": [s.tag for s in self.test_phases_ordered[0]],
            "test_phase_2": [s.tag for s in self.test_phases_ordered[1]],
            "main_phase_1": [s.tag for s in self.main_phases_ordered[0]],
            "main_phase_2": [s.tag for s in self.main_phases_ordered[1]],
        }
        run_info_json_string = json.dumps(init_info)
        json_file_path = f"{self.curr_experiment_folder}/init_info.json"
        with open(json_file_path, "w") as text_file:
            text_file.write(run_info_json_string)

    def _initialize_experiments(self):
        seed_start = 777

        test_glove_phase_experiments = []
        test_barehanded_phase_experiments = []
        glove_phase_experiments = []
        barehanded_phase_experiments = []

        def get_scene_tag(phase: str, scne: Scene):
            return f"id-{self.candidate_id}_group-{self.candidate_group.name}_phase-{phase}_hand-{self.hand_mode.name}_scene-{scne.__class__.__name__}".lower()

        test_order = [{"haptic_enabled": False,
                       "phase_name": "test_barehanded",
                       "destination": test_barehanded_phase_experiments},
                      {"haptic_enabled": True,
                       "phase_name": "test_gloved",
                       "destination": test_glove_phase_experiments}]

        if self.candidate_group == UserGroup.B:
            test_order.reverse()

        for i in range(self.TEST_PHASE_REPEAT_EXPERIMENT):
            for o in test_order:
                for t_scene_class in self.TEST_SCENES:
                    test_scene = t_scene_class()
                    test_scene.tag = get_scene_tag(phase=o["phase_name"], scne=test_scene)
                    test_scene.is_haptic_enabled = o["haptic_enabled"]
                    test_scene.hand_mode = self.hand_mode
                    test_scene.seed = seed_start
                    o["destination"].append(test_scene)
                    seed_start += 37

        main_order = [{"haptic_enabled": False,
                       "phase_name": "main_barehanded",
                       "destination": barehanded_phase_experiments},
                      {"haptic_enabled": True,
                       "phase_name": "main_gloved",
                       "destination": glove_phase_experiments}]

        if self.candidate_group == UserGroup.B:
            main_order.reverse()

        for i in range(self.MAIN_PHASE_REPEAT_EXPERIMENT):
            for order in main_order:
                for main_scene_type in self.SCENES:
                    main_scene = main_scene_type()
                    main_scene.tag = get_scene_tag(phase=order["phase_name"], scne=main_scene)
                    main_scene.is_haptic_enabled = order["haptic_enabled"]
                    main_scene.hand_mode = self.hand_mode
                    main_scene.seed = seed_start
                    order["destination"].append(main_scene)
                    seed_start += 37

        random.seed(f"{self.candidate_id}{self.candidate_group}")

        random.shuffle(barehanded_phase_experiments)
        random.shuffle(glove_phase_experiments)

        self.test_glove_phase_experiments = deque(test_glove_phase_experiments)
        self.test_barehanded_phase_experiments = deque(test_barehanded_phase_experiments)
        self.barehanded_phase_experiments = deque(barehanded_phase_experiments)
        self.glove_phase_experiments = deque(glove_phase_experiments)

        main_phases = [self.barehanded_phase_experiments, self.glove_phase_experiments]
        test_phases = [self.test_barehanded_phase_experiments, self.test_glove_phase_experiments]

        if self._starts_with_gloves():
            main_phases.reverse()
            test_phases.reverse()

        self.main_phases_ordered = main_phases
        self.test_phases_ordered = test_phases

    def execute_experiment(self, scene: Scene):

        marker_count_msg = self.MSG_TWO_MARKERS if isinstance(scene, SceneObstacleCourse) else self.MSG_ONE_MARKER

        self.show_instruction_and_wait(f"{marker_count_msg}\n{self.MSG_START_EXPERIMENT}", FlatColors.PETER_RIVER)

        self.show_instruction(self.MSG_EXPERIMENT_IN_PROGRESS, FlatColors.GREEN_SEA)

        self.execute_called_experiment_tags.append(scene.tag)

        scene_coordinator: SceneManager = MarkerBasedSceneManager(scene=scene)
        scene_coordinator.start_loop(camera=self.camera,
                                     projector=self.projector,
                                     workspace=self.workspace,
                                     depth_info=self.depth_info)

        scene_dump_pickle_path = f"{self.curr_experiment_folder}/{scene.tag}.pkl"
        scene.save_to_file(scene_dump_pickle_path)

        self.execution_completed_tags.append(scene.tag)
