from typing import List

import cv2
import numpy as np

from camera.camera import Camera
from config import config
from detection.color_data_processor import ColorDataProcessor
from detection.depth_data_processor import DepthDataProcessor
from detection.detection_lib import extract_physical_objects
from detection.hand_detector import HandDetector
from display.display_lib import debug_display
from display.projector import Projector
from interaction.interaction_manager import InteractionManager
from models.calibration.depth_info import DepthInfo
from models.calibration.workspace import Workspace
from models.physical.finger_tip_object import FingerTipObject
from models.physical.hand_object import HandObject
from models.physical.physical_entity import PhysicalEntity
from models.physical.physical_object import PhysicalObject
from orchestration.scene_manager import SceneManager
from tracking.physical_entity_tracker import PhysicalEntityTracker


class HandColorBasedSceneManager(SceneManager):
    """Orchestrates processing for markerless tracking and interaction"""

    def build_virtual_image(self, virtual_objects, physical_entities, workspace: Workspace):
        virtual_ground = np.zeros(workspace.virtual_workspace_shape_scaled).astype(np.uint8)
        virtual_ground = cv2.cvtColor(virtual_ground, cv2.COLOR_GRAY2BGR)

        for v in virtual_objects:
            v.draw_self(virtual_ground)

        for p in physical_entities:
            c = p.get_latest_tracked_obj().get_centroid()
            cv2.circle(virtual_ground, center=tuple(c), radius=2, color=(255, 0, 255), thickness=-1)

        return virtual_ground

    def start_loop(self, camera: Camera, projector: Projector, workspace: Workspace, depth_info: DepthInfo):
        self.scene.initialize(workspace)
        interaction_manager = InteractionManager(workspace, depth_info)

        physical_entity_manager = PhysicalEntityTracker()
        color_data_processor = ColorDataProcessor(workspace)
        depth_data_processor = DepthDataProcessor(workspace, depth_info)
        hand_detector = HandDetector(workspace)

        frame_id = -1
        while 1:
            frame_id += 1

            color_frame, depth_frame = camera.get_next_frames(is_depth_data_post_processed=True)

            delta_depth = depth_data_processor.calculate_delta_depth(depth_frame)

            color_contours = color_data_processor.extract_color_contours(color_frame)
            depth_contours = depth_data_processor.extract_depth_contours(depth_frame, delta_depth=delta_depth)

            physical_objects: List[PhysicalObject] = extract_physical_objects(
                color_based_contours=color_contours,
                depth_based_contours=depth_contours,
                workspace=workspace)

            hand_objects: List[HandObject] = hand_detector.detect_hand_objects(physical_objects)

            finger_tips: List[FingerTipObject] = [FingerTipObject(f_tip)
                                                  for h_obj in hand_objects for f_tip in h_obj.finger_tips]

            if __debug__:
                self.___debug_show_finger_tips_masks(finger_tips, color_frame, workspace)

            active_finger_tips: List[PhysicalEntity] = physical_entity_manager.update(finger_tips)

            interaction_manager.process_interactions(physical_entities=active_finger_tips,
                                                     virtual_objects=self.scene.get_interactive_virtual_objects(),
                                                     delta_depth=delta_depth)

            self.scene.update(physical_entities=active_finger_tips,
                              frame_id=frame_id)

            virtual_image = self.build_virtual_image(
                virtual_objects=self.scene.get_rendered_virtual_objects(),
                physical_entities=active_finger_tips,
                workspace=workspace
            )

            projection_image = workspace.rebuild_projector_sized_image(virtual_image)

            projector.display_img(projection_image)

            k = cv2.waitKey(1)
            if k == ord("q"):
                self.scene.save_to_file()
                return k

    def ___debug_show_finger_tips_masks(self, finger_tips: List[FingerTipObject], color_img, workspace: Workspace):
        if config["debug_display"]["SHOW_FINGER_TIPS_MASKS"]:
            d_img = workspace.transform_to_virtual_img(color_img)
            for f in finger_tips:
                c_mask = f.get_collision_mask(workspace.virtual_workspace_shape_scaled)
                d_img[c_mask == 255] = (0, 255, 255)
            debug_display("SHOW_FINGER_TIPS_MASKS", d_img)
