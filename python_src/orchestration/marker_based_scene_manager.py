import logging
from typing import List

import cv2
import numpy as np

from camera.camera import Camera
from config import config
from detection.depth_data_processor import DepthDataProcessor
from detection.marker_detector import MarkerDetector
from display.display_lib import debug_display
from display.projector import Projector
from interaction.interaction_manager import InteractionManager
from models.calibration.depth_info import DepthInfo
from models.calibration.workspace import Workspace
from models.physical.marker_object import MarkerObject
from models.physical.physical_entity import PhysicalEntity
from orchestration.scene_manager import SceneManager
from tracking.physical_entity_tracker import PhysicalEntityTracker
from utils.color_utils import SchemeColors


class MarkerBasedSceneManager(SceneManager):
    """Orchestrates processing for marker based tracking and interaction"""

    DEPTH_FRAME_PROCESSING_INTERVAL = config["orchestration"]["marker_based"]["DEPTH_FRAME_PROCESSING_INTERVAL"]
    file_name: str = None
    output_virtual_image = None

    def redraw_virtual_image(self, physical_entities, virtual_objects, workspace):
        if self.output_virtual_image is None:
            self.output_virtual_image = np.zeros(workspace.virtual_workspace_shape_scaled).astype(np.uint8)
            self.output_virtual_image = cv2.cvtColor(self.output_virtual_image, cv2.COLOR_GRAY2BGR)
        else:
            self.output_virtual_image[:, :, :3] = SchemeColors.TRANSPARENT

        for v in virtual_objects:
            v.draw_self(self.output_virtual_image)

        for p in physical_entities:
            c = p.get_latest_loc()
            cv2.circle(self.output_virtual_image, center=tuple(c), radius=1, color=SchemeColors.NEUTRAL, thickness=-1)

    def __del__(self):
        logging.getLogger(f"{self.__class__.__name__}").debug(f"~{self.__class__.__name__}()")

    def start_loop(self, camera: Camera, projector: Projector, workspace: Workspace, depth_info: DepthInfo):
        key_pressed = None
        self.scene.initialize(workspace)
        marker_detector = MarkerDetector(workspace)
        depth_data_processor = DepthDataProcessor(workspace, depth_info)
        interaction_manager = InteractionManager(workspace, depth_info)

        physical_entity_manager: PhysicalEntityTracker = PhysicalEntityTracker()

        frame_id = -1
        while 1:
            frame_id += 1

            delta_depth = None

            is_depth_data_processed = frame_id % self.DEPTH_FRAME_PROCESSING_INTERVAL == 0

            color_frame, depth_frame = camera.get_next_frames(is_depth_frame_required=is_depth_data_processed,
                                                              is_depth_data_post_processed=is_depth_data_processed)

            if is_depth_data_processed:
                delta_depth = depth_data_processor.calculate_delta_depth(depth_frame)

            marker_objects: List[MarkerObject] = marker_detector.extract_markers(color_frame)

            active_marker_entities: List[PhysicalEntity] = physical_entity_manager.update(
                tracked_objects=marker_objects)

            if __debug__:
                self.__debug_show_active_entities(active_marker_entities, color_frame, physical_entity_manager,
                                                  workspace)

            interaction_manager.process_interactions(physical_entities=active_marker_entities,
                                                     virtual_objects=self.scene.get_interactive_virtual_objects(),
                                                     delta_depth=delta_depth)

            self.scene.update(physical_entities=active_marker_entities,
                              frame_id=frame_id)

            self.redraw_virtual_image(
                physical_entities=active_marker_entities,
                virtual_objects=self.scene.get_rendered_virtual_objects(),
                workspace=workspace
            )

            projection_image = workspace.rebuild_projector_sized_image(self.output_virtual_image)

            projector.display_img(projection_image)

            key_pressed = cv2.waitKey(1)

            if self.scene.is_finished():
                break

            if key_pressed == ord("q"):
                break
        return key_pressed

    def __debug_show_active_entities(self, active_marker_entities, color_frame, physical_entity_manager, workspace):
        if config["debug_display"]["SHOW_ACTIVE_ENTITIES"]:
            d_img = workspace.transform_to_virtual_img(color_frame)
            physical_entity_manager.draw_entities_centroids(d_img, active_marker_entities)
            debug_display("SHOW_ACTIVE_ENTITIES", d_img)
