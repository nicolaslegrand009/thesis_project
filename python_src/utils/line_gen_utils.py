# Code from https://stackoverflow.com/questions/50731785/create-random-shape-contour-using-matplotlib by ImportanceOfBeingErnest

import logging
import math

import numpy as np
from scipy.special import binom
import random

from utils.math_utils import get_closest_point_on_line

RANDOM_POINTS_RECURSION_LIMIT = 1000


def bernstein(n, k, t):
    return binom(n, k) * t ** k * (1. - t) ** (n - k)


def bezier(points, num=200):
    N = len(points)
    t = np.linspace(0, 1, num=num)
    curve = np.zeros((num, 2))
    for i in range(N):
        curve += np.outer(bernstein(N - 1, i, t), points[i])
    return curve


class Segment():
    def __init__(self, p1, p2, angle1, angle2, **kw):
        self.p1 = p1;
        self.p2 = p2
        self.angle1 = angle1;
        self.angle2 = angle2
        self.numpoints = kw.get("numpoints", 100)
        r = kw.get("r", 0.3)
        d = np.sqrt(np.sum((self.p2 - self.p1) ** 2))
        self.r = r * d
        self.p = np.zeros((4, 2))
        self.p[0, :] = self.p1[:]
        self.p[3, :] = self.p2[:]
        self.calc_intermediate_points(self.r)

    def calc_intermediate_points(self, r):
        self.p[1, :] = self.p1 + np.array([self.r * np.cos(self.angle1),
                                           self.r * np.sin(self.angle1)])
        self.p[2, :] = self.p2 + np.array([self.r * np.cos(self.angle2 + np.pi),
                                           self.r * np.sin(self.angle2 + np.pi)])
        self.curve = bezier(self.p, self.numpoints)


def get_curve(points, **kw):
    segments = []
    for i in range(len(points) - 1):
        seg = Segment(points[i, :2], points[i + 1, :2], points[i, 2], points[i + 1, 2], **kw)
        segments.append(seg)
    curve = np.concatenate([s.curve for s in segments])
    return segments, curve


def ccw_sort(p):
    d = p - np.mean(p, axis=0)
    s = np.arctan2(d[:, 0], d[:, 1])
    return p[np.argsort(s), :]


def get_circular_bezier_curve(a, rad=0.2, edgy=0):
    a = ccw_sort(a)
    a = np.append(a, np.atleast_2d(a[0, :]), axis=0)
    a, x, y = get_bezier_curve(a, edgy, rad)
    return x, y, a


def get_bezier_curve(a, edgy, rad):
    """ given an array of points *a*, create a curve through
    those points.
    *rad* is a number between 0 and 1 to steer the distance of
          control points.
    *edgy* is a parameter which controls how "edgy" the curve is,
           edgy=0 is smoothest."""
    p = np.arctan(edgy) / np.pi + .5
    d = np.diff(a, axis=0)
    ang = np.arctan2(d[:, 1], d[:, 0])
    f = lambda ang: (ang >= 0) * ang + (ang < 0) * (ang + 2 * np.pi)
    ang = f(ang)
    ang1 = ang
    ang2 = np.roll(ang, 1)
    ang = p * ang1 + (1 - p) * ang2 + (np.abs(ang2 - ang1) > np.pi) * np.pi
    ang = np.append(ang, [ang[0]])
    a = np.append(a, np.atleast_2d(ang).T, axis=1)
    s, c = get_curve(a, r=rad, method="var")
    x, y = c.T
    return a, x, y


def get_end_to_end_bezier_curve(points, start=[0, 0], end=[1, 1], rad=0.2, edgy=0, sort=True):
    if sort:
        points = sorted(points, key=lambda k: _diagonal_sort(start, end, k))
    a, x, y = get_bezier_curve(np.asarray(points), rad, edgy)
    return x, y, a


def _diagonal_sort(s, e, p):
    k = np.asarray(get_closest_point_on_line(s, e, p))
    k = k - np.asarray(s)
    return math.sqrt(k[0] ** 2 + k[1] ** 2)


def get_random_points_diagonal(n=5, scale=0.8, start=[0, 0], end=[1, 1], min_diagonal_dist=None,
                               iteration_soft_limit=0):
    d = None
    a = None
    min_distance = min_diagonal_dist or 0.9 / n
    while d is None or np.any(d < min_distance):
        a = _get_n_random_points(n)
        a_projected = [get_closest_point_on_line(start, end, p) for p in a]
        a_projected = sorted(a_projected, key=lambda k: math.sqrt(k[0] ** 2 + k[1] ** 2))
        d = np.sqrt(np.sum(np.diff(a_projected, axis=0), axis=1) ** 2)

        if iteration_soft_limit >= RANDOM_POINTS_RECURSION_LIMIT:
            logging.getLogger(get_random_points_diagonal.__name__).warning(
                f"Recursion limit of {RANDOM_POINTS_RECURSION_LIMIT} exceeded")

    return a * scale


def _get_n_random_points(n):
    return [[random.uniform(0, 1), random.uniform(0, 1)] for i in range(0, n)]


def get_random_points_diagonal_rec(n=5, scale=0.8, start=[0, 0], end=[1, 1], min_diagonal_dist=None,
                                   recursion_counter=0):
    min_distance = min_diagonal_dist or 0.9 / n
    a = _get_n_random_points(n)
    a_projected = [get_closest_point_on_line(start, end, p) for p in a]
    a_projected = sorted(a_projected, key=lambda k: math.sqrt(k[0] ** 2 + k[1] ** 2))
    d = np.sqrt(np.sum(np.diff(a_projected, axis=0), axis=1) ** 2)
    if recursion_counter >= RANDOM_POINTS_RECURSION_LIMIT:
        logging.getLogger(get_random_points_diagonal.__name__).warning(
            f"Recursion limit of {RANDOM_POINTS_RECURSION_LIMIT} exceeded")
    if np.all(d >= min_distance):
        return a * scale
    else:
        return get_random_points_diagonal_rec(n=n, scale=scale, start=start, end=end, min_diagonal_dist=min_distance,
                                              recursion_counter=recursion_counter + 1)


def get_random_points(n=5, scale=0.8, min_distance=None, recursion_counter=0):
    """ create n random points in the unit square, which are *mindst*
    apart, then scale them."""
    min_distance = min_distance or .7 / n
    a = np.random.rand(n, 2)
    d = np.sqrt(np.sum(np.diff(ccw_sort(a), axis=0), axis=1) ** 2)
    if np.all(d >= min_distance) or recursion_counter >= RANDOM_POINTS_RECURSION_LIMIT:
        return a * scale
    else:
        return get_random_points(n=n, scale=scale, min_distance=min_distance,
                                 recursion_counter=recursion_counter + 1)
