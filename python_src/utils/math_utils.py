import math

import cv2
import numpy as np


def xy_coords_to_points(x, y):
    """List of x and y coordinates to contour"""
    contour = []
    for x, y in zip(x, y):
        xx = int(x) - 1
        yy = int(y) - 1
        contour.append([xx, yy])
    return np.asarray(contour)


def pythagoras(edge1, edge2):
    """Calculate hypotenuse length"""
    return math.sqrt((edge1 ** 2) + (edge2 ** 2))


def rad_to_deg(rad):
    """Convert radians to degrees"""
    return rad * 180 / math.pi


def distance(point_a, point_b):
    """Calculate distance between two 2-dimensional points"""
    return math.sqrt((point_a[0] - point_b[0]) ** 2 + (point_a[1] - point_b[1]) ** 2)


def closest_point(p, list_of_points):
    """Return closest point to p from a list of points"""
    return list_of_points[closest_point_index(p, list_of_points)]


def closest_point_index(p, list_of_points):
    """Returns index of the point in list_of_point closest to point p"""
    list_of_points = np.asarray(list_of_points)
    deltas = list_of_points - p
    dist_2 = np.einsum('ij,ij->i', deltas, deltas)
    return np.argmin(dist_2)


def calculate_angle(s_point, e_point, angle_point):
    """Calculate angle between 3 points using cosine theorem, with angle being at angle_point"""
    a = math.sqrt((e_point[0] - s_point[0]) ** 2 +
                  (e_point[1] - s_point[1]) ** 2)
    b = math.sqrt((angle_point[0] - s_point[0]) ** 2 +
                  (angle_point[1] - s_point[1]) ** 2)
    c = math.sqrt((e_point[0] - angle_point[0]) ** 2 + (e_point[1] - angle_point[1]) ** 2)

    if c == 0 or b == 0:
        return None

    val = (b ** 2 + c ** 2 - a ** 2) / (2 * b * c)

    if val < -1 or val > 1:
        return None

    angle = math.acos(val)  # cosine theorem
    return rad_to_deg(angle)


def calculate_centroid(contour):
    """Calculate centroid of a contour"""
    m = cv2.moments(contour)
    # Centroid
    if m['m00'] == 0:
        return None  # contour area is 0

    cx = int(m['m10'] / m['m00'])
    cy = int(m['m01'] / m['m00'])
    return np.asarray([cx, cy])


def get_closest_point_on_line(start, end, point):
    a_to_p = [point[0] - start[0], point[1] - start[1]]  # Storing vector A->P
    a_to_b = [end[0] - start[0], end[1] - start[1]]  # Storing vector A->B

    atb2 = a_to_b[0] ** 2 + a_to_b[1] ** 2  # **2 means "squared"
    #   Basically finding the squared magnitude
    #   of a_to_b

    atp_dot_atb = a_to_p[0] * a_to_b[0] + a_to_p[1] * a_to_b[1]
    # The dot product of a_to_p and a_to_b

    t = atp_dot_atb / atb2  # The normalized "distance" from a to
    #   your closest point

    return start[0] + a_to_b[0] * t, start[1] + a_to_b[1] * t
    # Add the distance to A, moving
    #   towards B
