# self implemented code from algorithm described by Matt Timmermans on https://stackoverflow.com/questions/59069373/random-obstacle-generating-algortihm-in-2d-array

import random

import numpy as np


def point_str(point):
    return str(point[0]) + ":" + str(point[1])


def is_clear(course, point):
    return course[point[0], point[1]] == 1


def is_start_end_connected(sets, start_point, end_point):
    start_set = sets[point_str(start_point)]
    return point_str(end_point) in start_set


def get_neighbours(point, course_size):
    py = point[0]
    px = point[1]
    neighbours = []
    for x in range(-1, 2):
        for y in range(-1, 2):
            nx = px + x
            ny = py + y
            if (nx != px or ny != py) and (nx == px or ny == py) and nx < course_size[1] and ny < course_size[
                0] and nx >= 0 and ny >= 0:
                neighbours.append([py + y, px + x])
    return neighbours


def remove_obstacle(course, sets, point, course_size):
    neighbours = get_neighbours(point, course_size)

    course[point[0], point[1]] = 1

    connected_set = sets[point_str(point)]
    for n in neighbours:
        if is_clear(course, n):
            s_n = sets[point_str(n)]
            connected_set = connected_set.union(s_n)

    for n in neighbours:
        if is_clear(course, n):
            sets[point_str(n)] = connected_set.copy()

    sets[point_str(point)] = connected_set


def gen_course(course_size_yx, start_point, end_point):
    y_bound, x_bound = course_size_yx
    course = np.zeros(course_size_yx.astype(np.int))
    disjoint_sets = {}

    for x in range(0, x_bound):
        for y in range(0, y_bound):
            st = point_str([y, x])
            disjoint_sets[st] = set([st])

    remove_obstacle(course, disjoint_sets, start_point, course_size_yx)
    remove_obstacle(course, disjoint_sets, end_point, course_size_yx)

    while not is_start_end_connected(disjoint_sets, start_point, end_point):
        x = random.randint(0, x_bound - 1)
        y = random.randint(0, y_bound - 1)
        remove_obstacle(course, disjoint_sets, [y, x], course_size_yx)

    obstacles = []
    for x in range(0, x_bound):
        for y in range(0, y_bound):
            if not is_clear(course, [y, x]):
                obstacles.append(np.asarray([y, x]))

    return course, obstacles


def generate_obstacle_course(size_yx, obstacle_count, start_point_yx, end_point_yx, max_try=100):
    valid_course = None
    location_yx_obstacles = None
    trials = 0
    while valid_course is None and trials < max_try:
        course, obstacles = gen_course(size_yx, start_point_yx, end_point_yx)
        if len(obstacles) == obstacle_count:
            valid_course = course
            location_yx_obstacles = obstacles
        trials += 1
    if valid_course is None:
        raise Exception(f'Could not generate valid obstacle course with max_try={max_try}')
    return valid_course, location_yx_obstacles
