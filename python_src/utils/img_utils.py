import cv2
import numpy as np


def dilate(img, kernel_size=41, iterations=1, dst=None):
    """Perform dilation operation on image, increases closed shapes areas"""
    kernel = cv2.getStructuringElement(
        cv2.MORPH_ELLIPSE, (kernel_size, kernel_size))
    dilated = cv2.dilate(src=img,
                         kernel=kernel,
                         dst=dst,
                         anchor=(-1, -1),
                         iterations=iterations,
                         borderValue=cv2.BORDER_CONSTANT)
    return dilated


def erode(img, kernel_size=41, iterations=1, dst=None):
    """Perform erosion operation on image, decreases closed shapes areas"""
    kernel = cv2.getStructuringElement(
        cv2.MORPH_ELLIPSE, (kernel_size, kernel_size))
    eroded = cv2.erode(src=img,
                       kernel=kernel,
                       dst=dst,
                       anchor=(-1, -1),
                       iterations=iterations,
                       borderValue=cv2.BORDER_CONSTANT)
    return eroded


def gaussian_blur(img, kernel_size=5, dst=None):
    return cv2.GaussianBlur(src=img,
                            ksize=(kernel_size,
                                   kernel_size),
                            sigmaX=0,
                            dst=dst)


def get_bounding_values(points):
    """Get bounding coordinate values from list of points"""
    x_list = [p[0] for p in points]
    y_list = [p[1] for p in points]
    return np.min(x_list), np.min(y_list), np.max(x_list), np.max(y_list)


def scale_image(img, factor, dst=None):
    """Resize image"""
    width = int(img.shape[1] * factor)
    height = int(img.shape[0] * factor)
    dim = (width, height)
    resized = cv2.resize(src=img, dsize=dim, dst=dst, interpolation=cv2.INTER_AREA)
    return resized


def are_masks_matching(mask1, mask2, matching_threshold):
    """Check if two masks have enough common intersection area"""
    overlap = np.logical_and(mask1, mask2)
    mask1_nonzero_count = np.count_nonzero(mask1)
    overlap_length = np.count_nonzero(overlap)
    score = (overlap_length / mask1_nonzero_count) if mask1_nonzero_count > 0 else 0
    is_match = score >= matching_threshold
    return is_match, score


def rescale_depth_value(measurement_in_meters, input_depth_scale):
    return measurement_in_meters / input_depth_scale


def convert_contour_to_mask(contours, shape):
    mask = np.zeros(shape)
    cv2.drawContours(mask, contours, -1, 255, -1)
    return mask


def extract_hsv_filtered_masks(img, hue_range_list):
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    return map(lambda hue_range: cv2.inRange(hsv_img, np.asarray(hue_range[0]), np.asarray(hue_range[1])),
               hue_range_list)


def extract_hsv_filtered_mask(img, hue_range):
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    return cv2.inRange(hsv_img, np.asarray(hue_range[0]), np.asarray(hue_range[1]))


def combine_masks(masks):
    masks = list(masks)
    combined_mask = masks[0]
    for i in range(1, len(masks)):
        combined_mask = np.where(combined_mask != 0, combined_mask, masks[i])
    return combined_mask


def add_border(img, color, border_size=5):
    top, bottom, left, right = [border_size] * 4
    return cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT,
                              value=color)


def write_on_img(img, msg, location_xy: np.ndarray, color=(255, 255, 255), font_scale=1, thickness=1):
    font = cv2.FONT_HERSHEY_SIMPLEX
    line_type = cv2.LINE_AA
    cv2.putText(img, msg,
                tuple(location_xy.astype(np.int)),
                font,
                thickness=thickness,
                fontScale=font_scale,
                color=tuple(color),
                lineType=line_type)


def rotate_img(img, angle, scale=1):
    (h, w) = img.shape[:2]
    # calculate the center of the image
    center = (w / 2, h / 2)
    rot_mat = cv2.getRotationMatrix2D(center, angle, scale)
    return cv2.warpAffine(img, rot_mat, (w, h))
