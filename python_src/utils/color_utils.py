# colors from: https://www.webnots.com/flat-ui-color-codes/

class FlatColors:
    TURQUOISE = [208, 224, 64]
    GREEN_SEA = [133, 160, 22]
    EMERALD = [113, 204, 46]
    NEPHRITIS = [96, 174, 39]
    PETER_RIVER = [219, 152, 52]
    BELIZE_HOLE = [185, 128, 41]
    AMETHYST = [182, 89, 155]
    WISTERIA = [173, 68, 142]
    WET_ASPHALT = [94, 73, 52]
    MIDNIGHT_BLUE = [80, 62, 44]
    SUN_FLOWER = [15, 196, 241]
    ORANGE = [18, 156, 243]
    CARROT = [34, 126, 230]
    PUMPKIN = [0, 84, 211]
    ALIZARIN = [60, 76, 231]
    POMEGRANATE = [43, 57, 192]
    CLOUDS = [241, 240, 236]
    SILVER = [199, 195, 189]
    CONCRETE = [166, 165, 149]
    ASBESTOS = [141, 140, 127]
    WHITE = [255, 255, 255]
    BLACK = [0, 0, 0, 0]


class SchemeColors:
    NEUTRAL = [150, 200, 200]  #
    CORRECT = [50, 200, 127]  # green
    WRONG = [50, 50, 255]  # red
    LIGHT_WRONG = [150, 150, 255]  # light red
    SECONDARY_YELLOW = [0, 197, 255]  # yellow
    SECONDARY_PINK = [127, 50, 200]  # pink
    SECONDARY_ORANGE = [50, 127, 255]  # orange
    TRANSPARENT = [0, 0, 0]
