import math
from typing import List

import numpy as np

from models.calibration.workspace import Workspace
from models.hand_enum import HandEnum
from models.physical.physical_entity import PhysicalEntity
from models.virtual.virtual_circle import VirtualCircle
from models.virtual.virtual_random_line import VirtualRandomLine
from models.virtual.virtual_text import VirtualText
from scenarios.scene import Scene
from utils.color_utils import SchemeColors
from utils.math_utils import closest_point, distance


class SceneTraceLine(Scene):
    """Trace the line"""

    # --- CLASS CONSTANTS

    # object properties
    INDICATOR_POINT_CIRCLE_THICKNESS = 1

    INDICATOR_CIRCLE_RADIUS = 30
    indicator_circle_radius_rescaled = None

    LINE_GEN_NUM_POINTS = 7  # 7
    LINE_GEN_EDGY = 0.1  # 0.05
    LINE_GEN_RAD = 0.2  # 0.2
    LINE_PADDING = 50
    LINE_VISUAL_THICKNESS = 1
    LINE_INITIAL_COLOR = [0, 255, 127]
    LINE_COLOR_CORRECT = [0, 255, 127]
    LINE_COLOR_WRONG = [50, 50, 255]

    UI_TEXT_SCALE = 1
    ui_text_scale_rescaled = None

    UI_TEXT_MSG_IN_PROGRESS = "In progress ..."
    UI_TEXT_MSG_FINISHED = "Good job!"
    UI_TEXT_MSG_INIT = "Touch the circle and trace the line"

    # haptic feedback
    DEVIATION_VIBRATION_DURATION = 100
    DEVIATION_THRESHOLD_UPPER = 15

    # --- INSTANCE VARIABLES

    # virtual objects
    start_circle: VirtualCircle = None
    end_circle: VirtualCircle = None
    trace_line: VirtualRandomLine = None
    ui_text: VirtualText = None
    start_text: VirtualText = None
    end_text: VirtualText = None

    def __init__(self):
        super(SceneTraceLine, self).__init__()

    def initialize(self, workspace: Workspace):
        super(SceneTraceLine, self).initialize(workspace)

        # rescaled attributes
        self.ui_text_scale_rescaled = self.rescale_to_workspace(self.UI_TEXT_SCALE)
        self.indicator_circle_radius_rescaled = self.rescale_to_workspace(self.INDICATOR_CIRCLE_RADIUS)

        self._add_initial_objects()

    def start(self):
        super(SceneTraceLine, self).start()
        print(f"{SceneTraceLine.__name__}.{self.start.__name__}()")

        self.trace_line.color = self.LINE_COLOR_WRONG
        self.trace_line.hovered_color = self.LINE_COLOR_CORRECT
        self.trace_line.touched_color = self.LINE_COLOR_CORRECT

        self.start_circle.set_all_colors(SchemeColors.TRANSPARENT)
        self.start_text.set_all_colors(SchemeColors.TRANSPARENT)
        self.end_circle.set_all_colors(SchemeColors.CORRECT)

        self.ui_text.message = self.UI_TEXT_MSG_IN_PROGRESS
        self.ui_text.color = SchemeColors.CORRECT

    def finish(self):
        super(SceneTraceLine, self).finish()
        print(f"{SceneTraceLine.__name__}.{self.finish.__name__}()")

        self.trace_line.set_all_colors(SchemeColors.CORRECT)

        self.start_circle.set_all_colors(SchemeColors.CORRECT)
        self.end_circle.set_all_colors(SchemeColors.CORRECT)

        self.ui_text.set_all_colors(SchemeColors.CORRECT)
        self.ui_text.message = self.UI_TEXT_MSG_FINISHED

        self.clear_interactive_virtual_objects()

    def update(self, physical_entities: List[PhysicalEntity], frame_id: int):
        super(SceneTraceLine, self).update(physical_entities, frame_id)
        if self.is_started() and not self.is_finished():
            distances = [distance(p.get_latest_loc(), closest_point(p.get_latest_loc(), self.trace_line.points))
                         for p in physical_entities]

            if len(distances) != 0:
                min_dist = np.min(distances)
                factor = 1 if min_dist > self.DEVIATION_THRESHOLD_UPPER else (min_dist / self.DEVIATION_THRESHOLD_UPPER)
                factor = math.sqrt(factor)
                adaptive_vibration_duration = self.DEVIATION_VIBRATION_DURATION * factor
                self.vibrate(adaptive_vibration_duration)

    def _add_initial_objects(self):

        self.trace_line: VirtualRandomLine = self._generate_initial_trace_line()

        starting_point = self.trace_line.points[0]
        end_point = self.trace_line.points[-1]

        self.start_text = self._generate_start_text(starting_point)
        self.end_text = self._generate_end_text(end_point)

        self.start_circle = self._generate_start_circle(start_point=starting_point)
        self.start_circle.on_initial_hover_function = self._on_start_circle_touch

        self.end_circle = self._generate_end_circle(end_point=end_point)
        self.end_circle.on_initial_hover_function = self._on_end_circle_touch

        self.ui_text = self._generate_initial_text()

        self.interactive_virtual_objects.extend([
            self.trace_line,
            self.start_circle,
            self.end_circle,
        ])

        self.rendered_virtual_objects.extend([
            self.start_text,
            self.trace_line,
            self.start_circle,
            self.end_circle,
            self.ui_text
        ])

    def _generate_end_text(self, end_point):
        end_text_loc = np.asarray(end_point) + [0,
                                                self.indicator_circle_radius_rescaled + 10 * self.ui_text_scale_rescaled]
        return VirtualText(end_text_loc, "End", scale=self.ui_text_scale_rescaled, rotated=True,
                           center_text=True)

    def _generate_start_text(self, starting_point):
        start_text_loc = np.asarray(starting_point) - [0,
                                                       self.indicator_circle_radius_rescaled + 20 * self.ui_text_scale_rescaled]
        return VirtualText(start_text_loc, "Start", scale=self.ui_text_scale_rescaled, rotated=True,
                           center_text=True)

    def _generate_initial_trace_line(self):
        line_padding = self.LINE_PADDING
        line_bounds = np.asarray(self.workspace.virtual_workspace_shape_scaled)[::-1]
        line_bounds = line_bounds - (line_padding * 2)
        offset = np.asarray([line_padding, line_padding])

        start_point = [0, 0]
        end_point = [1, 1]

        if self.MIRROR_FOR_LEFT_HAND and self.hand_mode == HandEnum.LEFT:
            start_point = [1, 0]
            end_point = [0, 1]

        trace_line: VirtualRandomLine = VirtualRandomLine(
            start_point=start_point,
            end_point=end_point,
            size=line_bounds,
            offset_xy=offset,
            num_points=self.LINE_GEN_NUM_POINTS,
            edgy=self.LINE_GEN_EDGY,
            rad=self.LINE_GEN_RAD,
            thickness=self.LINE_VISUAL_THICKNESS)

        trace_line.set_all_colors(self.LINE_INITIAL_COLOR)

        trace_line.is_touchable = False
        trace_line.is_static = True

        return trace_line

    def _generate_start_circle(self, start_point):
        start_circle = VirtualCircle(location_xy=start_point,
                                     radius=self.indicator_circle_radius_rescaled,
                                     visual_thickness=self.INDICATOR_POINT_CIRCLE_THICKNESS)

        start_circle.set_all_colors(SchemeColors.CORRECT)

        return start_circle

    def _generate_end_circle(self, end_point):
        end_circle = VirtualCircle(location_xy=end_point,
                                   radius=self.indicator_circle_radius_rescaled,
                                   visual_thickness=self.INDICATOR_POINT_CIRCLE_THICKNESS)

        end_circle.set_all_colors(SchemeColors.SECONDARY_YELLOW)

        return end_circle

    def _generate_initial_text(self):

        loc = np.asarray(self.workspace.virtual_workspace_shape_scaled[::-1])
        loc = loc - 20

        ui_text = VirtualText(location_xy=loc,
                              message=self.UI_TEXT_MSG_INIT,
                              scale=self.ui_text_scale_rescaled,
                              rotated=True)
        return ui_text

    def _on_start_circle_touch(self):
        if self.start_time is None:
            self.start()

    def _on_end_circle_touch(self):
        if self.is_started() and self.finish_time is None:
            self.finish()
