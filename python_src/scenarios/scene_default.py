from models.virtual.virtual_rectangle import VirtualRectangle
from scenarios.scene import Scene
from utils.color_utils import SchemeColors


class SceneDefault(Scene):

    def __init__(self):
        super(SceneDefault, self).__init__()
        self._add_initial_objects()

    def _add_initial_objects(self):
        to_test = [SchemeColors.NEUTRAL, SchemeColors.CORRECT, SchemeColors.WRONG, SchemeColors.LIGHT_WRONG, SchemeColors.SECONDARY_YELLOW,
                   SchemeColors.SECONDARY_PINK, SchemeColors.SECONDARY_ORANGE]

        r = 0
        col = 0
        for i, c in enumerate(to_test):
            rec = VirtualRectangle(location_xy=[col * 50, r * 50], size_xy=[50, 50])
            rec.set_all_colors(c)
            self.rendered_virtual_objects.append(rec)

            col += 1

            if col == 5:
                col = 0
                r += 1
