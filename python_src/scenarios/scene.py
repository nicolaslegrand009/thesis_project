import copy
import logging
import time
from datetime import datetime
from pathlib import Path
from typing import Dict
from typing import List

import dill

from config import config, scenes_folder_path
from haptics.haptics_manager import haptics_manager_singleton
from models.calibration.workspace import Workspace
from models.hand_enum import HandEnum
from models.metrics.location_time_frame import LocationTimeFrame
from models.physical.physical_entity import PhysicalEntity
from models.virtual.virtual_object import VirtualObject
import random

SCENE_DUMP_FILE_NAME = "scene_dump.pkl"
PICKLE_PROTOCOL_VERSION = config["logging"]["pickle"]["PROTOCOL_VERSION"]


class Scene:
    """Abstract class to implement use cases"""

    # --- CLASS CONSTANTS

    TRACK_PHYS_OBJ_LOC: bool = config["scenarios"]["TRACK_PHYS_OBJ_LOC"]
    MIRROR_FOR_LEFT_HAND: bool = config["scenarios"]["MIRROR_FOR_LEFT_HAND"]
    FINISH_VIBRATION_DURATION = 300

    # --- INSTANCE VARIABLES
    workspace: Workspace = None
    interactive_virtual_objects: List[VirtualObject] = None
    rendered_virtual_objects: List[VirtualObject] = None

    # tag
    tag = None
    seed = None

    # haptics enabled
    is_haptic_enabled = False
    hand_mode = HandEnum.RIGHT

    # metrics
    init_time = None
    start_time = None
    finish_time = None
    physical_entities_location_history: Dict[int, List[LocationTimeFrame]] = None

    def __init__(self):
        logging.getLogger(self.__class__.__name__).debug(f"{self.__class__.__name__}()")
        self.interactive_virtual_objects = []
        self.rendered_virtual_objects = []
        self.physical_entities_location_history: Dict[int, List[LocationTimeFrame]] = {}

    def vibrate(self, milliseconds):
        if self.is_haptic_enabled:
            haptics_manager_singleton.vibrate(self.hand_mode, milliseconds)

    def get_interactive_virtual_objects(self) -> List[VirtualObject]:
        return self.interactive_virtual_objects

    def get_rendered_virtual_objects(self) -> List[VirtualObject]:
        return self.rendered_virtual_objects

    def clear_interactive_virtual_objects(self):
        self.interactive_virtual_objects: List[VirtualObject] = []

    def clear_rendered_virtual_objects(self):
        self.rendered_virtual_objects: List[VirtualObject] = []

    def initialize(self, workspace: Workspace):
        self.init_time = time.time()
        self.workspace = workspace

        if self.seed is not None:
            random.seed(self.seed
                        )
            print(f"{self.__class__.__name__}: {self.seed}")

    def rescale_to_workspace(self, attr):
        return attr * self.workspace.virtual_workspace_scale

    def update(self, physical_entities: List[PhysicalEntity], frame_id: int):
        if self.TRACK_PHYS_OBJ_LOC:
            location_history = self.physical_entities_location_history
            for p in physical_entities:
                if p.id not in self.physical_entities_location_history:
                    location_history[p.id] = []
                loc_time = p.get_latest_loc_time()
                location_history[p.id].append(
                    LocationTimeFrame(centroid=loc_time.centroid,
                                      frame_id=frame_id,
                                      timestamp=loc_time.timestamp))

    def start(self):
        logging.getLogger(self.__class__.__name__).debug(f"{self.start.__name__}()")
        self.start_time = time.time()

    def finish(self):
        logging.getLogger(self.__class__.__name__).debug(f"{self.finish.__name__}()")
        self.finish_time = time.time()
        self.vibrate(self.FINISH_VIBRATION_DURATION)

    def is_started(self):
        return self.start_time is not None

    def is_finished(self):
        return self.finish_time is not None

    def _dump_file_name(self):
        date_string = datetime.fromtimestamp(self.init_time).strftime(
            '%Y-%m-%d---%Hh-%Mm-%Ss')
        state = "finished" if self.is_finished() else "unfinished" if self.is_started() else "unstarted"

        folder_path = f"{scenes_folder_path}/{self.__class__.__name__}"
        Path(folder_path).mkdir(parents=True, exist_ok=True)

        file_name = f"{date_string}---{state}"

        if self.tag:
            file_name = f"{file_name}-{self.tag}"

        return f"{folder_path}/{file_name}.pkl"

    def save_to_file(self, file_path=None):
        scene_cpy = copy.deepcopy(self)
        scene_cpy.haptic_manager = None

        if file_path is None:
            file_path = self._dump_file_name()

        with open(file_path, "wb") as output_file:
            dill.dump(scene_cpy, output_file, protocol=PICKLE_PROTOCOL_VERSION)

    def __del__(self):
        logging.getLogger(self.__class__.__name__).debug(f"~{self.__class__.__name__}()")
