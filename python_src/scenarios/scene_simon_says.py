import logging
import random
import time
from functools import partial
from typing import List

import numpy as np

from config import config
from models.calibration.workspace import Workspace
from models.metrics.round_info import RoundInfo
from models.physical.physical_entity import PhysicalEntity
from models.virtual.virtual_circle import VirtualCircle
from models.virtual.virtual_rectangle import VirtualRectangle
from models.virtual.virtual_text import VirtualText
from scenarios.scene import Scene
from utils.color_utils import SchemeColors


class SceneSimonSays(Scene):
    """Simon says"""

    # --- CLASS CONSTANTS

    TOTAL_ROUND_COUNT = config["scenarios"]["simon_says"]["TOTAL_ROUNDS"]
    NEXT_ROUND_TIMEOUT = config["scenarios"]["simon_says"]["NEXT_ROUND_TIMEOUT"]

    # virtual objects properties
    CORRECT_ANSWER_COLOR = SchemeColors.CORRECT
    WRONG_ANSWER_COLOR = SchemeColors.WRONG

    BLOCK_COUNT = 3
    BLOCK_COLORS = [SchemeColors.SECONDARY_YELLOW, SchemeColors.SECONDARY_PINK, SchemeColors.SECONDARY_ORANGE]
    BLOCK_HOVER_COLORS = [SchemeColors.SECONDARY_YELLOW, SchemeColors.SECONDARY_PINK, SchemeColors.SECONDARY_ORANGE]
    BLOCK_THICKNESS = 2
    BLOCK_TOUCH_CONFIRM_DELAY = 1
    BLOCK_HEIGHT = 100  # rescaled
    BLOCK_PADDING_Y = 300  # rescaled

    INSTRUCTION_TEXT_SCALE = 1  # rescaled

    START_BTN_PADDING_Y = 25  # rescaled
    START_BTN_SIZE = np.asarray([400, 100])  # rescaled

    INDICATOR_BLOB_RADIUS = 40  # rescaled

    INDICATOR_BLOB_LOC_REV = np.asarray([550, 45])  # rescaled

    # instruction text values
    SCENE_COMPLETED_TEXT = "Scene completed!"
    SIMON_SAYS_TEXT = "Simon says touch this color"
    START_TEXT = "Hover on the colored block to start round"
    CORRECT_ANSWER_TEXT = "Correct answer!"
    WRONG_ANSWER_TEXT = "Wrong answer!"

    # haptics
    TOUCH_VIBRATION_DURATION = 300
    CORRECT_TOUCH_VIBRATION_DURATION = 500
    WRONG_TOUCH_VIBRATION_DURATION = 1000

    # --- INSTANCE VARIABLES

    # virtual_objects
    virtual_blocks: List[VirtualRectangle] = None
    instruction_text: VirtualText = None
    indicator_blob: VirtualCircle = None
    start_btn: VirtualRectangle = None

    # virtual object properties
    block_touch_functions = None
    block_touched = None
    block_touched_execution_time = None

    # rescaled object properties
    instruction_text_scale_rescaled = None
    indicator_bloc_loc_rev_rescaled = None
    indicator_blob_radius_rescaled = None
    start_btn_size_rescaled = None
    start_btn_padding_y_rescaled = None
    block_height_rescaled = None
    block_padding_y_rescaled = None

    # round information
    current_round_index = None
    current_round_start_time = None
    current_round_correct_answer = None
    next_round_setup_start_time = None
    round_info_history: List[RoundInfo] = None

    def __init__(self):
        super(SceneSimonSays, self).__init__()
        self.round_info_history = []
        self.current_round_index = 0
        self.block_touch_functions = [partial(self._on_block_touched, i) for i in range(self.BLOCK_COUNT)]

    def initialize(self, workspace: Workspace):
        super(SceneSimonSays, self).initialize(workspace)
        self.instruction_text_scale_rescaled = self.rescale_to_workspace(self.INSTRUCTION_TEXT_SCALE)
        self.block_height_rescaled = self.rescale_to_workspace(self.BLOCK_HEIGHT)
        self.block_padding_y_rescaled = self.rescale_to_workspace(self.BLOCK_PADDING_Y)
        self.indicator_bloc_loc_rev_rescaled = self.rescale_to_workspace(self.INDICATOR_BLOB_LOC_REV)
        self.indicator_blob_radius_rescaled = self.rescale_to_workspace(self.INDICATOR_BLOB_RADIUS)
        self.start_btn_size_rescaled = self.rescale_to_workspace(self.START_BTN_SIZE)
        self.start_btn_padding_y_rescaled = self.rescale_to_workspace(self.START_BTN_PADDING_Y)

        self._add_initial_objects()
        self._setup_next_round()

    def finish(self):
        super(SceneSimonSays, self).finish()

        self.instruction_text.message = self.SCENE_COMPLETED_TEXT
        self.instruction_text.set_all_colors(SchemeColors.CORRECT)
        self.indicator_blob.set_all_colors(SchemeColors.TRANSPARENT)

    def _setup_next_round(self):
        self.clear_rendered_virtual_objects()
        self.clear_interactive_virtual_objects()
        self.instruction_text.message = self.START_TEXT
        self.instruction_text.set_all_colors(SchemeColors.NEUTRAL)

        self.interactive_virtual_objects.extend([self.start_btn])
        self.rendered_virtual_objects.extend([self.instruction_text, self.start_btn])

    def update(self, physical_entities: List[PhysicalEntity], frame_id: int):
        super(SceneSimonSays, self).update(physical_entities, frame_id)
        if self.is_started() and (not self.is_finished()) and self.next_round_setup_start_time is not None and (
                time.time() >= self.next_round_setup_start_time):
            self.next_round_setup_start_time = None
            self._setup_next_round()

        if self.block_touched is not None and time.time() > self.block_touched_execution_time:
            self._confirm_block_touch(self.block_touched)

    def _start_round(self):
        logging.getLogger(self.__class__.__name__).debug(f"{self._start_round.__name__}()")

        if not self.is_started():
            self.start()

        self.clear_interactive_virtual_objects()
        self.clear_rendered_virtual_objects()

        self.interactive_virtual_objects.extend(self.virtual_blocks)
        self.rendered_virtual_objects.extend(self.virtual_blocks)
        self.rendered_virtual_objects.extend([self.instruction_text, self.indicator_blob])

        self.current_round_correct_answer = random.randint(0, self.BLOCK_COUNT - 1)
        to_touch_color = self.BLOCK_COLORS[self.current_round_correct_answer]
        self.instruction_text.message = self.SIMON_SAYS_TEXT
        self.instruction_text.set_all_colors(SchemeColors.NEUTRAL)
        self.indicator_blob.set_all_colors(to_touch_color)

        for i in range(0, self.BLOCK_COUNT):
            b = self.virtual_blocks[i]
            b.on_touch_function = self.block_touch_functions[i]
            b.color = self.BLOCK_COLORS[i]
            b.touched_color = self.BLOCK_COLORS[i]

        self.current_round_start_time = time.time()

    def _add_initial_objects(self):
        self.instruction_text = self._generate_instruction_text()
        self.start_btn = self._generate_start_button()

        self.virtual_blocks = self._generate_color_blocks()
        self.indicator_blob = self._generate_indicator_blob()

    def _generate_start_button(self):
        workspace_yx = self.workspace.virtual_workspace_shape_scaled
        btn_location_xy = np.subtract(np.multiply(workspace_yx[::-1], 0.5),
                                      np.multiply(self.start_btn_size_rescaled, 0.5))

        btn_location_xy[1] = self.start_btn_padding_y_rescaled
        start_btn = VirtualRectangle(location_xy=btn_location_xy, size_xy=self.start_btn_size_rescaled,
                                     visual_thickness=2)
        start_btn.set_all_colors(SchemeColors.CORRECT)
        start_btn.touched_color = SchemeColors.CORRECT
        start_btn.on_hover_function = lambda: [
            self.vibrate(self.TOUCH_VIBRATION_DURATION), self._on_start_round_btn_hover()]

        return start_btn

    def _generate_indicator_blob(self):
        workspace_yx = self.workspace.virtual_workspace_shape_scaled

        loc_xy = np.subtract(workspace_yx[::-1], self.indicator_bloc_loc_rev_rescaled)

        vc = VirtualCircle(location_xy=loc_xy, radius=self.indicator_blob_radius_rescaled)
        return vc

    def _generate_instruction_text(self):
        loc = self.workspace.virtual_workspace_shape_scaled[::-1]
        loc = loc - 20
        t = VirtualText(location_xy=loc, scale=self.instruction_text_scale_rescaled,
                        message=self.START_TEXT,
                        rotated=True)
        t.set_all_colors(SchemeColors.NEUTRAL)
        return t

    def _generate_color_blocks(self):
        total_block_space = 0.7
        total_inter_block_space = 1 - total_block_space
        inter_block_count = self.BLOCK_COUNT + 1

        workspace_size_yx = self.workspace.virtual_workspace_shape_scaled

        block_width = (total_block_space / self.BLOCK_COUNT) * workspace_size_yx[1]
        space_size = (total_inter_block_space / inter_block_count) * workspace_size_yx[1]

        block_objects = []
        for i in range(0, self.BLOCK_COUNT):
            loc_x = block_width * i + space_size * (i + 1)
            vr = VirtualRectangle(location_xy=[loc_x, self.block_padding_y_rescaled],
                                  size_xy=[block_width, self.block_height_rescaled],
                                  visual_thickness=self.BLOCK_THICKNESS)
            vr.color = self.BLOCK_COLORS[i]
            vr.hovered_color = self.BLOCK_HOVER_COLORS[i]
            vr.touched_color = SchemeColors.NEUTRAL

            block_objects.append(vr)

        return block_objects

    def _confirm_block_touch(self, touched_block_id):
        logging.getLogger(self.__class__.__name__).debug("\ttouched_block_id:" + str(touched_block_id))
        logging.getLogger(self.__class__.__name__).debug("\tcorrect_block_id:" + str(self.current_round_correct_answer))

        self.block_touched = None
        self.block_touched_execution_time = None

        if self.current_round_start_time is not None:
            for block in self.virtual_blocks:
                block.on_touch_function = None

            round_info = RoundInfo(start_time=self.current_round_start_time,
                                   end_time=time.time(),
                                   correct_answer=self.current_round_correct_answer,
                                   user_answer=touched_block_id)

            self.current_round_start_time = None

            if round_info.is_round_won():
                self.vibrate(self.CORRECT_TOUCH_VIBRATION_DURATION)
                self.instruction_text.message = self.CORRECT_ANSWER_TEXT
                self.instruction_text.set_all_colors(self.CORRECT_ANSWER_COLOR)
                self.virtual_blocks[touched_block_id].color = self.CORRECT_ANSWER_COLOR
            else:
                self.vibrate(self.WRONG_TOUCH_VIBRATION_DURATION)
                self.instruction_text.message = self.WRONG_ANSWER_TEXT
                self.instruction_text.set_all_colors(self.WRONG_ANSWER_COLOR)
                self.virtual_blocks[touched_block_id].color = self.WRONG_ANSWER_COLOR

            self.indicator_blob.set_all_colors(SchemeColors.TRANSPARENT)

            self.round_info_history.append(round_info)

            self.current_round_index += 1

            if self.current_round_index >= self.TOTAL_ROUND_COUNT:
                self.finish()
            else:
                self.next_round_setup_start_time = time.time() + self.NEXT_ROUND_TIMEOUT

    def _on_block_touched(self, touched_block_id):
        if self.current_round_start_time is not None and self.block_touched is None:
            self.block_touched = touched_block_id
            self.block_touched_execution_time = time.time() + self.BLOCK_TOUCH_CONFIRM_DELAY

    def _on_start_round_btn_hover(self):
        self._start_round()
