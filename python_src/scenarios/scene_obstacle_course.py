import random
from typing import List

import numpy as np
from scipy.spatial import distance as sci_distance

from config import config
from interaction.interaction_lib import are_masks_overlapping
from models.calibration.workspace import Workspace
from models.hand_enum import HandEnum
from models.metrics.pebble_state import PebbleState
from models.physical.physical_entity import PhysicalEntity
from models.virtual.virtual_circle import VirtualCircle
from models.virtual.virtual_pinch_draggable_ellipse import VirtualPinchDraggableEllipse
from models.virtual.virtual_rectangle import VirtualRectangle
from models.virtual.virtual_text import VirtualText
from scenarios.scene import Scene
from utils.color_utils import SchemeColors
from utils.math_utils import distance, pythagoras
from utils.obstacle_course_utils import generate_obstacle_course


class SceneObstacleCourse(Scene):
    """Drag and drop, avoid obstacles"""

    # --- CLASS CONSTANTS

    # virtual objects properties
    OBSTACLE_GEN_MIN_RATIO = 0.50
    OBSTACLE_GEN_MAX_TRY = 10000
    OBSTACLE_GEN_RND_SIZE_COEF = 1.2
    OBSTACLE_GEN_CHANCE_MAGNIFY = 0.8
    OBSTACLE_GEN_CHANCE_LOCATION_REVERT = 0.2

    OBSTACLE_THICKNESS = 1
    OBSTACLE_COLOR = SchemeColors.LIGHT_WRONG
    OBSTACLE_COLLISION_COLOR = SchemeColors.WRONG
    OBSTACLE_EDGE_DELTA = 10
    OBSTACLE_SIZE_XY = np.asarray([160, 160])

    DROP_ZONE_THICKNESS = 1
    DROP_ZONE_COLOR = SchemeColors.CORRECT
    DROP_ZONE_FINISH_COLOR = SchemeColors.CORRECT
    DROP_ZONE_FINISH_OVERLAP_RATIO = 0.9
    DROP_ZONE_RADIUS = 75

    DROP_TEXT_SCALE = 0.9

    DROP_ZONE_TEXT_FINISHED = "Good job!"
    DROP_ZONE_TEXT_IN_PROGRESS = "Drop here"

    PEBBLE_RADIUS = 70
    PEBBLE_THICKNESS = 1
    PEBBLE_COLOR = SchemeColors.SECONDARY_YELLOW
    PEBBLE_PINCHED_COLOR = SchemeColors.SECONDARY_ORANGE
    PEBBLE_EDGE_DELTA = 30
    PEBBLE_TRACK_LOCATION_ON = config["scenarios"]["obstacle_course"]["TRACK_PEBBLE_LOC"]

    # haptics
    COLLISION_VIBRATION_DURATION = 200
    INITIAL_GRAB_VIBRATION_DURATION = 200

    # --- INSTANCE VARIABLES
    start_point_xy = None
    end_point_xy = None
    obstacle_edge_points = None

    # virtual objects
    pebble: VirtualPinchDraggableEllipse = None
    drop_zone: VirtualCircle = None
    drop_text: VirtualText = None
    obstacles: List[VirtualRectangle] = None

    # rescaled object properties
    drop_text_scale_rescaled = None
    drop_zone_radius_rescaled = None
    obstacle_size_xy_rescaled = None
    pebble_radius_rescaled = None

    # metrics
    pebble_is_colliding_count = None
    pebble_is_not_colliding_count = None
    pebble_state_history: List[PebbleState] = None

    def __init__(self):
        super(SceneObstacleCourse, self).__init__()
        self.pebble_state_history = []
        self.pebble_is_colliding_count = 0
        self.pebble_is_not_colliding_count = 0

    def initialize(self, workspace: Workspace):
        super(SceneObstacleCourse, self).initialize(workspace)

        self.obstacle_size_xy_rescaled = self.rescale_to_workspace(self.OBSTACLE_SIZE_XY)
        self.drop_text_scale_rescaled = self.rescale_to_workspace(self.DROP_TEXT_SCALE)
        self.drop_zone_radius_rescaled = self.rescale_to_workspace(self.DROP_ZONE_RADIUS)
        self.pebble_radius_rescaled = self.rescale_to_workspace(self.PEBBLE_RADIUS)

        self._add_initial_objects()

    def start(self):
        super(SceneObstacleCourse, self).start()

    def finish(self):
        super(SceneObstacleCourse, self).finish()
        self.drop_text.message = self.DROP_ZONE_TEXT_FINISHED
        self.drop_text.color = self.DROP_ZONE_FINISH_COLOR
        self.drop_zone.color = self.DROP_ZONE_FINISH_COLOR
        self.interactive_virtual_objects = []

    def update(self, physical_entities: List[PhysicalEntity], frame_id: int):
        super(SceneObstacleCourse, self).update(physical_entities, frame_id)
        if self.is_started() and not self.is_finished():
            self._process_virtual_obstacles_collisions()
            self._check_finish_condition()
        if self.PEBBLE_TRACK_LOCATION_ON:
            pebble: VirtualPinchDraggableEllipse = self.pebble
            axis_factor, rotation_angle = pebble.calculate_distortion_values()
            self.pebble_state_history.append(PebbleState(centroid=pebble.location_xy,
                                                         rotation_angle=rotation_angle,
                                                         axis_factor=axis_factor,
                                                         frame_id=frame_id
                                                         ))

    def on_grab_vibrate(self):
        self.vibrate(self.INITIAL_GRAB_VIBRATION_DURATION)

    def _on_first_pebble_grab(self):
        if not self.is_started() and not self.is_finished():
            self.on_grab_vibrate()
            self.start()
            self.pebble.on_initial_pinch_function = self.on_grab_vibrate

    def _add_initial_objects(self):
        workspace_size_xy = self.workspace.virtual_workspace_shape_scaled[::-1]
        grid_size_xy = np.asarray(workspace_size_xy / self.obstacle_size_xy_rescaled).astype(np.int)

        grid_start_point_xy = [1, 1]
        grid_end_point_xy = grid_size_xy - 1

        if self.MIRROR_FOR_LEFT_HAND and self.hand_mode == HandEnum.LEFT:
            grid_start_point_xy = [grid_size_xy[0] - 2, 1]
            grid_end_point_xy = [0, grid_size_xy[1] - 1]

        self.start_point_xy = (grid_start_point_xy * self.obstacle_size_xy_rescaled) + (
                self.obstacle_size_xy_rescaled / 2)
        self.end_point_xy = (grid_end_point_xy * self.obstacle_size_xy_rescaled) + (
                self.obstacle_size_xy_rescaled / 2)

        self.drop_zone: VirtualCircle = self._generate_drop_zone()
        self.pebble: VirtualPinchDraggableEllipse = self._generate_pebble()

        self.obstacles = self._generate_obstacles(grid_start_point_xy=grid_start_point_xy,
                                                  grid_end_point_xy=grid_end_point_xy,
                                                  grid_size_xy=grid_size_xy)
        self.pebble.on_initial_pinch_function = self._on_first_pebble_grab

        self.drop_text: VirtualText = self._generate_drop_text()

        self.obstacle_edge_points = [p for o in self.obstacles for p in
                                     o.get_poly_points(delta=self.OBSTACLE_EDGE_DELTA)]

        self.interactive_virtual_objects.extend([self.pebble])

        self.rendered_virtual_objects.extend(self.obstacles)
        self.rendered_virtual_objects.extend([self.pebble, self.drop_zone, self.drop_text])

    def _generate_drop_text(self):
        drop_text_loc = self.end_point_xy
        drop_text = VirtualText(location_xy=drop_text_loc,
                                message=self.DROP_ZONE_TEXT_IN_PROGRESS,
                                scale=self.drop_text_scale_rescaled,
                                rotated=True,
                                center_text=True
                                )
        drop_text.set_all_colors(SchemeColors.NEUTRAL)
        return drop_text

    def _generate_drop_zone(self):

        drop_zone = VirtualCircle(location_xy=self.end_point_xy,
                                  radius=self.drop_zone_radius_rescaled,
                                  visual_thickness=self.DROP_ZONE_THICKNESS)

        drop_zone.set_all_colors(self.DROP_ZONE_COLOR)

        return drop_zone

    def calculate_initial_pebble_loc(self):
        start_point = [1, 1]
        loc = np.add(np.multiply(start_point[::-1], self.obstacle_size_xy_rescaled),
                     np.multiply(self.obstacle_size_xy_rescaled, 0.5))
        return loc

    def _generate_pebble(self):

        draggable_ellipse = VirtualPinchDraggableEllipse(location_xy=self.start_point_xy,
                                                         radius=self.pebble_radius_rescaled,
                                                         visual_thickness=self.PEBBLE_THICKNESS)
        draggable_ellipse.is_touchable = False
        draggable_ellipse.set_all_colors(self.PEBBLE_COLOR)
        draggable_ellipse.pinched_color = self.PEBBLE_PINCHED_COLOR

        return draggable_ellipse

    def _generate_obstacles(self, grid_start_point_xy, grid_end_point_xy, grid_size_xy):
        obstacle_count = int(self.OBSTACLE_GEN_MIN_RATIO * grid_size_xy[0] * grid_size_xy[1])

        course, location_obstacles_yx = generate_obstacle_course(size_yx=grid_size_xy[::-1],
                                                                 obstacle_count=obstacle_count,
                                                                 start_point_yx=grid_start_point_xy[::-1],
                                                                 end_point_yx=grid_end_point_xy[::-1],
                                                                 max_try=self.OBSTACLE_GEN_MAX_TRY)

        virtual_obstacles = []

        size_coefficient = self.OBSTACLE_GEN_RND_SIZE_COEF
        location_displacement = ((
                                         size_coefficient * self.obstacle_size_xy_rescaled) - self.obstacle_size_xy_rescaled) / 2

        obstacle_radius = pythagoras(self.obstacle_size_xy_rescaled[0], self.obstacle_size_xy_rescaled[1]) / 2
        obstacle_radius_increased = obstacle_radius * size_coefficient

        pebble_threshold_distance = (self.pebble.radius + obstacle_radius_increased) + (
                self.pebble.visual_thickness * 2 + self.OBSTACLE_THICKNESS * 2) + pythagoras(location_displacement[0],
                                                                                             location_displacement[1])
        drop_zone_threshold_distance = (self.drop_zone.radius + obstacle_radius_increased) + (
                self.pebble.visual_thickness * 2 + self.OBSTACLE_THICKNESS * 2) + pythagoras(location_displacement[0],
                                                                                             location_displacement[1])

        for loc_indices_yx in location_obstacles_yx:
            loc_pixels_xy = np.multiply(loc_indices_yx[::-1], self.obstacle_size_xy_rescaled)
            v = VirtualRectangle(location_xy=loc_pixels_xy, size_xy=self.obstacle_size_xy_rescaled,
                                 visual_thickness=self.OBSTACLE_THICKNESS)
            v.set_all_colors(self.OBSTACLE_COLOR)
            v.center_color = SchemeColors.TRANSPARENT

            centroid, radius = v.get_bounding_circle()

            dist_from_pebble = distance(self.pebble.location_xy, centroid)
            dist_from_drop_zone = distance(self.drop_zone.location_xy, centroid)

            ok_to_transform = (dist_from_pebble > pebble_threshold_distance) and (
                    dist_from_drop_zone > drop_zone_threshold_distance)

            i = random.uniform(0, 1)
            to_apply_size = size_coefficient if i < self.OBSTACLE_GEN_CHANCE_MAGNIFY and ok_to_transform else 1
            to_apply_loc = location_displacement if i < self.OBSTACLE_GEN_CHANCE_MAGNIFY and ok_to_transform else [0, 0]

            i = random.uniform(0, 1)
            to_apply_loc[0] = 0 if i < self.OBSTACLE_GEN_CHANCE_LOCATION_REVERT else to_apply_loc[0]

            i = random.uniform(0, 1)
            to_apply_loc[1] = 0 if i < self.OBSTACLE_GEN_CHANCE_LOCATION_REVERT else to_apply_loc[1]

            v.size_xy = v.size_xy * to_apply_size
            v.location_xy = v.location_xy - to_apply_loc

            v.is_static = True
            virtual_obstacles.append(v)
        return virtual_obstacles

    def _check_finish_condition(self):
        drop_zone_loc = self.drop_zone.location_xy
        pebble_loc = self.pebble.location_xy
        dist = distance(drop_zone_loc, pebble_loc)

        if dist <= (1 - self.DROP_ZONE_FINISH_OVERLAP_RATIO) * (
                self.drop_zone_radius_rescaled + self.pebble_radius_rescaled) and not self.pebble.is_pinched():
            self.finish()

    def _process_virtual_obstacles_collisions(self):
        pebble_mask = self.pebble.get_visual_collision_mask(self.workspace.virtual_workspace_shape_scaled)
        collided_any = False
        distance_upper_bound = (pythagoras(self.obstacle_size_xy_rescaled[0], self.obstacle_size_xy_rescaled[1]) / 2)

        pebble_poly_points = self.pebble.get_poly_points(self.PEBBLE_EDGE_DELTA)

        for idx, obstacle in enumerate(self.obstacles):

            are_bounds_overlapping, dist = self.pebble.overlaps(obstacle)

            if are_bounds_overlapping:
                obstacle_mask = obstacle.get_collision_mask(self.workspace.virtual_workspace_shape_scaled)
                if are_masks_overlapping(pebble_mask, obstacle_mask):
                    collided_any = True
                    obstacle.set_all_colors(self.OBSTACLE_COLLISION_COLOR)
                else:
                    obstacle.set_all_colors(self.OBSTACLE_COLOR)
            else:
                obstacle.set_all_colors(self.OBSTACLE_COLOR)

        if collided_any:
            self.pebble_is_colliding_count += 1
        else:
            self.pebble_is_not_colliding_count += 1

        if self.pebble.is_pinched():
            if collided_any:
                self.vibrate(self.COLLISION_VIBRATION_DURATION)
            else:
                min_dist = sci_distance.cdist(pebble_poly_points, self.obstacle_edge_points).min()
                if min_dist <= distance_upper_bound:
                    factor = (distance_upper_bound - min_dist) / distance_upper_bound
                    adaptive_vibration_duration = self.COLLISION_VIBRATION_DURATION * factor * 0.35
                    self.vibrate(adaptive_vibration_duration)
