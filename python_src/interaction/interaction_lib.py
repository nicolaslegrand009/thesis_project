import numpy as np

from config import config

TOUCH_AREA_THRESHOLD_PIXELS = config["interaction"]["TOUCH_AREA_THRESHOLD"]


def are_masks_overlapping(mask1, mask2):
    overlap_mask = np.logical_and(mask1, mask2)
    return np.any(overlap_mask)


def are_masks_interacting(mask1, mask2, delta_depth, touch_distance_threshold):
    overlap_mask = np.logical_and(mask1, mask2)
    is_overlapping = np.any(overlap_mask)

    is_touching = False
    if is_overlapping and np.count_nonzero(overlap_mask) >= TOUCH_AREA_THRESHOLD_PIXELS:
        truth_map = np.logical_and((overlap_mask > 0), (delta_depth > 0))
        truth_map = np.logical_and(truth_map, (delta_depth <= touch_distance_threshold))
        is_touching = np.count_nonzero(truth_map) >= TOUCH_AREA_THRESHOLD_PIXELS

    return is_overlapping, is_touching
