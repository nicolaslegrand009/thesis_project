from typing import List

import numpy as np

from config import config
from display.display_lib import debug_display
from interaction.interaction_lib import are_masks_interacting, are_masks_overlapping
from models.calibration.depth_info import DepthInfo
from models.calibration.workspace import Workspace
from models.has_bounding_circle import HasBoundingCircle
from models.physical.physical_entity import PhysicalEntity
from models.virtual.virtual_object import VirtualObject
from models.virtual.virtual_pinch_draggable_object import VirtualPinchDraggableObject, PinchMode


class InteractionManager:
    """Process interaction between physical and virtual objects"""

    TOUCH_DISTANCE_THRESHOLD_IN_METERS = config["interaction"]["TOUCH_DISTANCE_THRESHOLD"]

    workspace: Workspace = None
    depth_calibration_info = None
    touch_distance_threshold_scaled = None

    def __init__(self, workspace: Workspace, depth_calibration_info: DepthInfo):
        self.workspace = workspace
        self.depth_calibration_info = depth_calibration_info

        self.touch_distance_threshold_scaled = depth_calibration_info.convert_to_depth_units(
            self.TOUCH_DISTANCE_THRESHOLD_IN_METERS
        )

    def _apply_pinch_drag_displacement(self, v: VirtualPinchDraggableObject):
        couple: List[PhysicalEntity] = v.drag_candidates[:2]

        if len(couple[0].location_history) >= 2 and len(couple[1].location_history) >= 2:
            couple_latest_pos = [c.location_history[-1].centroid for c in couple]
            latest_middle_point = (couple_latest_pos[0] + couple_latest_pos[1]) / 2

            couple_previous_pos = [c.location_history[-2].centroid for c in couple]
            previous_middle_point = (couple_previous_pos[0] + couple_previous_pos[1]) / 2

            middle_point_displacement = (latest_middle_point - previous_middle_point)
            v.location_xy = np.add(v.location_xy, middle_point_displacement)

    def process_interactions(self, physical_entities: List[PhysicalEntity], virtual_objects: List[VirtualObject],
                             delta_depth):

        if len(physical_entities) == 0 or len(virtual_objects) == 0:
            return

        if __debug__:
            self.___debug_show_delta_depth(delta_depth, self.workspace.virtual_workspace_shape_scaled)

        virtual_workspace_shape_scaled = self.workspace.virtual_workspace_shape_scaled
        physical_entities_collision_masks = [
            p.get_latest_tracked_obj().get_collision_mask(virtual_workspace_shape_scaled)
            for p in physical_entities]
        touch_distance_threshold = self.touch_distance_threshold_scaled
        for v in virtual_objects:
            drag_candidates = []
            append_to_candidates_func = drag_candidates.append
            v_collision_mask = v.get_collision_mask(virtual_workspace_shape_scaled)
            v_touchable = v.is_touchable
            for i, p in enumerate(physical_entities):
                if not isinstance(v, HasBoundingCircle) or not isinstance(p, HasBoundingCircle) or not v.overlaps(p)[0]:
                    is_overlapped, is_touched = False, False
                    p_collision_mask = physical_entities_collision_masks[i]

                    if delta_depth is not None and v_touchable:
                        is_overlapped, is_touched = are_masks_interacting(p_collision_mask,
                                                                          v_collision_mask,
                                                                          delta_depth,
                                                                          touch_distance_threshold)
                    else:
                        is_overlapped = are_masks_overlapping(p_collision_mask, v_collision_mask)

                    if is_overlapped:
                        v.trigger_hovered()
                    if is_touched:
                        v.trigger_touched()

                    if isinstance(v, VirtualPinchDraggableObject):
                        if (is_touched and v.pinch_mode == PinchMode.TOUCH) or (
                                is_overlapped and v.pinch_mode == PinchMode.HOVER):
                            append_to_candidates_func(p)

            if isinstance(v, VirtualPinchDraggableObject) and v.trigger_pinched(drag_candidates):
                self._apply_pinch_drag_displacement(v)

    def ___debug_show_delta_depth(self, delta_depth, output_shape):
        if config["debug_display"]["SHOW_DELTA"]:
            delta_mask_touch_zone = np.zeros(output_shape)
            delta_mask = np.zeros(output_shape)

            delta_depth = np.asarray(delta_depth).astype(dtype=np.float64)

            delta_mask[(delta_depth > 0)] = 255
            delta_mask_touch_zone[(delta_depth > 0) & (delta_depth < self.touch_distance_threshold_scaled)] = 255

            debug_display("SHOW_DELTA_ABOVE_CUTOFF", delta_mask)
            debug_display("SHOW_DELTA_TOUCH_INTERVAL", delta_mask_touch_zone)
