from detection.hand_detector import *
from display.display_lib import debug_display
from utils.img_utils import are_masks_matching, convert_contour_to_mask

MASK_MATCHING_THRESHOLD = config["detection"]["MASK_MATCHING_THRESHOLD"]


def extract_physical_objects(color_based_contours, depth_based_contours, workspace):
    if __debug__:
        ___debug_show_detected_contours(color_based_contours, depth_based_contours, workspace)

    physical_objects = _get_physical_objects(color_based_contours=color_based_contours,
                                             depth_based_contours=depth_based_contours, workspace=workspace)

    if __debug__:
        ___debug_show_physical_objects(physical_objects, workspace)

    return physical_objects


def _get_physical_objects(color_based_contours, depth_based_contours, workspace: Workspace):
    physical_objects = []
    color_based_dataset = {}

    for idx, c_contour in enumerate(color_based_contours):
        color_based_dataset[idx] = {
            'contour': c_contour,
            'mask': convert_contour_to_mask([c_contour], workspace.virtual_workspace_shape_scaled)}

    for d_contour in depth_based_contours:
        d_mask = convert_contour_to_mask([d_contour], workspace.virtual_workspace_shape_scaled)
        d_contour = d_contour
        for key, c_data in color_based_dataset.items():
            c_mask = c_data['mask']
            c_contour = c_data['contour']
            is_match, score = are_masks_matching(c_mask, d_mask, MASK_MATCHING_THRESHOLD)
            if is_match:
                physical_objects.append(PhysicalObject(
                    depth_based_mask=d_mask,
                    depth_based_contour=d_contour,
                    color_based_mask=c_mask,
                    color_based_contour=c_contour))
                del color_based_dataset[key]
                break
    return physical_objects


def ___debug_show_physical_objects(physical_objects, workspace: Workspace):
    if config["debug_display"]["SHOW_PHYSICAL_OBJECTS"]:
        output = np.zeros(workspace.virtual_workspace_shape_scaled)
        output = cv2.merge((output, output, output))
        for p_obj in physical_objects:
            p_obj.draw_color_contour(output)
            p_obj.draw_depth_contour(output)
        debug_display("SHOW_PHYSICAL_OBJECTS", output)


def ___debug_show_detected_contours(color_based_contours, depth_based_contours, workspace: Workspace):
    if config["debug_display"]["SHOW_DETECTED_CONTOURS"]:
        output = np.zeros(workspace.virtual_workspace_shape_scaled)
        output = cv2.merge((output, output, output))
        cv2.drawContours(output, depth_based_contours, -1, (255, 255, 0), -1)
        cv2.drawContours(output, color_based_contours, -1, (255, 0, 255), -1)
        debug_display("SHOW_DETECTED_CONTOURS", output)
