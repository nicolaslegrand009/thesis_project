import logging
from typing import List

import cv2
import numpy as np

from config import config
from display.display_lib import debug_display
from models.calibration.workspace import Workspace
from models.physical.marker_object import MarkerObject
from utils.img_utils import dilate, erode, convert_contour_to_mask, extract_hsv_filtered_mask
from utils.math_utils import calculate_centroid


class MarkerDetector:
    """Extract marker pixels from color frames"""
    workspace: Workspace = None
    MARKER_HSV_RANGE = config["detection"]["marker_detector"]["MARKER_HSV_RANGE"]

    def __init__(self, workspace: Workspace):
        logging.getLogger(f"{self.__class__.__name__}").debug(f"{self.__class__.__name__}()")
        self.workspace = workspace

    def _transform_contour_to_marker_obj(self, contour):
        mask = convert_contour_to_mask([contour], self.workspace.virtual_workspace_shape_scaled)
        centroid = calculate_centroid(contour)
        if centroid is not None:
            m_obj = MarkerObject(color_based_mask=mask, color_based_contour=contour, centroid=centroid)
            return m_obj
        else:
            return None

    def extract_markers(self, color_frame):
        masked_img = extract_hsv_filtered_mask(color_frame, self.MARKER_HSV_RANGE)

        erode(masked_img, kernel_size=3, dst=masked_img)
        dilate(masked_img, kernel_size=15, dst=masked_img)
        erode(masked_img, kernel_size=15, dst=masked_img)
        dilate(masked_img, kernel_size=3, dst=masked_img)

        masked_img_virtual = self.workspace.transform_to_virtual_img(masked_img)

        contours, _ = cv2.findContours(
            masked_img_virtual, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        marker_objects: List[MarkerObject] = [m_obj for m_obj in
                                              (self._transform_contour_to_marker_obj(v) for v in contours) if
                                              m_obj is not None]

        if __debug__:
            self.___debug_show_detected_markers(marker_objects, color_frame)

        return marker_objects

    def ___debug_show_detected_markers(self, marker_objects: List[MarkerObject], color_img):
        if config["debug_display"]["SHOW_DETECTED_MARKERS"]:
            d_img = self.workspace.transform_to_virtual_img(color_img)

            for m_obj in marker_objects:
                c_mask = m_obj.get_collision_mask(self.workspace.virtual_workspace_shape_scaled)
                centroid = np.asarray(m_obj.get_centroid())

                d_img[c_mask == 255] = (0, 255, 255)
                cv2.circle(d_img,
                           tuple(np.asarray(centroid).astype(np.int)),
                           radius=2,
                           color=(255, 0, 0),
                           thickness=2
                           )
            debug_display("SHOW_DETECTED_MARKERS", d_img)
