from collections import deque
from typing import List

import cv2
import numpy as np

from config import config
from display.display_lib import debug_display
from models.calibration.workspace import Workspace
from models.physical.hand_object import HandObject
from models.physical.physical_object import PhysicalObject
from utils.img_utils import add_border
from utils.math_utils import distance, calculate_angle

HULL_MAX_ANGLE = config["detection"]["hand_detector"]["HULL_POINT_MAX_ANGLE"]
HULL_CLUSTER_MAX_DISTANCE = config["detection"]["hand_detector"]["HULL_POINTS_MIN_DISTANCE"]


def clusterize_contour_points(contour, hull_ids, max_distance, distance_function):
    clusters = []
    index_queue = deque(hull_ids)
    while len(index_queue) > 0:
        current_id = index_queue.pop()
        assigned = False
        for cluster in clusters:
            if does_point_fit(cluster, contour, current_id, max_distance, distance_function):
                cluster.append(current_id)
                assigned = True
        if not assigned:
            new_cluster = [current_id]
            clusters.append(new_cluster)
    return clusters


def does_point_fit(cluster, contour, new_point_id, max_distance, d_func):
    new_point = np.vstack(contour[new_point_id]).squeeze()
    for point_id in cluster:
        cluster_point = np.vstack(contour[point_id]).squeeze()
        if d_func(new_point, cluster_point) > max_distance:
            return False
    return True


def flatten_cluster(clusters):
    return [c[len(c) // 2] for c in clusters]


def get_hull_defects_neighbours(hull_ids, defects):
    hull_neighbors_dict = {}

    for h in hull_ids:
        hull_neighbors_dict[h[0]] = []

    if type(defects) != type(None):  # avoid crashing.   (BUG not found)
        for i in range(defects.shape[0]):  # calculate the angle
            s, e, f, d = defects[i][0]
            hull_neighbors_dict[s].append(f)
            hull_neighbors_dict[e].append(f)
    else:
        return {}

    hull_neighbors_dict = {k: v for k, v in hull_neighbors_dict.items() if len(v) > 1}

    return hull_neighbors_dict


def filter_hulls_by_border(contour, hull_ids, border_mask):
    filtered_hull_ids = []
    for h in hull_ids:
        point = np.vstack(contour[h]).squeeze()
        if (border_mask[point[1]][
            point[0]] != 0):  # swapped because point (x,y) ;  img[row][column] => row = y and column = x
            filtered_hull_ids.append(h)
    return filtered_hull_ids


def get_hull_defects_filtered_by_angle(contour, hull_neighbors_dict):
    valid_hull_points = []
    valid_defect_points = []

    for h_id in hull_neighbors_dict:
        defect_id1 = hull_neighbors_dict[h_id][0]
        defect_id2 = hull_neighbors_dict[h_id][1]

        h_point = contour[h_id][0]
        d_point1 = contour[defect_id1][0]
        d_point2 = contour[defect_id2][0]

        hull_angle = calculate_angle(d_point1, d_point2, h_point)

        if hull_angle is not None and hull_angle < HULL_MAX_ANGLE:
            valid_hull_points.append(h_point)
            valid_defect_points.append(d_point1)
            valid_defect_points.append(d_point2)
    return valid_hull_points, valid_defect_points


def process_hull_defects(contour):
    hull_ids = cv2.convexHull(contour, returnPoints=False)

    if len(hull_ids) == 0:
        return [], []

    clustered_hull_ids = clusterize_contour_points(contour,
                                                   hull_ids,
                                                   HULL_CLUSTER_MAX_DISTANCE,
                                                   distance)

    flattened_hull_ids = np.asarray(flatten_cluster(clustered_hull_ids))

    if len(flattened_hull_ids) == 0:
        return [], []

    defects = cv2.convexityDefects(contour,
                                   flattened_hull_ids)

    hull_neighbors_dict = get_hull_defects_neighbours(flattened_hull_ids,
                                                      defects)

    if len(hull_neighbors_dict.keys()) == 0:
        return [], []

    valid_hull_points, valid_defect_points = get_hull_defects_filtered_by_angle(contour, hull_neighbors_dict)

    return valid_hull_points, valid_defect_points


def filter_points_by_border(border_mask, points):
    return [p for p in points if
            border_mask[p[1]][p[0]] != 0]  # swapped because point (x,y) ;  img[row][column] => row = y and column = x


class HandDetector:
    BORDER_SIZE = 5
    workspace: Workspace = None
    border_mask = None

    def __init__(self, workspace: Workspace):
        self.workspace = workspace

        border = np.zeros(workspace.virtual_workspace_shape_scaled)
        border.fill(255)
        self.border_mask = add_border(img=border,
                                      color=0,
                                      border_size=self.BORDER_SIZE)

    def is_object_hand(self, contour) -> [bool, List, List]:
        valid_hull_points, valid_defect_points = process_hull_defects(contour)
        valid_hull_points = filter_points_by_border(self.border_mask, valid_hull_points)

        if len(valid_hull_points) == 0:
            return False, [], []

        return True, valid_hull_points, valid_defect_points

    def detect_hand_objects(self, physical_objects: List[PhysicalObject]) -> List[HandObject]:
        hand_objects: List[HandObject] = []
        for i in range(0, len(physical_objects)):
            p_obj = physical_objects[i]
            is_hand, hull_points, defect_points = self.is_object_hand(p_obj.color_based_contour)
            if is_hand:
                hand_obj = HandObject(p_obj,
                                      finger_tips=hull_points,
                                      finger_joints=defect_points)
                hand_objects.append(hand_obj)

        if __debug__:
            self.___debug_show_hand_objects(hand_objects)

        return hand_objects

    def ___debug_show_hand_objects(self, hand_objects):
        if config["debug_display"]["SHOW_HAND_OBJECTS"]:
            o = np.zeros(self.workspace.virtual_workspace_shape_scaled)
            o = cv2.merge((o, o, o))
            for h_obj in hand_objects:
                h_obj.draw_self(o)
            debug_display("SHOW_HAND_OBJECTS", o)
