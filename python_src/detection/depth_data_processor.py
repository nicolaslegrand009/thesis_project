import logging

from detection.hand_detector import *
from models.calibration.depth_info import DepthInfo
from utils.img_utils import dilate, erode, gaussian_blur


def _calculate_delta_depth(depth_data, baseline, cutoff):
    above_cutoff = np.where(depth_data >= cutoff, 0, depth_data)
    delta_depth = np.subtract(baseline, depth_data)
    delta_depth = np.where(above_cutoff <= 0, -1, delta_depth
                           )
    return delta_depth


class DepthDataProcessor:
    """Depth data processing"""
    DEPTH_CONTOUR_MIN_AREA = config["detection"]["depth_processor"]["DEPTH_CONTOUR_MIN_AREA"]

    workspace: Workspace = None
    depth_calibration_info: DepthInfo = None

    def __init__(self, workspace: workspace, depth_calibration_info: DepthInfo):
        logging.getLogger(f"{self.__class__.__name__}").debug(f"{self.__class__.__name__}()")
        self.workspace: Workspace = workspace
        self.depth_calibration_info: DepthInfo = depth_calibration_info

    def calculate_delta_depth(self, depth_data):
        baseline = self.depth_calibration_info.baseline_depth
        baseline_cutoff = self.depth_calibration_info.baseline_depth_padded
        delta = _calculate_delta_depth(depth_data, baseline, baseline_cutoff)
        delta = self.workspace.transform_to_virtual_img(delta)
        return delta

    def extract_depth_contours(self, depth_data, delta_depth):
        delta_foreground = np.zeros(self.workspace.virtual_workspace_shape_scaled, dtype=np.uint8)
        delta_foreground[(delta_depth > 0)] = 255

        erode(delta_foreground, kernel_size=3, dst=delta_foreground)
        dilate(delta_foreground, kernel_size=11, dst=delta_foreground)
        erode(delta_foreground, kernel_size=11, dst=delta_foreground)
        dilate(delta_foreground, kernel_size=3, dst=delta_foreground)

        gaussian_blur(delta_foreground, 5, dst=delta_foreground)
        (thresh, delta_foreground) = cv2.threshold(delta_foreground, 128, 255, cv2.THRESH_BINARY)

        depth_contours, _ = cv2.findContours(
            delta_foreground, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        contours_depth_filtered = list(
            filter(lambda x: self.DEPTH_CONTOUR_MIN_AREA < cv2.contourArea(x), depth_contours))

        return contours_depth_filtered
