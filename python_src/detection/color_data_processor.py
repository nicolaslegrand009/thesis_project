from detection.hand_detector import *
from utils.img_utils import dilate, erode, gaussian_blur, extract_hsv_filtered_masks, combine_masks


class ColorDataProcessor:
    HSV_RANGE_LIST = config["detection"]["color_processor"]["HSV_RANGE_LISt"]
    CONTOUR_MIN_AREA = config["detection"]["color_processor"]["COLOR_CONTOUR_MIN_AREA"]
    workspace: Workspace = None

    def __init__(self, workspace: Workspace):
        self.workspace = workspace

    def extract_color_contours(self, color_img):
        masks = extract_hsv_filtered_masks(color_img, self.HSV_RANGE_LIST)
        combined_masks = combine_masks(masks)

        erode(combined_masks, kernel_size=3, dst=combined_masks)
        dilate(combined_masks, kernel_size=11, dst=combined_masks)
        erode(combined_masks, kernel_size=11, dst=combined_masks)
        dilate(combined_masks, kernel_size=3, dst=combined_masks)

        gaussian_blur(combined_masks, 5, dst=combined_masks)
        (thresh, combined_masks) = cv2.threshold(combined_masks, 128, 255, cv2.THRESH_BINARY)

        combination_mask = self.workspace.transform_to_virtual_img(combined_masks)

        contours, _ = cv2.findContours(
            combination_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        contours_filtered = list(filter(lambda x: self.CONTOUR_MIN_AREA < cv2.contourArea(x), contours))

        return contours_filtered
