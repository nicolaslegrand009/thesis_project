import argparse
import logging

from orchestration.experiment_coordinator import ExperimentCoordinator


def parse_console_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--hand", action="store", dest="hand_mode", help="Specify right or left hand")
    parser.add_argument("--group", action="store", dest="group", help="Group A or B")
    parser.add_argument("--id", action="store", dest="candidate_id", help="Candidate ID")
    console_args = parser.parse_args()

    error_arg_found = False

    if console_args.group is None or console_args.group.upper() not in ("A", "B"):
        print("Please specify a valid group 'A' or 'B' using the --group option")
        error_arg_found = True

    if console_args.candidate_id is None:
        print("Please specify a valid candidate ID using the --id option")
        error_arg_found = True

    if console_args.hand_mode is None or console_args.hand_mode.upper() not in ("LEFT", "RIGHT"):
        print("Please specify 'left' or 'right' hand using the --hand option")
        error_arg_found = True

    if error_arg_found:
        input("Press [enter] to exit")
        exit(1)

    return console_args


def main():
    logging.getLogger(main.__name__).debug(f"{main.__name__}()")
    console_args = parse_console_args()
    logging.getLogger(main.__name__).debug(console_args)

    is_left_hand = console_args.hand_mode.lower() == "left"
    candidate_group = console_args.group
    candidate_id = console_args.candidate_id

    experiment_coordinator = ExperimentCoordinator(
        candidate_id=candidate_id,
        candidate_group=candidate_group,
        is_left_hand_mode=is_left_hand
    )

    experiment_coordinator.start()


if __name__ == "__main__":
    main()
