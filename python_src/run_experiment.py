import argparse
import logging

import cv2
import random
from calibration.projector_camera_calibrator import ProjectorCameraCalibrator
from camera.camera_pipeline_api import CameraPipelineAPI
from display.second_screen_projector import SecondScreenProjector
from models.hand_enum import HandEnum
from orchestration.hand_color_based_scene_manager import HandColorBasedSceneManager
from orchestration.marker_based_scene_manager import MarkerBasedSceneManager
from scenarios.scene_default import SceneDefault
from scenarios.scene_obstacle_course import SceneObstacleCourse
from scenarios.scene_simon_says import SceneSimonSays
from scenarios.scene_trace_line import SceneTraceLine
import time

import time

random.seed(time.clock())
"""
=== START PROGRAM ===
"""


# file_name = "../data/output/recordings/hand1.bag"

def select_scene_creator(x):
    return {
        "1": lambda: SceneTraceLine(),
        "2": lambda: SceneObstacleCourse(),
        "3": lambda: SceneSimonSays()
    }.get(x, lambda: SceneDefault())


def select_scene_manager(is_marker_less, scene):
    return HandColorBasedSceneManager(scene=scene) if is_marker_less \
        else MarkerBasedSceneManager(
        scene=scene)


def get_console_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', action='store', dest='scene_tag',
                        help='Add a tag to saved scene file')
    parser.add_argument("-l", action="store_true", dest="left_hand_mode", help="Turn on left hand mode ...")
    parser.add_argument('-s', action='store', dest='scene_index',
                        help='[1]: Trace the Line, [2]: Obstacle Course, [3]: Simon Says')
    parser.add_argument("-m", action="store_true", dest="marker_less", help="Use marker-less tracking ...")
    parser.add_argument('-f', action='store', dest='camera_input_file_name',
                        help='camera input file path')
    parser.add_argument("-g", action="store_true", dest="haptic_mode", help="Enable haptic feedback ...")

    console_args = parser.parse_args()
    return console_args


def main():
    if 1:
        logging.getLogger(main.__name__).debug(f"{main.__name__}()")
        console_args = get_console_args()
        logging.getLogger(main.__name__).debug(console_args)

        scene_tag = console_args.scene_tag
        camera_input_file_name = console_args.camera_input_file_name
        scene_key = console_args.scene_index
        is_marker_less = console_args.marker_less
        is_left_hand_mode = console_args.left_hand_mode
        is_haptic_mode = console_args.haptic_mode

        scene = select_scene_creator(scene_key)()
        scene.tag = scene_tag
        scene.hand_mode = HandEnum.LEFT if is_left_hand_mode else HandEnum.RIGHT
        scene.is_haptic_enabled = is_haptic_mode
        scene_manager = select_scene_manager(is_marker_less, scene)

        with CameraPipelineAPI(camera_input_file_name) as camera:
            projector = SecondScreenProjector()
            calibrator = ProjectorCameraCalibrator(camera=camera, projector=projector)
            workspace, depth_info = calibrator.start_calibration()
            scene_manager.start_loop(camera=camera,
                                     projector=projector,
                                     workspace=workspace,
                                     depth_info=depth_info)
            scene.save_to_file()

    cv2.waitKey(0)


if __name__ == "__main__":
    main()
