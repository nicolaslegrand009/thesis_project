import cv2
import numpy as np

from scenarios.scene import Scene
from scenarios.scene_obstacle_course import SceneObstacleCourse
from scenarios.scene_simon_says import SceneSimonSays
from scenarios.scene_trace_line import SceneTraceLine
from utils.color_utils import FlatColors

EMPTY_SCENE_IMG_FILE_NAME = "empty_scene_img.jpg"
SCENE_PHYS_HISTORY_IMG_FILE_NAME = "scene_img_history_plotted.jpg"
PEBBLE_HISTORY_FILE_NAME = "scene_img_pebble_history_plotted.jpg"


def _draw_physical_object_history(scene_img, scene: Scene):
    for idx, p_id in enumerate(scene.physical_entities_location_history):
        for loc_time in scene.physical_entities_location_history[p_id]:
            cv2.circle(scene_img, tuple(np.asarray(loc_time.centroid).astype(int)), int(2), FlatColors.WISTERIA,
                       1)
            loc_time.centroid


def extract_obstacle_course_images(scene: SceneObstacleCourse, output_folder: str):
    scene_img = np.ones(scene.workspace.virtual_workspace_shape_scaled) * 255
    scene_img = cv2.merge([scene_img, scene_img, scene_img])

    pebble = scene.pebble
    pebble.location_xy = scene.pebble_state_history[0].centroid
    pebble.visual_thickness = 2
    pebble.set_all_colors(FlatColors.PETER_RIVER)
    pebble.draw_self(scene_img)

    drop_zone = scene.drop_zone
    drop_zone.visual_thickness = 2
    drop_zone.set_all_colors(scene.DROP_ZONE_COLOR)
    drop_zone.draw_self(scene_img)

    drop_zone_text = scene.drop_text
    drop_zone_text.message = scene.DROP_ZONE_TEXT_IN_PROGRESS
    drop_zone_text.set_all_colors(scene.DROP_ZONE_COLOR)
    drop_zone_text.draw_self(scene_img)

    for o in scene.obstacles:
        o.set_all_colors(FlatColors.POMEGRANATE)
        o.visual_thickness = 2
        o.center_color = FlatColors.WHITE
        o.draw_self(scene_img)

    empty_scene_file_path = f"{output_folder}/{scene.tag}{EMPTY_SCENE_IMG_FILE_NAME}"
    cv2.imwrite(empty_scene_file_path, scene_img)

    scene_img_phys_history = np.copy(scene_img)
    if scene.TRACK_PHYS_OBJ_LOC:
        scene_history_img_file_path = f"{output_folder}/{scene.tag}{SCENE_PHYS_HISTORY_IMG_FILE_NAME}"
        _draw_physical_object_history(scene_img_phys_history, scene)
        cv2.imwrite(scene_history_img_file_path, scene_img_phys_history)

    if scene.PEBBLE_TRACK_LOCATION_ON:
        scene_img_pebble_history = np.copy(scene_img_phys_history)
        pebble_history_file_path = f"{output_folder}/{scene.tag}{PEBBLE_HISTORY_FILE_NAME}"
        for loc_time in scene.pebble_state_history:
            cv2.circle(scene_img_pebble_history, tuple(np.asarray(loc_time.centroid).astype(int)), int(2),
                       pebble.color,
                       1)
            loc_time.centroid
        cv2.imwrite(pebble_history_file_path, scene_img_pebble_history)


def extract_trace_line_images(scene: SceneTraceLine, output_folder: str):
    workspace_shape = scene.workspace.virtual_workspace_shape_scaled
    scene_img = np.ones(workspace_shape) * 255
    scene_img = cv2.merge([scene_img, scene_img, scene_img])

    scene.trace_line.set_all_colors(FlatColors.PETER_RIVER)
    scene.trace_line.thickness = 2
    scene.trace_line.draw_self(scene_img)

    scene.start_circle.set_all_colors(FlatColors.GREEN_SEA)
    scene.start_circle.visual_thickness = 2
    scene.start_circle.draw_self(scene_img)

    scene.start_text.set_all_colors(FlatColors.GREEN_SEA)
    scene.start_text.draw_self(scene_img)

    scene.end_circle.set_all_colors(FlatColors.SUN_FLOWER)
    scene.end_circle.visual_thickness = 2
    scene.end_circle.draw_self(scene_img)

    scene.end_text.set_all_colors(FlatColors.SUN_FLOWER)
    scene.end_text.draw_self(scene_img)

    empty_scene_file_path = f"{output_folder}/{scene.tag}{EMPTY_SCENE_IMG_FILE_NAME}"
    cv2.imwrite(empty_scene_file_path, scene_img)

    if scene.TRACK_PHYS_OBJ_LOC:
        _draw_physical_object_history(scene_img, scene)
        scene_history_img_file_path = f"{output_folder}/{scene.tag}{SCENE_PHYS_HISTORY_IMG_FILE_NAME}"
        cv2.imwrite(scene_history_img_file_path, scene_img)


def extract_simon_says_images(scene: SceneSimonSays, output_folder: str):
    scene_img = np.ones(scene.workspace.virtual_workspace_shape_scaled) * 255
    scene_img = cv2.merge([scene_img, scene_img, scene_img])

    indicator_blob = scene.indicator_blob
    indicator_blob.set_all_colors(scene.BLOCK_COLORS[scene.round_info_history[-1].correct_answer])
    indicator_blob.draw_self(scene_img)

    instruction_text = scene.instruction_text
    instruction_text.message = scene.SIMON_SAYS_TEXT
    instruction_text.set_all_colors(FlatColors.BLACK)
    instruction_text.draw_self(scene_img)

    for ix, b in enumerate(scene.virtual_blocks):
        b.visual_thickness = 2
        b.set_all_colors(scene.BLOCK_COLORS[ix])
        b.draw_self(scene_img)

    empty_scene_file_path = f"{output_folder}/{scene.tag}{EMPTY_SCENE_IMG_FILE_NAME}"
    cv2.imwrite(empty_scene_file_path, scene_img)

    if scene.TRACK_PHYS_OBJ_LOC:
        scene_img_phys_history = np.copy(scene_img)
        scene_history_img_file_path = f"{output_folder}/{scene.tag}{SCENE_PHYS_HISTORY_IMG_FILE_NAME}"
        _draw_physical_object_history(scene_img_phys_history, scene)
        cv2.imwrite(scene_history_img_file_path, scene_img_phys_history)
