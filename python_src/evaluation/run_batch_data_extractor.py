import argparse
import json
import logging
import os
import time
from datetime import datetime
from pathlib import Path

import dill

from config import experiments_folder_path
from evaluation.data_extraction.extractor_obstacle_course import ExtractorObstacleCourse
from evaluation.data_extraction.extractor_simon_says import ExtractorSimonSays
from evaluation.data_extraction.extractor_trace_line import ExtractorTraceLine

from scenarios.scene_obstacle_course import SceneObstacleCourse
from scenarios.scene_trace_line import SceneTraceLine
from scenarios.scene_simon_says import SceneSimonSays

from evaluation.image_extraction.scene_image_extraction import extract_simon_says_images
from evaluation.image_extraction.scene_image_extraction import extract_trace_line_images
from evaluation.image_extraction.scene_image_extraction import extract_obstacle_course_images

from evaluation.data_extraction.extractor import TEST_PHASE_2_STRING, TEST_PHASE_1_STRING, MAIN_PHASE_2_STRING, \
    MAIN_PHASE_1_STRING

EXTRACT_VISUAL_ON = True


def process_pkl_file(exp_folder_path,
                     pkl_file_name,
                     init_info,
                     phase,
                     ):
    extractor_types = [ExtractorSimonSays, ExtractorTraceLine, ExtractorObstacleCourse]

    pkl_file_path = f"{exp_folder_path}/{pkl_file_name}"
    try:
        with open(pkl_file_path, "rb") as f:
            scene = dill.load(f)
            data = None
            for e in extractor_types:
                if isinstance(scene, e.scene_type):
                    extractor = e(scene=scene, init_info=init_info, phase=phase)
                    data = extractor.extract_data()
                    break

                if EXTRACT_VISUAL_ON:
                    visual_folder = f"{exp_folder_path}/visuals"
                    Path(visual_folder).mkdir(parents=True, exist_ok=True)
                    if isinstance(scene, SceneSimonSays):
                        extract_simon_says_images(scene, visual_folder)
                    elif isinstance(scene, SceneTraceLine):
                        extract_trace_line_images(scene, visual_folder)
                    elif isinstance(scene, SceneObstacleCourse):
                        extract_obstacle_course_images(scene, visual_folder)
                    else:
                        logging.warning(f"Scene type {scene.__class__.__name__} not recognized ...")

            return data

    except FileNotFoundError as e:
        logging.warning(f"File not found: {pkl_file_path}")


def process_experience_suite_folder(exp_folder_path):
    init_info_path = f"{exp_folder_path}/init_info.json"

    with open(init_info_path) as f:
        init_info = json.load(f)

    phases = [TEST_PHASE_1_STRING, TEST_PHASE_2_STRING, MAIN_PHASE_1_STRING, MAIN_PHASE_2_STRING]

    data_list = []

    for p in phases:
        tags = init_info[p]
        for tag in tags:
            data = process_pkl_file(exp_folder_path,
                                    pkl_file_name=f"{tag}.pkl",
                                    init_info=init_info,
                                    phase=p
                                    )
            if data is not None:
                data_list.append(data)

    return data_list


def parse_console_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", action="store", dest="folder_path", help="Folder path to save output data")
    console_args = parser.parse_args()

    error_arg_found = False

    if console_args.folder_path is None:
        print("Please specify a valid folder path using the -f option")
        error_arg_found = True

    if error_arg_found:
        input("Press [enter] to exit")
        exit(1)

    return console_args


def main():
    console_args = parse_console_args()
    folder_path = console_args.folder_path
    data_list_merged = []
    for exp_folder in os.listdir(experiments_folder_path):

        exp_folder_path = os.path.join(experiments_folder_path, exp_folder)
        if os.path.isdir(exp_folder_path):
            data_list = process_experience_suite_folder(exp_folder_path)
            data_list_merged.extend(data_list)

    date_string = datetime.fromtimestamp(time.time()).strftime(
        '%Y-%m-%d-%Hh-%Mm-%Ss')

    with open(f"{folder_path}/{date_string}-extracted-data.json", 'w') as f:
        json_data = json.dumps(data_list_merged, default=lambda o: o.__dict__)
        json.dump(json_data, f)


if __name__ == "__main__":
    main()
