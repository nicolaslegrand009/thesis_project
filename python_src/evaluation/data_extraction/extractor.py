from evaluation.data_extraction.scene_data import SceneData, TraceLineData, SimonSaysData, ObstacleCourseData
from scenarios.scene import Scene
from scenarios.scene_obstacle_course import SceneObstacleCourse
from scenarios.scene_simon_says import SceneSimonSays
from scenarios.scene_trace_line import SceneTraceLine

TEST_PHASE_1_STRING = "test_phase_1"
TEST_PHASE_2_STRING = "test_phase_2"
MAIN_PHASE_1_STRING = "main_phase_1"
MAIN_PHASE_2_STRING = "main_phase_2"


class Extractor:
    scene: Scene = None
    init_info = None
    data: SceneData = None
    scene_type: type = None

    def __init__(self, scene, init_info, phase):
        self.scene = scene
        self.init_info = init_info
        if isinstance(scene, SceneTraceLine):
            self.data = TraceLineData()
        elif isinstance(scene, SceneSimonSays):
            self.data = SimonSaysData()
        elif isinstance(scene, SceneObstacleCourse):
            self.data = ObstacleCourseData()
        else:
            raise Exception("Scene unrecognized")

        candidate_id = self.init_info["candidate_id"]
        candidate_group = self.init_info["candidate_group"]
        hand_mode = init_info["hand_mode"]

        allowed_combo = set({f"A{TEST_PHASE_1_STRING}{False}",
                             f"A{MAIN_PHASE_1_STRING}{False}",
                             f"A{TEST_PHASE_2_STRING}{True}",
                             f"A{MAIN_PHASE_2_STRING}{True}",
                             f"B{TEST_PHASE_1_STRING}{True}",
                             f"B{MAIN_PHASE_1_STRING}{True}",
                             f"B{TEST_PHASE_2_STRING}{False}",
                             f"B{MAIN_PHASE_2_STRING}{False}",
                             })

        if hand_mode != scene.hand_mode.name:
            raise Exception(f"Init hand_mode and scene.hand_mode don't match for tag: {self.scene.tag}")

        if f"{candidate_group}{phase}{scene.is_haptic_enabled}" not in allowed_combo:
            raise Exception(
                f"Haptics was enabled for scene in {phase} for {candidate_group} experiment for tag: {scene.tag}")

        self.data.candidate_id = candidate_id
        self.data.hand_mode = hand_mode
        self.data.group = candidate_group
        self.data.start_time = scene.start_time
        self.data.finish_time = scene.finish_time
        self.data.haptics_enabled = scene.is_haptic_enabled
        self.data.phase = phase
        self.data.scene_type = scene.__class__.__name__

        if self.scene.is_finished():
            self.data.duration = self.scene.finish_time - self.scene.start_time

    def extract_data(self) -> SceneData:
        pass
