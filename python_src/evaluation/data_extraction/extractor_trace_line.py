from typing import List, Tuple

import cv2
import numpy as np
from scipy.spatial import distance as sci_distance

from evaluation.data_extraction.extractor import Extractor
from evaluation.data_extraction.scene_data import TraceLineData
from scenarios.scene_trace_line import SceneTraceLine
from utils.color_utils import FlatColors
from utils.img_utils import write_on_img

SHOW_TRACE_LINE_FLOW = False
SHOW_FLOW_WAIT = 33


class ExtractorTraceLine(Extractor):
    data: TraceLineData = None
    scene: SceneTraceLine = None
    scene_type = SceneTraceLine

    def __init__(self, scene: SceneTraceLine, init_info, phase):
        super(ExtractorTraceLine, self).__init__(scene, init_info, phase)

    def extract_data(self) -> TraceLineData:
        if self.scene.is_finished():

            centroids_per_frame: dict[int, List[np.ndarray]] = {}

            for pid, history_list in self.scene.physical_entities_location_history.items():
                for loc_time_frame in history_list:
                    if self.scene.start_time <= loc_time_frame.timestamp <= self.scene.finish_time:
                        if loc_time_frame.frame_id not in centroids_per_frame:
                            centroids_per_frame[loc_time_frame.frame_id] = []
                        centroids_per_frame[loc_time_frame.frame_id].append(loc_time_frame.centroid)

            min_distances: List[float] = []

            centroids_per_frame: List[Tuple[int, np.ndarray]] = sorted(centroids_per_frame.items())

            for frame_id, centroids in centroids_per_frame:
                min_dist = sci_distance.cdist(centroids, self.scene.trace_line.points).min()
                min_distances.append(min_dist)

                if SHOW_TRACE_LINE_FLOW:
                    output = np.zeros(self.scene.workspace.virtual_workspace_shape_scaled)
                    output = np.stack((output,) * 3, axis=-1).astype(np.uint8)
                    pts = [np.asarray(self.scene.trace_line.points).astype(np.int32)]
                    cv2.polylines(output, pts=pts, isClosed=False,
                                  color=tuple(FlatColors.GREEN_SEA))
                    for c in centroids:
                        cv2.circle(output, tuple(c.astype(np.int)), radius=3, color=FlatColors.BELIZE_HOLE,
                                   thickness=1)

                    write_on_img(output, str(min_dist), np.asarray([20, 20]), font_scale=0.5)

                    cv2.imshow("SHOW_TRACE_LINE_FLOW", output)
                    cv2.waitKey(SHOW_FLOW_WAIT)

            self.data.closest_distances = min_distances
        return self.data
