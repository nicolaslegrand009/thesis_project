from evaluation.data_extraction.extractor import Extractor
from evaluation.data_extraction.scene_data import SimonSaysData
from scenarios.scene_simon_says import SceneSimonSays


class ExtractorSimonSays(Extractor):
    data: SimonSaysData = None
    scene: SceneSimonSays = None
    scene_type = SceneSimonSays

    def __init__(self, scene: SceneSimonSays, init_info, phase):
        super(ExtractorSimonSays, self).__init__(scene, init_info, phase)

    def extract_data(self) -> SimonSaysData:
        if self.scene.is_finished():
            self.data.round_durations = [r.end_time - r.start_time for r in self.scene.round_info_history]
        return self.data