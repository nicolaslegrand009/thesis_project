from dataclasses import dataclass
from typing import List


@dataclass
class SceneData:
    scene_type = None
    start_time = None
    finish_time = None
    duration: int = None
    candidate_id: str = None
    hand_mode: str = None
    group: str = None
    haptics_enabled: bool = None
    phase: str = None


@dataclass
class TraceLineData(SceneData):
    closest_distances: List[float] = None


@dataclass
class SimonSaysData(SceneData):
    round_durations: List[float] = None


@dataclass
class ObstacleCourseData(SceneData):
    closest_pebble_obstacles_distances: List[float] = None
    collision_durations: List[float] = None
    collision_frame_count: int = None
    collision_free_frame_count: int = None
    collision_count: int = None