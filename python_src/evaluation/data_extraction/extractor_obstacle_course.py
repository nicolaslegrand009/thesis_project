import cv2
import numpy as np
from scipy.spatial import distance as sci_distance

from evaluation.data_extraction.extractor import Extractor
from evaluation.data_extraction.scene_data import ObstacleCourseData
from scenarios.scene_obstacle_course import SceneObstacleCourse
from utils.color_utils import FlatColors

SHOW_OBSTACLE_FLOW = False
SHOW_FLOW_WAIT = 33


class ExtractorObstacleCourse(Extractor):
    data: ObstacleCourseData = None
    scene: SceneObstacleCourse = None
    scene_type = SceneObstacleCourse

    def __init__(self, scene: SceneObstacleCourse, init_info, phase):
        super(ExtractorObstacleCourse, self).__init__(scene, init_info, phase)

    def extract_data(self) -> ObstacleCourseData:
        if self.scene.is_finished():
            obstacle_points = self.scene.obstacle_edge_points
            min_distances = self._extract_min_distances(obstacle_points)
            frame_count_collision_occurring, frame_count_no_collision_occurring, collision_durations, new_collision_count = self._extract_collision_data()

            self.data.closest_pebble_obstacles_distances = min_distances

            self.data.collision_frame_count = frame_count_collision_occurring
            self.data.collision_free_frame_count = frame_count_no_collision_occurring
            self.data.collision_durations = collision_durations
            self.data.collision_count = new_collision_count

        return self.data

    def _extract_collision_data(self):
        output_shape = self.scene.workspace.virtual_workspace_shape_scaled
        obstacle_masks = np.zeros(output_shape).astype(np.bool)

        for o in self.scene.obstacles:
            collision_mask = o.get_collision_mask(obstacle_masks.shape)
            obstacle_masks = np.logical_or(obstacle_masks, collision_mask)

        collision_count = 0
        no_collision_count = 0

        collision_start_time = None
        collision_durations = []
        new_collision_count = 0

        for pebble_state in self.scene.pebble_state_history:
            if self.scene.start_time <= pebble_state.timestamp <= self.scene.finish_time:
                pebble_mask = np.zeros(output_shape)
                cv2.ellipse(pebble_mask,
                            center=tuple(np.asarray(pebble_state.centroid).astype(int)),
                            axes=(int(self.scene.pebble_radius_rescaled),
                                  int(pebble_state.axis_factor * self.scene.pebble_radius_rescaled)),
                            angle=int(pebble_state.rotation_angle),
                            startAngle=int(0.0),
                            endAngle=int(360.0),
                            color=255,
                            thickness=self.scene.pebble.collision_thickness,
                            lineType=cv2.LINE_AA
                            )
                overlap_mask = np.logical_and(pebble_mask, obstacle_masks)

                if SHOW_OBSTACLE_FLOW:
                    merged_display = np.zeros(output_shape)
                    merged_display = np.stack((merged_display,) * 3, axis=-1).astype(np.uint8)
                    merged_display[pebble_mask != 0] = FlatColors.PETER_RIVER
                    merged_display[obstacle_masks] = FlatColors.WHITE
                    merged_display[overlap_mask] = FlatColors.POMEGRANATE
                    cv2.imshow("SHOW_OBSTACLE_FLOW", merged_display)
                    cv2.waitKey(SHOW_FLOW_WAIT)

                is_colliding = np.any(overlap_mask)

                if is_colliding:
                    if collision_start_time is None:
                        collision_start_time = pebble_state.timestamp
                        new_collision_count += 1
                    collision_count += 1
                else:
                    if collision_start_time is not None:
                        collision_durations.append(pebble_state.timestamp - collision_start_time)
                        collision_start_time = None
                    no_collision_count += 1

        if collision_start_time is not None:
            collision_durations.append(self.scene.finish_time - collision_start_time)

        return collision_count, no_collision_count, collision_durations, new_collision_count

    def _extract_min_distances(self, obstacle_points):
        min_distances = []
        for pebble_state in self.scene.pebble_state_history:
            if self.scene.start_time <= pebble_state.timestamp <= self.scene.finish_time:
                pebble_points = cv2.ellipse2Poly(
                    center=tuple(np.asarray(pebble_state.centroid).astype(int)),
                    axes=(int(self.scene.pebble_radius_rescaled),
                          int(pebble_state.axis_factor * self.scene.pebble_radius_rescaled)),
                    angle=int(pebble_state.rotation_angle),
                    arcStart=int(0.0),
                    arcEnd=int(360.0),
                    delta=int(30)
                )
                min_dist = sci_distance.cdist(pebble_points, obstacle_points).min()

                min_distances.append(min_dist)
        return min_distances
