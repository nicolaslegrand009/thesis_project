import argparse

import dill

from evaluation.image_extraction.scene_image_extraction import extract_obstacle_course_images, \
    extract_trace_line_images, extract_simon_says_images
from scenarios.scene_obstacle_course import SceneObstacleCourse
from scenarios.scene_simon_says import SceneSimonSays
from scenarios.scene_trace_line import SceneTraceLine


def parse_console_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", action="store", dest="file_path", help="File path (.pkl) containing scene data")
    parser.add_argument("-o", action="store", dest="output_folder", help="Output folder")
    console_args = parser.parse_args()

    error_arg_found = False

    if console_args.file_path is None:
        print("Please specify a valid file path using the -f option")
        error_arg_found = True

    if console_args.output_folder is None:
        print("Please specify a valid folder path using the -o option")
        error_arg_found = True

    if error_arg_found:
        input("Press [enter] to exit")
        exit(1)

    return console_args


def main():
    console_args = parse_console_args()
    file_path = console_args.file_path
    output_folder_path = console_args.output_folder

    with open(file_path, "rb") as f:
        scene = dill.load(f)
        if isinstance(scene, SceneSimonSays):
            extract_simon_says_images(scene, output_folder_path)
        elif isinstance(scene, SceneObstacleCourse):
            extract_obstacle_course_images(scene, output_folder_path)
        elif isinstance(scene, SceneTraceLine):
            extract_trace_line_images(scene, output_folder_path)
        else:
            raise Exception("Scene unrecognized")


if __name__ == "__main__":
    main()
