import logging
import os
import warnings

import cv2
import numpy as np
from imutils.perspective import order_points
from reportlab.graphics import renderPM
from svglib.svglib import svg2rlg

from camera.camera import Camera
from config import config, calibrations_folder_path, errors_folder_path
from display.display_lib import debug_display
from display.projector import Projector
from models.calibration.depth_info import DepthInfo
from models.calibration.workspace import Workspace
from third_party.gen_pattern import PatternMaker
from utils.img_utils import get_bounding_values

CHESSBOARD_SVG_PATH = os.path.abspath(calibrations_folder_path + "/chessboard.svg")
CHESSBOARD_JPG_PATH = os.path.abspath(calibrations_folder_path + "/chessboard.jpg")


def extract_ordered_corners_from_shape(shape):
    corners = np.asarray([[0, 0], [shape[1], 0],
                          [shape[1], shape[0]],
                          [0, shape[0]]])
    return order_points(corners)


def extract_background_depth(training_depth_frames):
    with warnings.catch_warnings():  # ignore warnings for this block
        warnings.simplefilter("ignore", category=RuntimeWarning)

        training_depth_frames = np.asarray(training_depth_frames, dtype=np.float64)
        training_depth_frames[(training_depth_frames <= 0)] = None

        baseline = np.nanmean(training_depth_frames, axis=0).astype(dtype=np.float64)
        baseline[(np.isnan(baseline))] = -1

        std = np.nanstd(training_depth_frames, axis=0).astype(dtype=np.float64)
        baseline_cutoff = baseline - (std * 3)
        baseline_cutoff[(np.isnan(baseline_cutoff))] = -1

    return baseline, baseline_cutoff


def extract_chessboard_corners(chessboard_intersections, pattern_size):
    src_corners = np.asarray([chessboard_intersections[0],
                              chessboard_intersections[pattern_size[0] - 1],
                              chessboard_intersections[-1],
                              chessboard_intersections[-pattern_size[0]]
                              ])
    src_corners = order_points(np.squeeze(src_corners))
    return src_corners


def extract_workspace_shape(window_shape, chessboard_square_size):
    offset_x = int(
        ((window_shape[1] % chessboard_square_size) / 2) + chessboard_square_size)
    offset_y = int(
        ((window_shape[0] % chessboard_square_size) / 2) + chessboard_square_size)

    offset_shape = (offset_y, offset_x)

    workspace_shape = (int(window_shape[0] - (offset_shape[0] * 2)),
                       int(window_shape[1] - (offset_shape[1] * 2)))

    return workspace_shape, offset_shape


class ProjectorCameraCalibrator:
    """Calibrates virtual and physical world"""

    CHESSBOARD_SQUARE_SIZE = config["workspace_calibrator"]["CHESSBOARD_SQUARE_SIZE"]
    MAX_CALIBRATION_FRAME_COUNT = config["workspace_calibrator"]["MAX_CALIBRATION_FRAMES"]
    MIN_DEPTH_CALIBRATION_FRAME_COUNT = config["workspace_calibrator"]["MIN_DEPTH_CALIBRATION_FRAMES"]
    VIRTUAL_WORKSPACE_SCALE = config["workspace_calibrator"]["VIRTUAL_WORKSPACE_SCALE"]
    FRAME_SKIP_COUNT: int = config["workspace_calibrator"]["SKIP_FIRST_FRAMES_COUNT"]

    # calculated properties
    chessboard_cols = None
    chessboard_rows = None
    pattern_size = None

    def __init__(self, camera: Camera, projector: Projector):
        logging.getLogger(ProjectorCameraCalibrator.__name__).debug(f"{ProjectorCameraCalibrator.__name__}()")
        self.camera = camera
        self.projector = projector

        self.chessboard_cols = int(projector.WINDOW_SHAPE_YX[1] / self.CHESSBOARD_SQUARE_SIZE)
        self.chessboard_rows = int(projector.WINDOW_SHAPE_YX[0] / self.CHESSBOARD_SQUARE_SIZE)
        self.pattern_size = (self.chessboard_cols - 1, self.chessboard_rows - 1)

    def _depth_calibration_loop(self):
        logging.getLogger(ProjectorCameraCalibrator.__name__).debug(
            f"\t\t{ProjectorCameraCalibrator.__name__}.{self._depth_calibration_loop.__name__}()")
        black_canvas = np.zeros(self.projector.WINDOW_SHAPE_YX)
        black_canvas = cv2.merge([black_canvas, black_canvas, black_canvas])
        self.projector.display_img(black_canvas, disable_fps_display=True)
        cv2.waitKey(10)

        training_depth_frames = []
        calibration_frame_counter = 0
        while calibration_frame_counter < self.MIN_DEPTH_CALIBRATION_FRAME_COUNT:
            color_img, depth_data = self.camera.get_next_frames(is_depth_data_post_processed=True)
            training_depth_frames.append(np.copy(depth_data))
            calibration_frame_counter += 1
            cv2.waitKey(1)
        return training_depth_frames

    def _skip_first_frames(self):
        for i in range(0, self.FRAME_SKIP_COUNT):
            self.camera.get_next_frames()

    def start_calibration(self) -> (Workspace, DepthInfo):
        logging.getLogger(ProjectorCameraCalibrator.__name__).debug(
            f"\t{ProjectorCameraCalibrator.__name__}.{self.start_calibration.__name__}()")

        self._skip_first_frames()

        workspace = self._calibrate_workspace()

        depth_info = self._calibrate_depth()

        return workspace, depth_info

    def _calibrate_workspace(self):
        chessboard_intersections, color_img = self._chessboard_calibration_loop()

        live_workspace_corners = extract_chessboard_corners(chessboard_intersections, self.pattern_size)

        self._set_camera_roi(live_workspace_corners)

        workspace_shape, offset_shape = extract_workspace_shape(self.projector.WINDOW_SHAPE_YX,
                                                                self.CHESSBOARD_SQUARE_SIZE)

        workspace_shape_scaled = np.asarray([int(workspace_shape[0] * self.VIRTUAL_WORKSPACE_SCALE),
                                             int(workspace_shape[1] * self.VIRTUAL_WORKSPACE_SCALE)])

        virtual_workspace_corners_scaled = extract_ordered_corners_from_shape(workspace_shape_scaled)

        transformation_matrix = cv2.getPerspectiveTransform(live_workspace_corners,
                                                            virtual_workspace_corners_scaled)

        workspace: Workspace = Workspace(virtual_workspace_scaled=workspace_shape_scaled,
                                         virtual_workspace_shape_scale=self.VIRTUAL_WORKSPACE_SCALE,
                                         virtual_workspace_offset=offset_shape,
                                         transformation_matrix=transformation_matrix)

        if __debug__:
            self.___debug_show_workspace_transform(color_img, workspace)

        return workspace

    def _calibrate_depth(self):

        training_depth_frames = self._depth_calibration_loop()

        baseline_depth, baseline_depth_padded = extract_background_depth(
            training_depth_frames)

        depth_calibration_info: DepthInfo = DepthInfo(baseline_depth=baseline_depth,
                                                      baseline_depth_padded=baseline_depth_padded,
                                                      input_depth_scale=self.camera.get_depth_scale())
        if __debug__:
            self.___debug_show_baseline_coverage(baseline_depth)

        return depth_calibration_info

    def _set_camera_roi(self, live_workspace_corners):
        min_x, min_y, max_x, max_y = get_bounding_values(live_workspace_corners)
        self.camera.set_roi(min_x=min_x,
                            min_y=min_y,
                            max_x=max_x,
                            max_y=max_y
                            )

    def ___debug_show_baseline_coverage(self, baseline_depth_map_min):
        if config["debug_display"]["SHOW_BASELINE_COVERAGE"]:
            debug_img = np.ones(baseline_depth_map_min.shape).astype(np.uint8)
            debug_img[baseline_depth_map_min < 0] = 0
            debug_img = debug_img * 255
            debug_display("SHOW_BASELINE_COVERAGE", debug_img)

    def _generate_and_save_chessboard(self):
        pm = PatternMaker(cols=self.chessboard_cols,
                          rows=self.chessboard_rows,
                          units="px",
                          square_size=self.CHESSBOARD_SQUARE_SIZE,
                          page_height=self.projector.WINDOW_SHAPE_YX[0],
                          page_width=self.projector.WINDOW_SHAPE_YX[1],
                          radius_rate=1,
                          output=CHESSBOARD_SVG_PATH,
                          )
        pm.makeCheckerboardPattern()
        pm.save()
        checker_board = svg2rlg(CHESSBOARD_SVG_PATH)
        renderPM.drawToFile(checker_board, CHESSBOARD_JPG_PATH, fmt="JPG")
        chessboard = cv2.imread(CHESSBOARD_JPG_PATH, 0)
        return chessboard

    def _chessboard_calibration_loop(self):
        logging.getLogger(ProjectorCameraCalibrator.__name__).debug(
            f"\t\t{ProjectorCameraCalibrator.__name__}.{self._chessboard_calibration_loop.__name__}()")

        chessboard = self._generate_and_save_chessboard()
        self.projector.display_img(chessboard, disable_fps_display=True)
        color_img, gray_img, depth_data = None, None, None

        # loop until chessboard found
        calibration_frame_counter = 0
        found_chessboard = False
        chessboard_intersections = None
        while not found_chessboard:
            color_img, depth_data = self.camera.get_next_frames(is_depth_data_post_processed=True)

            self.projector.display_img(chessboard, disable_fps_display=True)
            cv2.waitKey(1)

            gray_img = cv2.cvtColor(color_img, cv2.COLOR_BGR2GRAY)

            # Find the chess board corners
            found_chessboard, chessboard_intersections = cv2.findChessboardCorners(gray_img,
                                                                                   patternSize=self.pattern_size,
                                                                                   corners=None,
                                                                                   flags=cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE)

            calibration_frame_counter += 1
            if calibration_frame_counter >= self.MAX_CALIBRATION_FRAME_COUNT:
                error_file_name = errors_folder_path + "/failed_finding_chessboard.png"
                cv2.imwrite(error_file_name, color_img)
                raise Exception(f"Taking too long to find chessboard corners. Check file '{error_file_name}' for image")

        chessboard_intersections = self._refine_chessboard(color_img, gray_img, chessboard_intersections)

        return chessboard_intersections, color_img

    def _refine_chessboard(self, color_img, gray_img, chessboard_intersections):
        # termination criteria
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
        chessboard_intersections_refined = cv2.cornerSubPix(gray_img, chessboard_intersections, (11, 11), (-1, -1),
                                                            criteria)
        if __debug__:
            self.___debug_show_chessboard_detected(color_img, chessboard_intersections_refined)

        return chessboard_intersections_refined

    def ___debug_show_chessboard_detected(self, color_img, chessboard_intersections_refined):
        if config["debug_display"]["SHOW_CHESSBOARD_DETECTED"]:
            output_img = np.copy(color_img)
            output_img = cv2.drawChessboardCorners(output_img,
                                                   patternSize=self.pattern_size,
                                                   corners=chessboard_intersections_refined,
                                                   patternWasFound=True)
            debug_display('SHOW_CHESSBOARD_DETECTED', output_img)

    def ___debug_show_workspace_transform(self, color_img, workspace: Workspace):
        if config["debug_display"]["SHOW_WORKSPACE_TRANSFORM"]:
            output = np.copy(color_img)
            output = cv2.warpPerspective(output, workspace.transformation_matrix,
                                         (workspace.virtual_workspace_shape_scaled[1],
                                          workspace.virtual_workspace_shape_scaled[0])
                                         )
            debug_display("SHOW_WORKSPACE_TRANSFORM", output)
