from multiprocessing import Queue, Process, Pipe, connection

import cv2

from display.projector import Projector
from display.second_screen_projector import SecondScreenProjector


def projector_queue_loop(projector_queue: Queue):
    project = SecondScreenProjector()
    while 1:
        rep = projector_queue.get()
        project.display_img(img=rep[0], disable_fps_display=rep[1])
        cv2.waitKey(1)


def projector_pipe_loop(p_output: connection.Connection):
    project = SecondScreenProjector()
    while 1:
        obj = p_output.recv()
        project.display_img(img=obj[0], disable_fps_display=obj[1])
        cv2.waitKey(1)


class ProjectorProcessWrapper(Projector):
    projection_queue: Queue = None
    projector_process: Process = None
    pipe_mode = True
    p_input: connection.Connection = None

    def __init__(self):

        if self.pipe_mode:
            p_output, p_input = Pipe()
            projector_process = Process(target=projector_pipe_loop, args=(p_output,))
            self.p_input = p_input
        else:
            projection_queue = Queue(maxsize=1)
            projector_process = Process(target=projector_queue_loop, args=(projection_queue,))
            self.projection_queue = projection_queue

        projector_process.start()
        self.projector_process = projector_process

    def display_img(self, img, disable_fps_display=False):
        if self.pipe_mode:
            self.p_input.send([img, disable_fps_display])
        else:
            self.projection_queue.put([img, disable_fps_display])

    def __del__(self):
        print("Terminating projector process")
        self.projector_process.terminate()
