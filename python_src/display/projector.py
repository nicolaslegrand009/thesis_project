from config import config


class Projector:
    """Projector display wrapper class"""

    WINDOW_NAME: str = "PROJECTED_WINDOW"
    WINDOW_SHAPE_YX = config["projector"]["PROJECTED_WINDOW_SHAPE"]
    WINDOW_POSITION_YX = config["projector"]["PROJECTED_WINDOW_POSITION"]
    SHOW_FPS: bool = config["projector"]["SHOW_FPS"]

    def display_img(self, img, disable_fps_display=False):
        pass
