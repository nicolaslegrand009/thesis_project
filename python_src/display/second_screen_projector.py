import logging
import time

import cv2
import numpy as np

from config import config
from display.display_lib import debug_display, init_window
from display.projector import Projector
from utils.img_utils import write_on_img


class SecondScreenProjector(Projector):
    """Projector display wrapper class"""

    FPS_HISTORY_COUNT = 10

    now = None
    last = None
    frame = None
    fps = None

    def __init__(self):
        logging.getLogger(Projector.__name__).debug(f"{Projector.__name__}()")
        self.last = 0
        self.frame = 0
        self.fps = 0

        init_window(title=self.WINDOW_NAME,
                    shape_xy=self.WINDOW_SHAPE_YX[::-1],
                    position_xy=self.WINDOW_POSITION_YX[::-1],
                    is_fullscreen=True)

    def display_img(self, img, disable_fps_display=False):
        if self.SHOW_FPS:
            if self.frame % self.FPS_HISTORY_COUNT == 0:
                self.now = time.time()
                diff = self.now - self.last

                if self.frame != 0:
                    self.fps = self.FPS_HISTORY_COUNT / diff

                self.last = self.now
            if not disable_fps_display:
                write_on_img(img, str(round(self.fps, 2)), np.asarray([50, 50]))
            self.frame += 1

        cv2.imshow(self.WINDOW_NAME, img)

        if __debug__:
            if config["debug_display"]["SHOW_PROJECTOR_IMAGE"]:
                debug_display("SHOW_PROJECTOR_IMAGE", img)
