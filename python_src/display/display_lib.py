import logging
import textwrap

import cv2
import numpy as np

from config import config
from utils.color_utils import FlatColors
from utils.img_utils import scale_image

_instruction_display_initialized = False
_INSTRUCTION_DISPLAY_SHAPE = config["instructions_display"]["INSTRUCTION_DISPLAY_SHAPE"]
_IS_INSTRUCTION_FULLSCREEN = config["instructions_display"]["INSTRUCTION_DISPLAY_FULLSCREEN"]
INSTRUCTION_DISPLAY_NAME = "INSTRUCTIONS"

if __debug__:
    _IS_INSTRUCTION_FULLSCREEN = False

_DEBUG_DISABLE_INSTRUCTIONS = config["debug_display"]["DISABLE_INSTRUCTIONS_WINDOW"]


def init_window(title, shape_xy, position_xy, is_fullscreen=False):
    if is_fullscreen:
        cv2.namedWindow(title, flags=cv2.WND_PROP_VISIBLE)
        cv2.moveWindow(title, position_xy[0], position_xy[1])
        cv2.setWindowProperty(title, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    else:
        cv2.namedWindow(title, flags=cv2.WND_PROP_AUTOSIZE)
        cv2.moveWindow(title, position_xy[0], position_xy[1])

    img = np.zeros(shape_xy)
    cv2.imshow(title, img)
    cv2.waitKey(1)


def debug_display(title, img):
    cv2.imshow(title, img)


def display_instruction(msg: str,
                        bg_color=FlatColors.MIDNIGHT_BLUE,
                        col_char_count: int = 50,
                        line_space=50,
                        color=FlatColors.BLACK,
                        font_size=1,
                        font_thickness=2,
                        font=cv2.FONT_HERSHEY_SIMPLEX):
    global _instruction_display_initialized

    if __debug__ and _DEBUG_DISABLE_INSTRUCTIONS:
        logging.getLogger(f"{display_instruction.__name__}").debug("Instruction")
        logging.getLogger(f"{display_instruction.__name__}").debug(msg)
    else:
        if not _instruction_display_initialized:
            _init_instruction_display()

        img = np.zeros(_INSTRUCTION_DISPLAY_SHAPE).astype(np.uint8)

        img = cv2.merge([img, img, img])
        img[:] = bg_color

        wrapped_text = textwrap.wrap(msg, width=col_char_count)

        for i, line in enumerate(wrapped_text):
            text_size = cv2.getTextSize(line, font, font_size, font_thickness)[0]

            gap = text_size[1] + line_space

            y = int((img.shape[0] + text_size[1]) / 2) + i * gap
            x = int((img.shape[1] - text_size[0]) / 2)

            cv2.putText(img,
                        text=line,
                        org=(x, y),
                        fontFace=font,
                        fontScale=font_size,
                        color=tuple(color),
                        thickness=font_thickness,
                        lineType=cv2.LINE_AA)
            i += 1

        if __debug__:
            img = scale_image(img, 0.5)
        cv2.imshow(INSTRUCTION_DISPLAY_NAME, img)
        cv2.waitKey(1)


def _init_instruction_display():
    global _instruction_display_initialized
    init_window(title=INSTRUCTION_DISPLAY_NAME,
                shape_xy=_INSTRUCTION_DISPLAY_SHAPE,
                position_xy=[0, 0],
                is_fullscreen=_IS_INSTRUCTION_FULLSCREEN)
    _instruction_display_initialized = True
