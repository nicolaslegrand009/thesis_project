from .location_time_frame import LocationTimeFrame


class PebbleState(LocationTimeFrame):
    rotation_angle = None
    axis_factor = None

    def __init__(self, centroid, rotation_angle, axis_factor, frame_id, timestamp=None):
        super(PebbleState, self).__init__(centroid, frame_id, timestamp=timestamp)
        self.rotation_angle = rotation_angle
        self.axis_factor = axis_factor
