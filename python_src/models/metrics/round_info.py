import time


class RoundInfo:
    start_time: time.time = None
    end_time: time.time = None
    correct_answer: int = None
    user_answer: int = None

    def round_duration(self):
        return self.end_time - self.start_time

    def is_round_won(self):
        return self.correct_answer == self.user_answer

    def __init__(self, start_time, end_time, correct_answer, user_answer):
        self.start_time = start_time
        self.end_time = end_time
        self.correct_answer = correct_answer
        self.user_answer = user_answer