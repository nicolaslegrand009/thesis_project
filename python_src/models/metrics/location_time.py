import time


class LocationTime:
    centroid = None
    timestamp = None

    def __init__(self, centroid, timestamp=None):
        self.centroid = centroid
        self.timestamp = time.time() if timestamp is None else time.time()
