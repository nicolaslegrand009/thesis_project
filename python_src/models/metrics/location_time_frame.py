from .location_time import LocationTime


class LocationTimeFrame(LocationTime):
    frame_id: int = None

    def __init__(self, centroid, frame_id: int, timestamp=None):
        super(LocationTimeFrame, self).__init__(centroid=centroid, timestamp=timestamp)
        self.frame_id = frame_id
