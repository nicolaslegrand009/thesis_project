from enum import Enum


class HandEnum(Enum):
    RIGHT = "RIGHT",
    LEFT = "LEFT"
