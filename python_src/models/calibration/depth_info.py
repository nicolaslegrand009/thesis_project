class DepthInfo:
    baseline_depth = None
    baseline_depth_padded = None
    input_depth_scale = None

    def __init__(self, baseline_depth, baseline_depth_padded, input_depth_scale):
        self.baseline_depth = baseline_depth
        self.baseline_depth_padded = baseline_depth_padded
        self.input_depth_scale = input_depth_scale

    def convert_to_depth_units(self, measurement_in_meters):
        return measurement_in_meters / self.input_depth_scale
