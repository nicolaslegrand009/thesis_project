import cv2
import numpy as np

from utils.img_utils import scale_image


class Workspace:
    """Keeps calibration data and basic information used for processing interaction between virtual and physical
    world """

    virtual_workspace_offset = None
    virtual_workspace_scale = None
    virtual_workspace_shape_scaled = None
    transformation_matrix = None

    def __init__(self, virtual_workspace_scaled, virtual_workspace_shape_scale, virtual_workspace_offset,
                 transformation_matrix):
        self.virtual_workspace_shape_scaled = np.asarray(virtual_workspace_scaled)
        self.virtual_workspace_scale = np.asarray(virtual_workspace_shape_scale)
        self.virtual_workspace_offset = np.asarray(virtual_workspace_offset)
        self.transformation_matrix = transformation_matrix

    def transform_to_virtual_points(self, points):
        points = np.asarray([points], dtype=np.float32)
        return cv2.perspectiveTransform(points, self.transformation_matrix)

    def transform_to_virtual_img(self, img):
        return cv2.warpPerspective(img, self.transformation_matrix,
                                   tuple(self.virtual_workspace_shape_scaled[::-1]))

    def rebuild_projector_sized_image(self, virtual_img):
        projection_img = scale_image(virtual_img, (1 / self.virtual_workspace_scale))
        offset = self.virtual_workspace_offset
        projection_img = np.pad(projection_img, ((offset[0], offset[0]), (offset[1], offset[1]), (0, 0)),
                                mode='constant')
        return projection_img
