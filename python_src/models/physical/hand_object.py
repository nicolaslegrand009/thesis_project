import cv2
import numpy as np

from .physical_object import PhysicalObject


class HandObject(PhysicalObject):
    finger_tips = None
    finger_joints = None
    centroid = None

    def get_collision_mask(self, container_shape):
        finger_radius = 10
        m = np.zeros(container_shape)
        for t in self.finger_tips:
            cv2.circle(m, tuple(t), finger_radius, 255, -1)
        m = np.where(self.color_based_mask == 0, 0, m)
        return m

    def __init__(self, p_obj: PhysicalObject, finger_tips, finger_joints):
        super(HandObject, self).__init__(depth_based_contour=p_obj.depth_based_contour,
                                         depth_based_mask=p_obj.depth_based_mask,
                                         color_based_contour=p_obj.color_based_contour,
                                         color_based_mask=p_obj.color_based_mask
                                         )

        self.finger_joints = np.asarray(finger_joints)
        self.finger_tips = np.asarray(finger_tips)

    def draw_self(self, img):
        self.draw_color_contour(img)
        self.draw_finger_tips(img)
        self.draw_finger_joints(img)
        self.draw_centroid(img)

    def draw_finger_tips(self, img):
        for c in self.finger_tips:
            cv2.circle(img, center=tuple(c), radius=5, color=(255, 255, 0), thickness=3)

    def draw_finger_joints(self, img):
        for c in self.finger_joints:
            cv2.circle(img, center=tuple(c), radius=3, color=(255, 0, 0), thickness=2)
