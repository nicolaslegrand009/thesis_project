import cv2

from utils.math_utils import calculate_centroid


class PhysicalObject:
    depth_based_contour = None
    depth_based_mask = None
    color_based_contour = None
    color_based_mask = None
    touching_virtual_objects = []
    hovering_virtual_objects = []

    def get_collision_mask(self, container_shape):
        return self.color_based_mask

    def __init__(self, depth_based_contour, depth_based_mask, color_based_contour, color_based_mask):
        self.depth_based_mask = depth_based_mask
        self.depth_based_contour = depth_based_contour
        self.color_based_mask = color_based_mask
        self.color_based_contour = color_based_contour

    def draw_depth_contour(self, img):
        cv2.drawContours(img, [self.depth_based_contour], -1, color=(0, 0, 255), thickness=3)

    def draw_color_contour(self, img):
        cv2.drawContours(img, [self.color_based_contour], -1, color=(255, 0, 255), thickness=3)

    def get_centroid(self):
        return calculate_centroid(self.color_based_contour)

    def draw_centroid(self, img):
        cv2.circle(img, center=tuple(self.get_centroid()), radius=3, color=(0, 255, 255), thickness=3)
