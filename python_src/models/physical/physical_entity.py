import collections
from typing import List

from models.metrics.location_time import LocationTime
from models.physical.tracked_object import TrackedObject


class PhysicalEntity:
    HISTORY_SIZE = 3
    location_history: List[LocationTime] = None
    _latest_tracked_object: TrackedObject = None
    id = None

    def get_latest_loc_time(self):
        return self.location_history[-1]

    def get_latest_loc(self):
        loc_time = self.get_latest_loc_time()
        return None if loc_time is None else self.location_history[-1].centroid

    def get_displacement(self):
        if len(self.location_history) > 2:
            return self.location_history[-1].centroid - self.location_history[-2].centroid
        else:
            return None

    def __init__(self, _id):
        self.id = _id
        self.location_history = collections.deque(maxlen=self.HISTORY_SIZE)

    def set_latest_tracked_obj(self, obj: TrackedObject):
        self._latest_tracked_object = obj
        self.location_history.append(LocationTime(obj.get_centroid()))

    def get_latest_tracked_obj(self):
        return self._latest_tracked_object
