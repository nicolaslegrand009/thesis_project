from typing import Tuple

import cv2
import numpy as np

from config import config
from models.has_bounding_circle import HasBoundingCircle
from models.physical.tracked_object import TrackedObject


class MarkerObject(TrackedObject, HasBoundingCircle):
    COLLISION_MASK_RADIUS = config["models"]["FINGER_TIP_RADIUS"]

    color_based_mask = None
    color_based_contour = None
    centroid: np.ndarray = None
    collision_mask = None

    def __init__(self, color_based_mask, color_based_contour, centroid: np.ndarray):
        self.color_based_mask = color_based_mask
        self.centroid = centroid
        self.color_based_contour = color_based_contour

    def get_bounding_circle(self) -> Tuple[Tuple[any, any], any]:
        return self.centroid, self.COLLISION_MASK_RADIUS

    def get_centroid(self):
        return self.centroid

    def draw_color_contour(self, img):
        cv2.drawContours(img, [self.color_based_contour], -1, color=(255, 0, 255), thickness=-1)

    def get_collision_mask(self, container_shape):
        if self.collision_mask is None:
            mask = np.zeros(container_shape)
            cv2.circle(mask, tuple(np.asarray(self.centroid).astype(np.int)), self.COLLISION_MASK_RADIUS, 255, -1)
            self.collision_mask = mask
        return self.collision_mask
