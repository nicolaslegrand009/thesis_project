from typing import Tuple

import cv2
import numpy as np

from config import config
from models.has_bounding_circle import HasBoundingCircle
from models.physical.tracked_object import TrackedObject


class FingerTipObject(TrackedObject, HasBoundingCircle):
    COLLISION_MASK_RADIUS: int = config["models"]["FINGER_TIP_RADIUS"]
    centroid: np.ndarray = None
    collision_mask: np.ndarray = None

    def __init__(self, centroid: np.ndarray):
        self.centroid = centroid

    def get_bounding_circle(self) -> Tuple[Tuple[any, any], any]:
        return self.centroid, self.COLLISION_MASK_RADIUS

    def get_collision_mask(self, container_shape):
        if self.collision_mask is None:
            mask = np.zeros(container_shape)
            cv2.circle(mask, tuple(np.asarray(self.centroid).astype(np.int)), self.COLLISION_MASK_RADIUS, 255, -1)
            self.collision_mask = mask
        return self.collision_mask

    def get_centroid(self):
        return self.centroid
