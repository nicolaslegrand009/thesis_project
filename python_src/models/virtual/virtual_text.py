import cv2
import numpy as np

from models.virtual.virtual_object import VirtualObject
from utils.color_utils import FlatColors


class VirtualText(VirtualObject):
    location_xy: [int] = None
    font = cv2.FONT_HERSHEY_SIMPLEX
    font_scale = None
    message = None
    rotated = None
    thickness = 1
    center_text = False

    def __init__(self, location_xy, message, scale=1, color=FlatColors.WHITE, rotated=False, center_text=False):
        super().__init__()
        self.location_xy = location_xy
        self.message = message
        self.color = color
        self.font_scale = scale
        self.rotated = rotated
        self.center_text = center_text

    def get_collision_mask(self, container_shape):
        m = np.zeros(container_shape)
        return m

    def get_size(self):
        return cv2.getTextSize(self.message, self.font, self.font_scale, self.thickness)[0]

    def draw_self(self, img):
        temp = np.asarray(np.rot90(img, 2)).astype(np.uint8)
        draw_color = self.get_color()

        h, w, _ = img.shape

        text_size = self.get_size()

        loc_xy = self.location_xy

        if self.center_text:
            loc_xy = loc_xy + (np.asarray([text_size[0], 0]) / 2)

        if self.rotated:
            loc_xy = np.asarray([w, h]) - loc_xy

        cv2.putText(temp,
                    text=self.message,
                    org=tuple(np.asarray(loc_xy).astype(int)),
                    fontFace=self.font,
                    fontScale=self.font_scale,
                    color=draw_color,
                    thickness=self.thickness,
                    lineType=cv2.LINE_AA,
                    bottomLeftOrigin=False
                    )

        temp = np.asarray(np.rot90(temp, 2)).astype(np.uint8)

        np.copyto(dst=img, src=temp)
