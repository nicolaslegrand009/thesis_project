import cv2
import numpy as np

from models.virtual.virtual_object import VirtualObject
from utils.line_gen_utils import get_circular_bezier_curve, get_random_points
from utils.math_utils import xy_coords_to_points


class VirtualRandomClosedShape(VirtualObject):
    points = None
    thickness = None
    location_xy = [0, 0]
    _collision_mask = None

    def __init__(self, size, offset_xy, num_points, rad, edgy, thickness):
        a = get_random_points(n=num_points, scale=1)
        x_coords, y_coords, _ = get_circular_bezier_curve(a, rad=rad, edgy=edgy)

        x_coords *= size[0]
        y_coords *= size[1]

        x_coords += offset_xy[0]
        y_coords += offset_xy[1]

        self.points = xy_coords_to_points(x_coords, y_coords)
        self.thickness = thickness

    def draw_self(self, img):
        draw_color = self.get_color()
        cv2.drawContours(img, [self.points], -1, color=draw_color, thickness=self.thickness, lineType=cv2.LINE_AA)

    def get_collision_mask(self, container_shape):
        if self._collision_mask is None or not self.is_static:
            collision_mask = np.zeros(container_shape)
            cv2.drawContours(collision_mask, [self.points], -1, color=(255), thickness=self.thickness,
                             lineType=cv2.LINE_AA)
            self._collision_mask = collision_mask
        return self._collision_mask
