from typing import Tuple

import cv2
import numpy as np

from models.has_bounding_circle import HasBoundingCircle
from models.virtual.virtual_object import VirtualObject
from utils.math_utils import distance


class VirtualRectangle(VirtualObject, HasBoundingCircle):
    size_xy: [int] = None
    visual_thickness = None
    collision_thickness = None
    _collision_mask = None
    center_color = None

    def __init__(self, location_xy, size_xy, visual_thickness=-1, collision_thickness=-1):
        self.location_xy = location_xy
        self.size_xy = size_xy
        self.visual_thickness = visual_thickness
        self.collision_thickness = collision_thickness

    def get_bounding_circle(self) -> Tuple[Tuple[any, any], any]:
        centroid = np.add(self.location_xy, np.multiply(self.size_xy, 0.5))
        radius = distance(centroid, self.location_xy)
        return centroid, radius

    def get_collision_mask(self, container_shape):
        if self._collision_mask is None or not self.is_static:
            collision_mask = np.zeros(container_shape)
            p2 = np.add(self.location_xy, self.size_xy)
            cv2.rectangle(collision_mask, tuple(np.asarray(self.location_xy).astype(int)),
                          tuple(np.asarray(p2).astype(int)), 255,
                          self.collision_thickness)
            self._collision_mask = collision_mask
        return self._collision_mask

    def get_poly_points(self, delta):
        f"""Get points on edge of {VirtualRectangle.__name__} with a distance of delta between each other"""
        pts = []
        loc_xy = np.asarray(self.location_xy)
        size_xy = np.asarray(self.size_xy)
        p2 = loc_xy + size_xy
        dist = p2 - loc_xy
        inc = np.asarray(dist / delta)

        pts.extend([loc_xy + np.asarray([inc[0] * f, 0]) for f in range(delta + 1)])
        pts.extend([loc_xy + np.asarray([0, inc[1] * f]) for f in range(1, delta + 1)])

        pts.extend([loc_xy + np.asarray([dist[0], inc[1] * f]) for f in range(1, delta + 1)])
        pts.extend([loc_xy + np.asarray([inc[0] * f, dist[1]]) for f in range(1, delta)])
        return pts

    def draw_self(self, img):
        draw_color = self.get_color()
        point2 = np.add(self.location_xy, self.size_xy)

        if self.visual_thickness != -1 and self.center_color is not None:
            cv2.rectangle(img, tuple(np.asarray(self.location_xy).astype(int)), tuple(np.asarray(point2).astype(int)),
                          self.center_color, -1)

        cv2.rectangle(img, tuple(np.asarray(self.location_xy).astype(int)), tuple(np.asarray(point2).astype(int)),
                      draw_color, self.visual_thickness)
