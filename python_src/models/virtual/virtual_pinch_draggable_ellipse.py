import math
from typing import List

import cv2
import numpy as np

from models.physical.physical_entity import PhysicalEntity
from models.virtual.virtual_circle import VirtualCircle
from models.virtual.virtual_pinch_draggable_object import VirtualPinchDraggableObject
from utils.math_utils import distance, rad_to_deg


class VirtualPinchDraggableEllipse(VirtualPinchDraggableObject, VirtualCircle):
    MIN_SQUEEZE = 0.3

    def __init__(self, location_xy, radius, visual_thickness=-1, collision_thickness=-1):
        self.location_xy = location_xy
        self.radius = radius
        self.visual_thickness = visual_thickness
        self.collision_thickness = collision_thickness

    def get_visual_collision_mask(self, container_shape):

        v_mask = np.zeros(container_shape)

        factor, alpha = self.calculate_distortion_values()

        cv2.ellipse(v_mask,
                    center=tuple(np.asarray(self.location_xy).astype(int)),
                    axes=(int(self.radius), int(factor * self.radius)),
                    angle=int(alpha),
                    startAngle=int(0.0),
                    endAngle=int(360.0),
                    color=255,
                    thickness=self.visual_thickness,
                    lineType=cv2.LINE_AA
                    )
        return v_mask

    def get_poly_points(self, delta):
        f"""Get points on edge of {VirtualPinchDraggableEllipse.__name__} with a distance of delta between each other"""
        axis_factor, rotation_angle = self.calculate_distortion_values()
        return cv2.ellipse2Poly(
            center=tuple(np.asarray(self.location_xy).astype(int)),
            axes=(int(self.radius), int(axis_factor * self.radius)),
            angle=int(rotation_angle),
            arcStart=int(0.0),
            arcEnd=int(360.0),
            delta=int(delta)
        )

    def calculate_distortion_values(self):
        axis_factor = 1
        rotation_angle = 0
        if self._is_pinched():
            couple: List[PhysicalEntity] = self.drag_candidates[:2]
            couple = [c.get_latest_loc() for c in couple]
            a, b = couple
            dist = distance(a, b)

            axis_factor = dist / (self.radius * 2)
            if axis_factor < self.MIN_SQUEEZE:
                axis_factor = self.MIN_SQUEEZE
            elif axis_factor > 1:
                axis_factor = 1

            if b[0] - a[0] != 0:
                tan_alpha = (b[1] - a[1]) / (b[0] - a[0])
                rotation_angle = math.atan(tan_alpha)
                rotation_angle = rad_to_deg(rotation_angle)
                rotation_angle = 90 + rotation_angle
        return axis_factor, rotation_angle

    def draw_self(self, img):
        draw_color = self.get_color()
        axis_factor, rotation_angle = self.calculate_distortion_values()

        cv2.ellipse(img,
                    center=tuple(np.asarray(self.location_xy).astype(int)),
                    axes=(int(self.radius), int(axis_factor * self.radius)),
                    angle=int(rotation_angle),
                    startAngle=int(0.0),
                    endAngle=int(360.0),
                    color=draw_color,
                    thickness=self.visual_thickness,
                    lineType=cv2.LINE_AA
                    )
