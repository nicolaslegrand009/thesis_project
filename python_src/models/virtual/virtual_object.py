import time

from utils.color_utils import FlatColors


class VirtualObject:
    color = FlatColors.SUN_FLOWER
    hovered_color = FlatColors.EMERALD
    touched_color = FlatColors.PETER_RIVER
    pinched_color = FlatColors.WISTERIA

    TIMEOUT = 0.05

    is_static = False

    is_hoverable = True
    is_touchable = True

    touched_time = None
    hovered_time = None

    on_initial_touch_function = None
    on_initial_hover_function = None

    on_touch_function = None
    on_hover_function = None

    location_xy: [int] = None

    def trigger_touched(self):
        if self.on_initial_touch_function is not None and not self.is_touched():
            self.on_initial_touch_function()

        if self.on_touch_function is not None:
            self.on_touch_function()

        self.touched_time = time.time()

    def trigger_hovered(self):
        if self.on_initial_hover_function is not None and not self.is_hovered():
            self.on_initial_hover_function()

        if self.on_hover_function is not None:
            self.on_hover_function()

        self.hovered_time = time.time()

    def is_hovered(self):
        if self.hovered_time is None:
            return False
        return time.time() - self.hovered_time <= self.TIMEOUT

    def is_touched(self):
        if self.touched_time is None:
            return False
        return time.time() - self.touched_time <= self.TIMEOUT

    def get_collision_mask(self, container_shape):
        pass

    def draw_self(self, img):
        pass

    def get_color(self):
        draw_color = self.color
        if self.is_hovered():
            draw_color = self.hovered_color
        if self.is_touched():
            draw_color = self.touched_color
        return draw_color

    def set_all_colors(self, color):
        self.color = color
        self.hovered_color = color
        self.touched_color = color
