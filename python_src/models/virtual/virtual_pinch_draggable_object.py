import time
from enum import Enum
from typing import List

from models.physical.physical_entity import PhysicalEntity
from models.virtual.virtual_object import VirtualObject
from utils.color_utils import FlatColors


class PinchMode(Enum):
    HOVER = 1,
    TOUCH = 2


class VirtualPinchDraggableObject(VirtualObject):
    pinched_color = FlatColors.WISTERIA
    pinch_mode: PinchMode = PinchMode.HOVER
    drag_candidates: List[PhysicalEntity] = []
    pinched_time = None
    on_initial_pinch_function = None

    def trigger_pinched(self, physical_entities: List[PhysicalEntity]) -> bool:
        previous_candidate_ids = {x.id for x in self.drag_candidates}
        good_candidates = {p.id: p for p in physical_entities if p.id in previous_candidate_ids}
        self.drag_candidates = physical_entities if (len(good_candidates) < 2) else list(good_candidates.values())

        is_pinched = self._is_pinched()
        if is_pinched:
            if self.pinched_time is None and self.on_initial_pinch_function is not None:
                self.on_initial_pinch_function()
            self.pinched_time = time.time()
        else:
            self.pinched_time = None
        return is_pinched

    def _is_pinched(self):
        is_pinched = (len(self.drag_candidates) > 1) and (
                (self.is_hovered() and self.pinch_mode == PinchMode.HOVER) or (
                self.is_touched() and self.pinch_mode == PinchMode.TOUCH))
        return is_pinched

    def is_pinched(self):
        if self.pinched_time is None:
            return False
        return time.time() - self.pinched_time <= self.TIMEOUT

    def get_color(self):
        draw_color = super(VirtualPinchDraggableObject, self).get_color()

        if self.is_pinched():
            draw_color = self.pinched_color

        return draw_color

    def set_all_colors(self, color):
        super(VirtualPinchDraggableObject, self).set_all_colors(color)
        self.pinched_color = color
