import cv2
import numpy as np

from models.has_bounding_circle import HasBoundingCircle
from models.virtual.virtual_object import VirtualObject


class VirtualCircle(VirtualObject, HasBoundingCircle):
    radius: any = None
    visual_thickness = None
    collision_thickness = None
    _collision_mask = None

    def get_bounding_circle(self):
        return self.location_xy, self.radius

    def __init__(self, location_xy, radius, visual_thickness=-1, collision_thickness=-1):
        self.location_xy = location_xy
        self.radius = radius
        self.visual_thickness = visual_thickness
        self.collision_thickness = collision_thickness

    def get_collision_mask(self, container_shape):
        if self._collision_mask is None or not self.is_static:
            m = np.zeros(container_shape)
            cv2.circle(m, tuple(np.asarray(self.location_xy).astype(int)), int(self.radius), 255,
                       self.collision_thickness)
            self._collision_mask = m
        return self._collision_mask

    def draw_self(self, img):
        draw_color = self.get_color()
        cv2.circle(img,
                   center=tuple(np.asarray(self.location_xy).astype(int)),
                   radius=int(self.radius),
                   color=draw_color,
                   thickness=self.visual_thickness,
                   lineType=cv2.LINE_AA)
