from typing import Tuple

from utils.math_utils import distance


class HasBoundingCircle:
    def get_bounding_circle(self) -> Tuple[Tuple[any, any], any]:
        """returns (centroid,radius)"""
        pass

    def overlaps(self, other: 'HasBoundingCircle') -> Tuple[bool, any]:
        """returns (is_overlapping, distance_between_centroids)"""
        centroid, radius = self.get_bounding_circle()
        o_centroid, o_radius = other.get_bounding_circle()
        dist = distance(centroid, o_centroid)
        return dist <= radius + o_radius, dist
