class Camera:
    """Base class for depth camera"""

    def __init__(self, file_name=None):
        pass

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_value, tb):
        pass

    def get_depth_scale(self):
        pass

    def get_next_frames(self, is_depth_frame_required=True, is_depth_data_post_processed=False):
        """Returns color and depth frame"""
        pass

    def set_roi(self, min_x, min_y, max_x, max_y):
        pass
