import json
from typing import List

import numpy as np
import pyrealsense2 as rs

from camera.camera import Camera
from config import config, camera_presets_path
from display.display_lib import debug_display


class CameraSyncerAPI(Camera):
    """Camera class wrapping pyrealsense2 SDK using rs::syncer and rs::sensor"""

    INPUT_SHAPE: List = config["camera"]["CAMERA_SHAPE"]
    DEPTH_CAMERA_SHAPE: List = config["camera"]["DEPTH_CAMERA_SHAPE"]

    COLOR_FPS: int = config["camera"]["COLOR_FPS"]
    DEPTH_FPS: int = config["camera"]["DEPTH_FPS"]

    PRESET_FILE = config["camera"]["PRESET_FILE"]

    spacial_filter_magnitude = config["camera"]["F_SPATIAL_MAG"]
    spacial_filter_alpha = config["camera"]["F_SPATIAL_ALPHA"]
    spacial_filter_delta = config["camera"]["F_SPATIAL_DELTA"]
    spacial_filter: rs.spatial_filter = None
    colorizer = rs.colorizer = None

    SYNCER_CAPACITY = 2
    syncer: rs.syncer = None
    depth_scale = None
    color_sensor: rs.color_sensor = None
    depth_sensor: rs.depth_sensor = None
    roi_sensor: rs.roi_sensor = None
    alignment: rs.align = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        print(f"~{CameraSyncerAPI.__name__}")
        self.color_sensor.stop()
        self.depth_sensor.stop()

    def set_roi(self, min_x, min_y, max_x, max_y):
        roi = rs.region_of_interest()
        roi.min_x = int(min_x)
        roi.min_y = int(min_y)
        roi.max_x = int(max_x)
        roi.max_y = int(max_y)
        self.roi_sensor.set_region_of_interest(roi)

    def _load_preset(self, device: rs.device):
        device_as_advanced: rs.rs400_advanced_mode = rs.rs400_advanced_mode(device)
        with open(f"{camera_presets_path}/{self.PRESET_FILE}") as json_file:
            data = json.load(json_file)
            json_string = str(data).replace("'", '\"')
            device_as_advanced.load_json(json_string)
            print(f"\t{CameraSyncerAPI.__name__} loaded '{self.PRESET_FILE}'")

    def __init__(self, file_name=None):
        device_list: rs.device_list = rs.context().query_devices()

        if len(device_list) == 0:
            raise Exception("No intel realsense camera device found")

        device: rs.device = device_list[0]

        color_sensor: rs.color_sensor = device.first_color_sensor()
        color_sensor.set_option(rs.option.enable_auto_exposure, True)
        depth_sensor: rs.depth_sensor = device.first_depth_sensor()
        roi_sensor: rs.roi_sensor = device.first_roi_sensor()

        color_profiles = color_sensor.get_stream_profiles()
        depth_profiles = depth_sensor.get_stream_profiles()

        color_profile = None
        depth_profile = None

        for c_profile in color_profiles:
            if c_profile.is_video_stream_profile():
                c_profile: rs.video_stream_profile = c_profile.as_video_stream_profile()
                if c_profile.fps() == self.COLOR_FPS and c_profile.width() == self.INPUT_SHAPE[
                    1] and c_profile.height() == \
                        self.INPUT_SHAPE[
                            0] and c_profile.format() == rs.format.bgr8:
                    color_profile = c_profile
                    break

        for d_profile in depth_profiles:
            if d_profile.is_video_stream_profile():
                d_profile: rs.video_stream_profile = d_profile.as_video_stream_profile()
                if d_profile.fps() == self.DEPTH_FPS and d_profile.width() == self.DEPTH_CAMERA_SHAPE[
                    1] and d_profile.height() == self.DEPTH_CAMERA_SHAPE[
                    0] and d_profile.format() == rs.format.z16:
                    depth_profile = d_profile
                    break

        if color_profile is None:
            raise Exception("Valid color streaming profile not found")

        if depth_profile is None:
            raise Exception("Valid color streaming profile not found")

        color_sensor.open(color_profile)
        depth_sensor.open(depth_profile)

        syncer = rs.syncer(self.SYNCER_CAPACITY)
        color_sensor.start(syncer)
        depth_sensor.start(syncer)

        self.syncer = syncer
        self.color_sensor = color_sensor
        self.depth_sensor = depth_sensor
        self.roi_sensor = roi_sensor
        self.depth_scale = depth_sensor.get_depth_scale()
        self.alignment = rs.align(rs.stream.color)

        self.spatial_filter = rs.spatial_filter()
        self.spatial_filter.set_option(
            rs.option.filter_magnitude, self.spacial_filter_magnitude)
        self.spatial_filter.set_option(rs.option.filter_smooth_alpha, self.spacial_filter_alpha)
        self.spatial_filter.set_option(
            rs.option.filter_smooth_delta, self.spacial_filter_delta)

    def get_depth_scale(self):
        return self.depth_scale

    def get_next_frames(self, is_depth_frame_required=True, is_depth_data_post_processed=False):
        color_array = None
        depth_array = None

        it = 0
        while (color_array is None) or (is_depth_frame_required and (depth_array is None)):
            it += 1
            frameset: rs.composite_frame = self.syncer.wait_for_frames().as_frameset()

            aligned_frameset: rs.composite_frame = self.alignment.process(frameset)

            color_data: rs.frame = aligned_frameset.get_color_frame()
            depth_data: rs.frame = aligned_frameset.get_depth_frame()

            if color_data.is_video_frame():
                color_array = np.asanyarray(color_data.get_data())
                if __debug__:
                    self.___debug_show_camera_color(color_array)

            if depth_data.is_depth_frame():
                if is_depth_data_post_processed:
                    depth_data = self.spatial_filter.process(depth_data)

                if __debug__:
                    self.___debug_show_camera_depth_colorized(depth_frame=depth_data)

                depth_array = np.asanyarray(depth_data.get_data())

            if it > 10:
                print("depth_frame_req" + str(is_depth_frame_required))
                print(it)

        if it != 1:
            print(it)

        return color_array, depth_array

    def ___debug_show_camera_color(self, color_frame):
        if config["debug_display"]["SHOW_CAMERA_COLOR_INPUT"]:
            debug_display("SHOW_CAMERA_COLOR_INPUT", color_frame)

    def ___debug_show_camera_depth_colorized(self, depth_frame):
        if config["debug_display"]["SHOW_CAMERA_DEPTH_COLORIZED"]:
            if self.colorizer is None:
                self.colorizer = rs.colorizer()
                self.colorizer.set_option(rs.option.color_scheme, 0)

            colorized_depth = np.asanyarray(
                self.colorizer.colorize(depth_frame).get_data())
            debug_display("SHOW_CAMERA_DEPTH_COLORIZED", colorized_depth)
