from multiprocessing import Queue, Process, Pipe
from multiprocessing.connection import Connection

from camera.camera import Camera
from camera.camera_pipeline_api import CameraPipelineAPI


def camera_read_loop(camera_queue: Queue, file_name=None):
    with CameraPipelineAPI(file_name) as camera:
        while 1:
            camera_queue.put(camera.get_next_frames(is_depth_data_post_processed=True))


def camera_pipe_loop(p_input: Connection, file_name=None):
    with CameraPipelineAPI(file_name) as camera:
        while 1:
            p_input.send(camera.get_next_frames(is_depth_data_post_processed=True))


class CameraProcessWrapper(Camera):
    camera_queue: Queue = None
    camera_process: Process = None

    pipe_mode = True
    p_output: Connection = None

    def get_depth_scale(self):
        return 0.001

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        print("Terminating camera process ...")
        self.camera_process.terminate()

    def __init__(self, file_name=None):

        if self.pipe_mode:
            p_output, p_input = Pipe()
            camera_process = Process(target=camera_pipe_loop, args=(p_input, file_name,))
            self.p_output = p_output
        else:
            camera_queue = Queue(maxsize=1)
            camera_process = Process(target=camera_read_loop, args=(camera_queue, file_name,))
            self.camera_queue = camera_queue

        camera_process.start()
        self.camera_process = camera_process

    def get_next_frames(self, is_depth_frame_required=True, is_depth_data_post_processed=False):
        if self.pipe_mode:
            return self.p_output.recv()
        else:
            return self.camera_queue.get()
