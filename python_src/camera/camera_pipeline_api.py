import json
import logging
import pickle
from datetime import datetime
from typing import List

import numpy as np
import pyrealsense2 as rs

from camera.camera import Camera
from config import config, recordings_folder_path, camera_presets_path
from display.display_lib import debug_display
from utils.img_utils import scale_image

pickle_protocol_version = config["logging"]["pickle"]["PROTOCOL_VERSION"]


class CameraPipelineAPI(Camera):
    """Camera class wrapping pyrealsense2 SDK using rs::frame_queue with rs::pipeline"""

    # --- CLASS CONSTANTS
    INPUT_SHAPE: List = config["camera"]["CAMERA_SHAPE"]
    DEPTH_CAMERA_SHAPE: List = config["camera"]["DEPTH_CAMERA_SHAPE"]

    COLOR_FPS: int = config["camera"]["COLOR_FPS"]
    DEPTH_FPS: int = config["camera"]["DEPTH_FPS"]
    IS_RECORD_MODE: bool = config["camera"]["RECORD"]

    SPACIAL_F_MAG = config["camera"]["F_SPATIAL_MAG"]
    SPACIAL_F_ALPHA = config["camera"]["F_SPATIAL_ALPHA"]
    SPATIAL_F_DELTA = config["camera"]["F_SPATIAL_DELTA"]

    DECIMATION_F_MAG = config["camera"]["F_DECIMATION_VAL"]

    REPEAT_PLAYBACK_ON = config["camera"]["REPEAT_PLAYBACK"]
    PRESET_FILE_NAME = config["camera"]["PRESET_FILE"]

    # --- instance variables
    file_name: str = None
    streaming: bool = False

    device: rs.device = None
    pipeline: rs.pipeline = None
    camera_config: rs.config = None
    profile: rs.stream_profile = None
    queue: rs.frame_queue = None

    colorizer = None
    alignment: rs.align = None

    spatial_filter: rs.spatial_filter = None
    decimation_filter: rs.decimation_filter = None

    def __init__(self, file_name=None):
        logging.getLogger(f"{self.__class__.__name__}").debug(f"{self.__class__.__name__}()")

        if file_name is not None:
            logging.getLogger(f"{self.__class__.__name__}").info(f"\t reading from file: '{file_name}'")

        device_list: rs.device_list = rs.context().query_devices()
        if len(device_list) == 0:
            raise Exception("No intel realsense camera device found")

        self.device: rs.device = device_list[0]
        self.file_name = file_name
        self.queue = rs.frame_queue(capacity=1)
        self.pipeline = rs.pipeline()
        self.camera_config = rs.config()

        self._configure()
        self._configure_filters()
        self._start_stream()

    def _configure_filters(self):
        self.spatial_filter = rs.spatial_filter()
        self.spatial_filter.set_option(
            rs.option.filter_magnitude, self.SPACIAL_F_MAG)
        self.spatial_filter.set_option(rs.option.filter_smooth_alpha, self.SPACIAL_F_ALPHA)
        self.spatial_filter.set_option(
            rs.option.filter_smooth_delta, self.SPATIAL_F_DELTA)

        self.decimation_filter = rs.decimation_filter()
        self.decimation_filter.set_option(rs.option.filter_magnitude, self.DECIMATION_F_MAG)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if self.streaming:
            self._stop_stream()
        logging.getLogger(f"{self.__class__.__name__}").debug(f"~{self.__class__.__name__}()")

    def _start_stream(self):
        self.profile = self.pipeline.start(self.camera_config, self.queue)
        self.streaming = True

    def _stop_stream(self):
        self.pipeline.stop()
        self.streaming = False

    def get_depth_scale(self):
        depth_sensor = self.profile.get_device().first_depth_sensor()
        return depth_sensor.get_depth_scale()

    def get_next_frames(self, is_depth_frame_required=True, is_depth_data_post_processed=False):
        frameset = self.queue.wait_for_frame().as_frameset()

        aligned_frameset = self.alignment.process(frameset)

        depth_data = aligned_frameset.get_depth_frame()

        if self.DECIMATION_F_MAG != 1:
            depth_data = self.decimation_filter.process(depth_data)

        if is_depth_data_post_processed:
            depth_data = self.spatial_filter.process(depth_data)

        if __debug__:
            self.___debug_show_camera_depth_colorized(depth_data)

        depth_data = np.asanyarray(depth_data.get_data())

        color_data = np.asanyarray(aligned_frameset.get_color_frame().get_data())

        if self.DECIMATION_F_MAG != 1:
            color_data = scale_image(color_data, 1 / self.DECIMATION_F_MAG)

        if __debug__:
            self.___debug_show_camera_color_frame(color_data)

        return color_data, depth_data

    def _configure(self):
        self.alignment = rs.align(rs.stream.color)

        self.camera_config.enable_stream(
            rs.stream.depth, self.DEPTH_CAMERA_SHAPE[1], self.DEPTH_CAMERA_SHAPE[0], rs.format.z16, self.DEPTH_FPS)

        self.camera_config.enable_stream(
            rs.stream.color, self.INPUT_SHAPE[1], self.INPUT_SHAPE[0], rs.format.bgr8, self.COLOR_FPS)

        if self.file_name is not None:
            self.camera_config.enable_device_from_file(self.file_name, repeat_playback=self.REPEAT_PLAYBACK_ON)
        elif self.file_name is None and self.IS_RECORD_MODE:
            date_string = datetime.today().strftime('%Y-%m-%d---%Hh-%Mm-%Ss')
            record_file_name = f"{recordings_folder_path}/{date_string}-recording.bag"
            self.camera_config.enable_record_to_file(record_file_name)
            print(f"\t {CameraPipelineAPI.__name__}: Recording enabled to file: '" + record_file_name + "'")
            config_file_name = f"{recordings_folder_path}/{date_string}-config.pkl"
            with open(config_file_name, "wb") as output_file:
                pickle.dump(config, output_file, protocol=pickle_protocol_version)

    def ___debug_show_camera_color_frame(self, color_frame):
        if config["debug_display"]["SHOW_CAMERA_COLOR_INPUT"]:
            debug_display("SHOW_CAMERA_COLOR_INPUT", color_frame)

    def ___debug_show_camera_depth_colorized(self, depth_frame):
        if config["debug_display"]["SHOW_CAMERA_DEPTH_COLORIZED"]:
            if self.colorizer is None:
                self.colorizer = rs.colorizer
                self.colorizer.set_option(rs.option.color_scheme, 0)

            colorized_depth = np.asanyarray(
                self.colorizer.colorize(depth_frame).get_data())
            debug_display("SHOW_CAMERA_DEPTH_COLORIZED", colorized_depth)

    def _load_preset(self, device: rs.device):
        device_as_advanced: rs.rs400_advanced_mode = rs.rs400_advanced_mode(device)
        with open(f"{camera_presets_path}/{self.PRESET_FILE_NAME}") as json_file:
            data = json.load(json_file)
            json_string = str(data).replace("'", '\"')
            device_as_advanced.load_json(json_string)
            print(f"\t{CameraPipelineAPI.__name__} loaded '{self.PRESET_FILE_NAME}'")

    def set_roi(self, min_x, min_y, max_x, max_y):
        roi_sensor: rs.roi_sensor = self.device.first_roi_sensor()
        roi = rs.region_of_interest()
        roi.min_x = int(min_x)
        roi.min_y = int(min_y)
        roi.max_x = int(max_x)
        roi.max_y = int(max_y)
        roi_sensor.set_region_of_interest(roi)
