import cv2
import numpy as np

from calibration.projector_camera_calibrator import ProjectorCameraCalibrator
from camera.camera_pipeline_api import CameraPipelineAPI
from display.second_screen_projector import SecondScreenProjector
from models.calibration.workspace import Workspace
from models.virtual.virtual_rectangle import VirtualRectangle
from utils.color_utils import FlatColors


class MarkerAutoCalibration:
    calibration_zone: VirtualRectangle = None
    MARGIN = [5, 50, 25]
    CALIBRATION_ZONE_SIZE = [50, 50]
    ITERATIONS_PER_COLOR = 60
    # COLOR_LIST = [FlatColors.BLACK, FlatColors.WHITE, FlatColors.BELIZE_HOLE, FlatColors.GREEN_SEA, FlatColors.ALIZARIN]
    COLOR_LIST = [FlatColors.BLACK, FlatColors.WHITE]



    def wait_for_marker_placement(self, workspace, projector):

        loc = np.subtract(np.multiply(workspace.virtual_workspace_shape_scaled[::-1], 0.5),
                          np.multiply(self.CALIBRATION_ZONE_SIZE, 0.5))
        self.calibration_zone = VirtualRectangle(loc, size_xy=self.CALIBRATION_ZONE_SIZE, visual_thickness=2)
        self.calibration_zone.set_all_colors(FlatColors.WHITE)

        virtual_img = np.zeros(workspace.virtual_workspace_shape_scaled).astype(np.uint8)
        virtual_img = cv2.cvtColor(virtual_img, cv2.COLOR_GRAY2BGR)
        self.calibration_zone.draw_self(virtual_img)

        projection_img = workspace.rebuild_projector_sized_image(virtual_img)
        projector.display_img(projection_img)

        while True:
            k = cv2.waitKey()
            if k == ord("c"):
                break

    def project_color(self, projector, workspace: Workspace, color):

        virtual_img = np.zeros(workspace.virtual_workspace_shape_scaled).astype(np.uint8)
        virtual_img = cv2.cvtColor(virtual_img, cv2.COLOR_GRAY2BGR)

        self.calibration_zone.set_all_colors(color)
        self.calibration_zone.visual_thickness = -1
        self.calibration_zone.draw_self(virtual_img)

        projection_img = workspace.rebuild_projector_sized_image(virtual_img)
        projector.display_img(projection_img)
        cv2.waitKey(1)

    def extract_marker_colors(self, camera, workspace: Workspace):
        c, d = camera.get_next_frames()
        c = cv2.cvtColor(c, cv2.COLOR_BGR2HSV)
        color_virtual_img = workspace.transform_to_virtual_img(c)
        calibration_mask = self.calibration_zone.get_collision_mask(workspace.virtual_workspace_shape_scaled)
        color_virtual_img[calibration_mask == 0] = (0, 0, 0)
        loc_xy = np.asarray(self.calibration_zone.location_xy).astype(np.int)
        size_xy = np.asarray(self.calibration_zone.size_xy).astype(np.int)
        relevant_area = color_virtual_img[loc_xy[1]:loc_xy[1] + size_xy[1], loc_xy[0]:loc_xy[0] + size_xy[0]]
        return relevant_area

    def correct(self, hsv):
        hsv[0] = max(0, hsv[0])
        hsv[0] = min(180, hsv[0])

        hsv[1] = max(0, hsv[1])
        hsv[1] = min(255, hsv[1])

        hsv[2] = max(0, hsv[2])
        hsv[2] = min(255, hsv[2])
        return hsv

    def start_loop(self):
        with CameraPipelineAPI() as camera:
            projector = SecondScreenProjector()

            projector_camera_calibrator = ProjectorCameraCalibrator(camera, projector)
            workspace, depth_calibration_info = projector_camera_calibrator.start_calibration()

            self.wait_for_marker_placement(workspace, projector)

            color_squares = []

            for c in self.COLOR_LIST:
                self.project_color(projector, workspace, c)
                for i in range(0, self.ITERATIONS_PER_COLOR):
                    color_squares.append(self.extract_marker_colors(camera, workspace))

            color_squares = np.asarray(color_squares)
            pixel_list = color_squares.reshape(-1, color_squares.shape[-1])

            lower_bound = np.min(pixel_list, axis=0)
            upper_bound = np.max(pixel_list, axis=0)

            print("hsv range:")
            print(list([list(lower_bound), list(upper_bound)]))

            lower_bound = np.subtract(lower_bound, self.MARGIN)
            upper_bound = np.add(upper_bound, self.MARGIN)

            c_l = self.correct(lower_bound)
            c_h = self.correct(upper_bound)

            print("hsv range corrected:")
            print(list([list(c_l), list(c_h)]))

            cv2.waitKey(0)


def main():
    mac = MarkerAutoCalibration()
    mac.start_loop()


if __name__ == "__main__":
    main()
