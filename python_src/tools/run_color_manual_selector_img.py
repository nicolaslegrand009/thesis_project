import sys

import cv2
import numpy as np

from utils.img_utils import dilate, erode, extract_hsv_filtered_masks, combine_masks

# https://www.rapidtables.com/web/color/color-picker.html

file_name = "C:\\Users\\nicolas\\Documents\\n_docs\\school\\mp\\thesis_project\\data\\input\\pictures\\hand_pink_yellow.png"

if len(sys.argv) > 1:
    file_name = sys.argv[1]

INPUT_WINDOW_NAME = "input_window"
TRACK_BAR_WINDOW = "trackbar_"
MASK_WINDOW_NAME = "mask_"
SHOW_INDV_MASKS = True

START_HUE_LIST = [
    #[[0, 44, 0], [180, 255, 148]],  # hands
    #[[0, 210, 0], [180, 255, 255]],  # hand under light
    #[[0, 0, 0], [180, 255, 43]]  # haptics
        [[165, 130, 0], [180, 255, 255]] # pink marker
]


def nothing(x):  # needed for createTrackbar to work in python.
    pass


def create_tracker_windows(hue_list):
    for i in range(0, len(hue_list)):
        window_name = TRACK_BAR_WINDOW + str(i)
        cv2.namedWindow(window_name)
        cv2.createTrackbar('hue_low', window_name, 0, 180, nothing)
        cv2.createTrackbar('sat_low', window_name, 0, 255, nothing)
        cv2.createTrackbar('val_low', window_name, 0, 255, nothing)
        cv2.createTrackbar('hue_high', window_name, 0, 180, nothing)
        cv2.createTrackbar('sat_high', window_name, 0, 255, nothing)
        cv2.createTrackbar('val_high', window_name, 0, 255, nothing)
    update_tracker_pos(hue_list)


def update_tracker_pos(hue_list):
    for i in range(0, len(hue_list)):
        window_name = TRACK_BAR_WINDOW + str(i)
        hue_range = hue_list[i]
        low = hue_range[0]
        high = hue_range[1]
        cv2.setTrackbarPos("hue_low", window_name, low[0])
        cv2.setTrackbarPos("sat_low", window_name, low[1])
        cv2.setTrackbarPos("val_low", window_name, low[2])
        cv2.setTrackbarPos("hue_high", window_name, high[0])
        cv2.setTrackbarPos("sat_high", window_name, high[1])
        cv2.setTrackbarPos("val_high", window_name, high[2])


def get_updated_hue_list(hue_list):
    result = np.copy(hue_list)
    for i in range(0, len(result)):
        window_name = TRACK_BAR_WINDOW + str(i)
        hue_range = result[i]
        low = hue_range[0]
        high = hue_range[1]
        low[0] = cv2.getTrackbarPos("hue_low", window_name)
        low[1] = cv2.getTrackbarPos("sat_low", window_name)
        low[2] = cv2.getTrackbarPos("val_low", window_name)
        high[0] = cv2.getTrackbarPos("hue_high", window_name)
        high[1] = cv2.getTrackbarPos("sat_high", window_name)
        high[2] = cv2.getTrackbarPos("val_high", window_name)
    return result


def main():
    start_hue_list = START_HUE_LIST
    print("opening file: " + file_name)
    img = cv2.imread(file_name)
    create_tracker_windows(start_hue_list)
    while True:
        cv2.imshow(INPUT_WINDOW_NAME, img)
        x_list = get_updated_hue_list(start_hue_list)

        masks = extract_hsv_filtered_masks(img, x_list)

        if SHOW_INDV_MASKS:
            for i in range(0, len(masks)):
                cv2.imshow(MASK_WINDOW_NAME + str(i), masks[i])

        combined_masks = combine_masks(masks)

        cv2.imshow("combined_masks", combined_masks)

        for i in range(0, 2):
            combined_masks = dilate(combined_masks, 5)
            combined_masks = erode(combined_masks, 5)

        cv2.imshow("processed", combined_masks)


        k = cv2.waitKey(10)
        if k == ord("q"):
            break


if __name__ == "__main__":
    main()
