import logging
from typing import Dict, List, Tuple

import cv2

from models.physical.physical_entity import PhysicalEntity
from models.physical.tracked_object import TrackedObject
from tracking.centroid_tracker import CentroidTracker
from utils.img_utils import write_on_img


class PhysicalEntityTracker:
    """Keeps track of physical objects using CentroidTracker"""
    centroid_tracker = None
    physical_entities: Dict[str, PhysicalEntity] = None

    def __init__(self):
        logging.getLogger(f"{self.__class__.__name__}").debug(f"{self.__class__.__name__}()")
        self.physical_entities = {}
        self.centroid_tracker = CentroidTracker(frame_count_before_disappear=30)
        self.centroid_tracker.on_register = self.on_register
        self.centroid_tracker.on_unregister = self.on_unregister

    def _active_entity_map_filter_func(self, object_id, disappeared, tracked_objects,
                                       object_id_to_centroid_idx) -> PhysicalEntity:
        if disappeared[object_id] > 0:
            return None
        else:
            phys_entity: PhysicalEntity = self.physical_entities[object_id]
            p_obj = tracked_objects[object_id_to_centroid_idx[object_id]]
            phys_entity.set_latest_tracked_obj(p_obj)
            return phys_entity

    def update(self, tracked_objects: List[TrackedObject]):

        centroids: List[Tuple[any, any]] = [t.get_centroid() for t in tracked_objects]

        object_id_to_centroids, disappeared, object_id_to_centroid_idx = self.centroid_tracker.update(centroids)

        active_entities: List[PhysicalEntity] = [phys_entity for phys_entity in
                                                 (
                                                     self._active_entity_map_filter_func(p_id, disappeared,
                                                                                         tracked_objects,
                                                                                         object_id_to_centroid_idx) for
                                                     p_id in
                                                     object_id_to_centroid_idx)
                                                 if
                                                 phys_entity is not None]

        return active_entities

    def on_register(self, p_id, centroid):
        self.physical_entities[p_id] = PhysicalEntity(p_id)

    def on_unregister(self, p_id):
        del self.physical_entities[p_id]

    def draw_entities_centroids(self, img, physical_entities):
        for p_ent in physical_entities:
            centroid = p_ent.get_latest_loc()
            if centroid is not None:
                write_on_img(img,
                             msg=f"c:{str(p_ent.id)}", location_xy=centroid)
                cv2.circle(img, center=tuple(centroid), radius=3, color=(0, 255, 255), thickness=3)
