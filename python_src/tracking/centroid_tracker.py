# based on Adrian Rosebrock's code from https://www.pyimagesearch.com/2018/07/23/simple-object-tracking-with-opencv/

from collections import OrderedDict

import numpy as np
from scipy.spatial import distance as dist


class CentroidTracker:
    """Track objects across frames in time based on their centroids"""
    on_register = None
    on_unregister = None

    def __init__(self, frame_count_before_disappear):
        self.next_object_id = 0
        self.objects = OrderedDict()
        self.disappeared = OrderedDict()
        self.frame_count_before_disappear = frame_count_before_disappear

    def register(self, centroid):
        register_id = self.next_object_id
        self.objects[register_id] = centroid
        self.disappeared[register_id] = 0
        self.on_register(register_id, centroid)
        self.next_object_id += 1
        return register_id

    def unregister(self, object_id):
        del self.objects[object_id]
        del self.disappeared[object_id]
        self.on_unregister(object_id)

    def get_id(self, centroid):

        object_ids = list(self.objects.keys())
        object_centroids = list(self.objects.values())

        distance_matrix = dist.cdist(np.array(object_centroids), [centroid])

        rows = distance_matrix.min(axis=1).argsort()
        cols = distance_matrix.argmin(axis=1)[rows]
        object_id = None

        for (row, col) in zip(rows, cols):
            object_id = object_ids[row]
            break
        return object_id

    def update(self, centroids):

        my_result = {}

        if len(centroids) == 0:
            for object_id in list(self.disappeared.keys()):
                self.disappeared[object_id] += 1

                if self.disappeared[object_id] > self.frame_count_before_disappear:
                    self.unregister(object_id)
            return self.objects, self.disappeared, my_result

        input_centroids = centroids

        if len(self.objects) == 0:
            for i in range(0, len(input_centroids)):
                centroid = input_centroids[i]
                self.register(centroid)

        else:
            objects_ids = list(self.objects.keys())
            object_centroids = list(self.objects.values())

            D = dist.cdist(np.array(object_centroids), input_centroids)

            rows = D.min(axis=1).argsort()
            cols = D.argmin(axis=1)[rows]

            used_rows = set()
            used_cols = set()

            for (row, col) in zip(rows, cols):

                if row in used_rows or col in used_cols:
                    continue

                object_id = objects_ids[row]
                self.objects[object_id] = input_centroids[col]
                self.disappeared[object_id] = 0

                my_result[object_id] = col

                used_rows.add(row)
                used_cols.add(col)

            unused_rows = set(range(0, D.shape[0])).difference(used_rows)
            unused_cols = set(range(0, D.shape[1])).difference(used_cols)

            if D.shape[0] >= D.shape[1]:

                for row in unused_rows:
                    object_id = objects_ids[row]
                    self.disappeared[object_id] += 1

                    if self.disappeared[object_id] > self.frame_count_before_disappear:
                        self.unregister(object_id)

            else:

                for col in unused_cols:
                    centroid = input_centroids[col]
                    id = self.register(centroid)
                    my_result[id] = col

        return self.objects, self.disappeared, my_result
