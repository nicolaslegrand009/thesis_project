# Enhancing Projected Augmented Reality Interactions using Haptic Technology


Student: Nicolas Legrand

Student number: 01710212

Supervisors: Supervisors: Prof. dr. ir. Filip De Turck, Dr. ir. Femke De Backere

Counsellors: Ir. Joris Heyse, Dr. ir. Maria Torres Vega, Ir. Stéphanie Carlier

## Thesis source code
This repository contains the implementation described in the paper. 
The jupyter notebooks used for the objective experiments covered in the thesis are available in the following [git repository](https://gitlab.com/nicolaslegrand009/thesis_objective_measurements).

___

### Python Main Application
Note: <> denotes changes to be made

#### Setup

Install the required python packages by using the following command in the "python_src" folder:

`python -m pip install -r requirements.txt`

It is advised to use [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) to create an isolated environment. 

A few handy conda commands are listed below:

- Create a conda environment
`conda create --name <environment-name> python=<3.7.6>`

- Use an exported environment file
`conda env create -f <environment-name>.yml`

#### Usage

Running a single experiment: 
`python -OO run_experiment.py -s <scene_index> <-l> <-m>`

- `-l` enables left handed mode
- `-m` enables markerless mode
- `-s` determines the experiment `1` Trace the Line, `2` Obstacle Course, `3` Simon Says 

Running the full experiment suite:
`python -OO run_experiment_set.py --hand <left|right> --id <candidate_id> --group <A|B>`
- `--hand` denotes left or right hand mode
- `--id` denotes the candidate id (string)
- `--group` denotes the group the candidate belongs to (A or B)
___

### Unity Supporting Application
A supporting Unity application was written to control haptic gloves ([Hi5 VR Gloves](https://hi5vrglove.com/)).
The python main application sends messages via [ZMQ](https://zeromq.org/) to the Unity application to trigger vibrations.

#### Build and Run Unity Application

- Import the "unity_src" folder in Unity
- Build the project with the `server build` checkbox checked
- Run the `.exe` file generated

